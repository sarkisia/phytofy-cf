import React, { Component } from 'react';
import styled, { css } from 'styled-components';

import Icon from './Icons';

const Button = styled.button`
	border: 0;
	border-radius: 4px;
	height: 36px;
	padding: 0 26px;
	color: var(--backgroundColor, #FFFFFF);
	appearance: none;
	cursor: pointer;
	transition-duration: .2s;

	${props => props.state === "success" && css`
		margin-top: var(--spacing, 30px);
		background-color: var(--green, #009D3D);

		&:hover {
			background-color: var(--darkGreen, #006d0e);
		}
  `}

	${props => props.state === "cancel" && css`
		background-color: var(--sidebarPrimaryColor, #C5C6C8);

		&:hover {
			background-color: var(--darkGrey, #A7A8AA);
		}
  `}

	${props => props.state === "danger" && css`
		margin-top: var(--spacing, 30px);
		background-color: var(--red, #E53012);

		&:hover {
			background-color: #D84315;
		}
  `}

	svg {
		width: auto;
		height: 26px;
	}
`

const Text = styled.span`
	font-size: 1rem;
`

export default class extends Component {
	render() {
		const icon = this.props.icon;
		const name = this.props.name;

		return (
			<Button disabled={this.props.disabled} state={this.props.state} type='button' onClick={this.props.onClick}>
				{icon ? <Icon icon={this.props.icon} /> : ''}
				{name ? <Text>{this.props.name}</Text> : ''}
			</Button>
		)
	}
}
