// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';

import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
import FiltersContainer from './FiltersContainer';
import Profile, { canopyConfigs, getCanopyConfigsForSelect } from '../models/Profile';
import { lengthUnitOptions, getLengthToRead } from '../models/Settings'
import * as SidebarActions from '../actions/sidebar';
import * as ProfilesActions from '../actions/profiles'; 


const ProfileContainer = class extends Component {

  constructor(props) {
    super(props);
    this.state = { objects: [] };
  }

  componentDidMount() {
    this.props.profilesActions.getObjects();
  }

  render() {
    let { t } = this.props;

    const tableAttributes = {
      name: {name: t('name'), configs: []},
      config: {name: t('orientation'), configs: [], transform: (row) => {return canopyConfigs[row.config]}},
      length: {name: t('length'), configs: ['measure'], transform: (row) => {return `${getLengthToRead(row.length, this.props.settings.lengthUnit)}`}},
      width: {name: t('width'), configs: ['measure'], transform: (row) => {return `${getLengthToRead(row.width, this.props.settings.lengthUnit)}`}},
      height: {name: t('height'), configs: ['measure'], transform: (row) => {return `${getLengthToRead(row.height, this.props.settings.lengthUnit)}`}},
      deltaLength: {name: `△ ${t('length')}`, configs: ['measure'], transform: (row) => {return row.deltaLength ? `${getLengthToRead(row.deltaLength, this.props.settings.lengthUnit)}` : '-'}},
      deltaWidth: {name: `△ ${t('width')}`, configs: ['measure'], transform: (row) => {return row.deltaWidth ? `${getLengthToRead(row.deltaWidth, this.props.settings.lengthUnit)}` : '-'}},
      nCanopies: {name: t('canopies'), configs: ['measure']},
    }

    let actionButtons = [
      { icon: 'add', onClick: (ids) => { this.props.profilesActions.create() } },
    ];

    let actionButtonsWhenSelected = [{ icon: 'delete', onClick: () => { this.props.profilesActions.deleteMultiple() } }];

    let configFilter = [
      {value: -1, name: 'All'},
      ...getCanopyConfigsForSelect()
    ]

    const filters = [
      {label: t('orientation'), options: configFilter, attribute: 'config'}
    ]

    const rowOptions = [
      {optionMethod: this.props.profilesActions.edit, iconName: 'edit'},
      {optionMethod: this.props.profilesActions.duplicate, iconName: 'duplicate'},
      {optionMethod: this.props.profilesActions.deleteObj, iconName: 'delete'}
    ];

    return (
      <div>
        <FiltersContainer search={(value) => {this.props.profilesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.profilesActions.addFilter} removeFilter={this.props.profilesActions.removeFilter} />
        <Content>
          {this.props.objects != null &&
            <ListContainer title={t('layouts')} actions={{...this.props.profilesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
              actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} rowOptions={rowOptions} />
          }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.profiles.sortAttribute ? state.profiles.sortAttribute : null,
    sortType: state.profiles.sortType ? state.profiles.sortType : 'orderAsc',
    objects: state.profiles.objects,
    count: state.profiles.count,
    from: state.profiles.from,
    to: state.profiles.to,
    selected: state.profiles.selected,
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    profilesActions: {...bindActionCreators(ProfilesActions, dispatch)}
  }
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(ProfileContainer));