// @flow
import validator from 'validator';
import Notifications from 'react-notification-system-redux';
import moment from 'moment';

import Profile from '../models/Profile';
import Canopy, { CanopyFormValidator, getFixturesFromCanopy } from '../models/Canopy';
import { getPageSize } from '../models/Settings';
import Schedule from '../models/Schedule';
import Recipe from '../models/Recipe';
import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
  makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext,
  makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo, runCommandOnFixtures } from './helpers';
import { openSidebar, closeSidebar } from './sidebar';

const defaultCanopy = {name: ""};

const queryCanopies = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let canopies;


  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);

  try {
    canopies = await Canopy.cfind(filters).sort({name: 1}).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return canopies;
}

const queryCountCanopies = async(filters = {}) => {
  try {
    return Canopy.ccount(filters).exec();
  } catch(err){
    console.error(err);
    return 0;
  }
}

export const getObjects = () => {
  return async (dispatch, getState) => {
    let { sortAttribute, sortType, from, filters } = getState().canopies;
    let objects = await queryCanopies('name', 1, 1, {});
    for(let i=0; i<objects.length; i++){
        let canopy = objects[i];
        let fixtures = await getFixturesFromCanopy(canopy);
        
        let schedules = await Schedule.cfind({canopies: canopy._id}).exec();
        schedules = schedules.sort(function(a,b) {return moment(a.end_date) < moment(b.end_date) ? 1 : ((moment(b.end_date) < moment(a.end_date)) ? -1 : 0)} )
        if(schedules.length > 0){
          let recipe = await Recipe.findOne({_id: schedules[0].recipeId});
          let recipeLastPeriodTime = recipe.periods[recipe.periods.length-1].end;
          let lastScheduleDate = moment(`${schedules[0].end_date} ${recipeLastPeriodTime}`);
          let diff = lastScheduleDate.diff(moment(), 'seconds');
          
          if(diff >= 3600*24) {
            objects[i].scheduleTime = lastScheduleDate.diff(moment(), 'days');
            objects[i].scheduleTimeUnit = 'days'
          } else if (diff >= 3600) {
            objects[i].scheduleTime = lastScheduleDate.diff(moment(), 'hours');
            objects[i].scheduleTimeUnit = 'hours'
          } else if (diff >= 60) {
            objects[i].scheduleTime = lastScheduleDate.diff(moment(), 'minutes');
            objects[i].scheduleTimeUnit = 'minutes'
          } else {
            objects[i].scheduleTime = diff;
            objects[i].scheduleTimeUnit = 'seconds'
          }
        }

        try {
          await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
            objects[i].ledState = await getState().devices.comm.getLEDstate(fixture.serial_num);
            objects[i].irradiance = objects[i].ledState.uv+objects[i].ledState.blue+objects[i].ledState.green+objects[i].ledState.farRed+objects[i].ledState.hyperRed+objects[i].ledState.white;
            
          });
        } catch(err){
          console.error(err);
        }
    }

    let count = await countQuery(filters, Canopy);
    dispatch({
        type: "CANOPIES_GET",
        objects,
        count,
        ...await getPaginationInfo(from, count)
    })
  }
}


// export const getObjects = makeGetObjectsAction('CANOPIES_GET', 'canopies', Canopy, queryCanopies);
export const getObjectsAll = makeGetObjectsAllAction('CANOPIES_GET_ALL', 'canopies', Canopy, queryCanopies);
export const addFilter = makeAddFilterAction('CANOPIES_ADD_FILTER', 'canopies', getObjects);
export const removeFilter = makeRemoveFilterAction('CANOPIES_REMOVE_FILTER', 'canopies', getObjects);
