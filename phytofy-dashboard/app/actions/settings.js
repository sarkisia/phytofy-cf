import Notifications from 'react-notification-system-redux';
import JSZip from 'jszip';
import fs from 'fs';
import Settings, {getIrradianceColorscale, getIrradianceUnit, getLengthUnit, getPageSize} from '../models/Settings';
import Profile, {exportData as exportProfiles, importData as importProfiles} from '../models/Profile';
import Canopy from '../models/Canopy';


export const setSettings = (pageSize, lengthUnit, irradianceUnit, irradianceColorscale) => {
    return async (dispatch, getState) => {
        await Settings.remove({}, { multi: true });
        await Settings.insert({name: 'pageSize', value: pageSize});
        await Settings.insert({name: 'lengthUnit', value: lengthUnit});
        await Settings.insert({name: 'irradianceUnit', value: irradianceUnit});
        await Settings.insert({name: 'irradianceColorscale', value: irradianceColorscale});

        dispatch({
            type: 'SETTINGS_SET',
            pageSize,
            lengthUnit,
            irradianceUnit,
            irradianceColorscale
        })

        dispatch(Notifications.success({
            title: 'Preferences changed',
            message: `Preferences changed with success.`
        }));
    }
}

export const getSettings = () => {
    return async (dispatch, getState) => {
        let pageSize = await getPageSize();
        let lengthUnit = await getLengthUnit();
        let irradianceUnit = await getIrradianceUnit();
        let irradianceColorscale = await getIrradianceColorscale();

        dispatch({
            type: 'SETTINGS_GET',
            pageSize,
            lengthUnit,
            irradianceUnit,
            irradianceColorscale
        })
    }
};

export const exportData = (profiles, recipes) => {
    return async (dispatch, getState) => {
        const {dialog} = require('electron').remote
        dialog.showSaveDialog({defaultPath: 'phytofyExport.zip'}, (async (path) => {
            const Json2csvParser = require('json2csv').Parser;
            let fields = ['_id', 'name', ''];
            let profilesCSV = profiles ? exportProfiles() : null;

            var zip = new JSZip();
            if(profilesCSV) zip.file("layouts.csv", profilesCSV);

            zip.generateNodeStream({type:'nodebuffer',streamFiles:true})
            .pipe(fs.createWriteStream(path))
            .on('finish', function () {
                // JSZip generates a readable stream with a "end" event,
                // but is piped here in a writable stream which emits a "finish" event.
                dispatch(Notifications.success({
                    title: 'Data exported',
                    message: `Data exported with success.`
                }));        
            });
        }));
    }
}

export const importData = (files) => {
    return async (dispatch, getState) => {
        for (var i = 0; i < files.length; i++) {
            let file = files[i];
        
            fs.readFile(file.path, function(err, data) {
                if (err) throw err;
                JSZip.loadAsync(data).then(function (zip) {
                  // Get list of filenames, which are also keys for additional details
                  let zipFiles = Object.keys(zip.files);
                    
                  for(i=0; i<zipFiles.length; i++){
                    zip.file(zipFiles[i]).async('nodebuffer').then(function(content) {
                        var parse = require('csv-parse/lib/sync');
                        var records = parse(content, {columns: true});
                        importProfiles(records);
                    });
                  }
                });
            });
        }

        dispatch(Notifications.success({
            title: 'Data imported',
            message: `Data imported with success.`
        }));
    }
}

export const deleteData = () => {
    return async (dispatch, getState) => {
        dispatch(Notifications.warning({
            title: `Reset data`,
            message: `Are you sure you want to delete all the data?`,
            action: {
            label: 'Yes',
            callback: () => {
                dispatch(confirmDeleteData())
            }
            }
        }));
    };
}

export const confirmDeleteData = () => {
    return async (dispatch, getState) => {
        Profile.remove({}, {multi: true});
        Canopy.remove({}, {multi: true});
        dispatch(Notifications.success({
            title: 'Data deleted',
            message: `Data deleted with success.`
        }));
    };
}