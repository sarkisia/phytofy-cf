import React, { Component } from 'react'
// import Icon from '../atoms/Icons'
import { Line } from 'react-chartjs-2'

// import FormGroup from '../molecules/FormGroup'
// import ItemTimeContainer from '../molecules/ItemTimeContainer'
import WavelenghtControlGroup from '../organisms/WavelenghtControlGroup'
import ChannelContainer from '../molecules/ChannelContainer'
import TotalContainer from '../molecules/TotalContainer'
import ChartContainer from '../molecules/ChartContainer'
import VerticalSlider from '../atoms/VerticalSlider'
import LightStatusContainer from '../molecules/LightStatusContainer'
import ResetContainer from '../molecules/ResetContainer'
import Toggle from '../atoms/Toggle'
import Button from '../atoms/Button'

const data = (canvas) => {
    canvas.width  = 670;
    canvas.height  = 300;
    const ctx = canvas.getContext("2d");

    /* Gradient Colors */

    const gradient = ctx.createLinearGradient(0, 0, ctx.canvas.width, 0);
    gradient.addColorStop(0, 'rgb(64, 0, 64)');
    gradient.addColorStop(0.16, 'rgb(255, 0, 255)');
    gradient.addColorStop(0.36, 'rgb(0, 0, 255)');
    gradient.addColorStop(0.38, 'rgb(0, 255, 255)');
    gradient.addColorStop(0.43, 'rgb(0, 255, 0)');
    gradient.addColorStop(0.56, 'rgb(255, 255, 0)');
    gradient.addColorStop(0.69, 'rgb(255, 0, 0)');
    gradient.addColorStop(0.96, 'rgb(255, 0, 0)');
    gradient.addColorStop(1, 'rgb(64, 0, 0)');

    console.log("width: " + ctx.canvas.width);

    return {
        labels: ['300', '', '', '', '', '800'],
        datasets: [
            {
                fill: true,
                backgroundColor : gradient,
                lineTension: 0.3,
                borderWidth: 0,
                borderColor: 'rgba(255, 255, 255, 0)',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointRadius: 0,
                data: [31, 173, 73, 198, 93, 193],
            }
        ]
    }
}

class RealTimeControl extends Component {
    render() {
        return (
            <div>
                <WavelenghtControlGroup>
                    <ChannelContainer title="Channels" unit={["μmol/m", <sup>2</sup>, "/s"]}>
                        <VerticalSlider channel="uv" min={0} max={400} defaultValue={31} percentage="00" />
                        <VerticalSlider channel="blue" min={0} max={400} defaultValue={173} percentage="00" />
                        <VerticalSlider channel="green" min={0} max={400} defaultValue={73} percentage="00" />
                        <VerticalSlider channel="hyperRed" min={0} max={400} defaultValue={198} percentage="00" />
                        <VerticalSlider channel="farRed" min={0} max={400} defaultValue={93} percentage="00" />
                        <VerticalSlider channel="warmWhite" min={0} max={400} defaultValue={193} percentage="00" />
                    </ChannelContainer>
                    <TotalContainer title="Total">
                        <VerticalSlider channel="total" min={0} max="300" defaultValue={200} disabled={true} unit="%" />
                    </TotalContainer>
                </WavelenghtControlGroup>

                <ChartContainer title="Wavelenght">
                    <Line
                        data={data}
                        options={{
                            pointDot: false,
                            tooltips: false,
                            legend: {
                                display: false,
                            },
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        color: "rgba(0, 0, 0, 0)",
                                    },
                                    ticks: {
                                        min: 300,
                                        max: 800,
                                        stepSize: 500,
                                    }
                                }],
                                yAxes: [{
                                    gridLines: {
                                        color: "rgba(0, 0, 0, 0)",
                                    },
                                    ticks: {
                                        min: 0,
                                        max: 400,
                                        stepSize: 500
                                    }
                                }]
                            }
                        }}
                    />
                </ChartContainer>

                <WavelenghtControlGroup>
                    <LightStatusContainer title="Lights">
                        <Toggle optionLeft="On" optionRight="Off" />
                    </LightStatusContainer>

                    <ResetContainer title="Reset">
                        <Button state="success" name="Back to Recipe" onClick="console.log('click')" />
                    </ResetContainer>
                </WavelenghtControlGroup>
            </div>
        )
    }
}

export default RealTimeControl;
