import React, { Component } from 'react'
import Timeline from 'react-visjs-timeline'
import { Accordion, AccordionItem, AccordionItemTitle, AccordionItemBody } from 'react-accessible-accordion'
import Icon from '../atoms/Icons'
import { Line } from 'react-chartjs-2'

import FormGroup from '../molecules/FormGroup'
import ItemTimeContainer from '../molecules/ItemTimeContainer'
import WavelenghtControlGroup from '../organisms/WavelenghtControlGroup'
import ChannelContainer from '../molecules/ChannelContainer'
import TotalContainer from '../molecules/TotalContainer'
import ChartContainer from '../molecules/ChartContainer'
import VerticalSlider from '../atoms/VerticalSlider'
import Button from '../atoms/Button'

// import '../../css/Vis.css'
// import '../../css/Accordion.css'

/* Timeline Options */

const options = {
    width: '100%',
    height: '79px',
    showMajorLabels: false,
    showMinorLabels: true,
    showCurrentTime: false,
    min: 1532473200000,
    max: 1532559599000,
    start: 1532473200000,
    end: 1532559599000,
    zoomMin: 300000,
    stack: false,
    format: {
        minorLabels: {
            minute: 'h:mma',
            hour: 'ha'
        }
    }
}

/* Timeline Items */

const items = [
    { id: 1, start: '2018-07-25 10:00:00', end: '2018-07-25 10:59:00' },
    { id: 2, start: '2018-07-25 11:00:00', end: '2018-07-25 11:59:00' },
    { id: 3, start: '2018-07-25 12:30:00', end: '2018-07-25 13:30:00' },
    { id: 4, start: '2018-07-25 17:00:00', end: '2018-07-25 19:00:00' },
]

const data = (canvas) => {
    const ctx = canvas.getContext("2d");

    /* Gradient Colors */

    const gradient = ctx.createLinearGradient(0, 0, 600, 0);
    gradient.addColorStop(0, 'rgb(64, 0, 64)');
    gradient.addColorStop(0.16, 'rgb(255, 0, 255)');
    gradient.addColorStop(0.36, 'rgb(0, 0, 255)');
    gradient.addColorStop(0.38, 'rgb(0, 255, 255)');
    gradient.addColorStop(0.43, 'rgb(0, 255, 0)');
    gradient.addColorStop(0.56, 'rgb(255, 255, 0)');
    gradient.addColorStop(0.69, 'rgb(255, 0, 0)');
    gradient.addColorStop(0.96, 'rgb(255, 0, 0)');
    gradient.addColorStop(1, 'rgb(64, 0, 0)');

    return {
        labels: ['300', '', '', '', '', '800'],
        datasets: [
            {
                fill: true,
                backgroundColor : gradient,
                lineTension: 0.3,
                borderWidth: 0,
                borderColor: 'rgba(255, 255, 255, 0)',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointRadius: 0,
                data: [31, 173, 73, 198, 93, 193],
            }
        ]
    }
}

const TimelineItems = () => (
    <Accordion accordion={false}>
        <AccordionItem>
            <AccordionItemTitle>
                <span>Title</span>
                <div class="accordion__arrow">
                    <Icon icon="arrowDown" fill="#87888A" />
                </div>
            </AccordionItemTitle>
            <AccordionItemBody>
                <ItemTimeContainer title="Time">
                    <FormGroup type="time" label="Start" value="" min="00:00" max="23:59" />
                    <FormGroup type="time" label="End" value="" min="00:00" max="23:59" />
                </ItemTimeContainer>

                <WavelenghtControlGroup>
                    <ChannelContainer title="Channels" unit={["μmol/m", <sup>2</sup>, "/s"]}>
                        <VerticalSlider channel="uv" min={0} max={400} defaultValue={31} percentage="00" />
                        <VerticalSlider channel="blue" min={0} max={400} defaultValue={173} percentage="00" />
                        <VerticalSlider channel="green" min={0} max={400} defaultValue={73} percentage="00" />
                        <VerticalSlider channel="hyperRed" min={0} max={400} defaultValue={198} percentage="00" />
                        <VerticalSlider channel="farRed" min={0} max={400} defaultValue={93} percentage="00" />
                        <VerticalSlider channel="warmWhite" min={0} max={400} defaultValue={193} percentage="00" />
                    </ChannelContainer>
                    <TotalContainer title="Total">
                        <VerticalSlider channel="total" min={0} max="300" defaultValue={200} disabled={true} unit="%" />
                    </TotalContainer>
                </WavelenghtControlGroup>

                <ChartContainer title="Wavelenght">
                    <Line
                        data={data}
                        options={{
                            pointDot: false,
                            tooltips: false,
                            legend: {
                                display: false,
                            },
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        color: "rgba(0, 0, 0, 0)",
                                    },
                                    ticks: {
                                        min: 300,
                                        max: 800,
                                        stepSize: 500,
                                    }
                                }],
                                yAxes: [{
                                    gridLines: {
                                        color: "rgba(0, 0, 0, 0)",
                                    },
                                    ticks: {
                                        min: 0,
                                        max: 400,
                                        stepSize: 500
                                    }
                                }]
                            }
                        }}
                    />
                </ChartContainer>

                <Button state="success" name="Save" />
                <Button state="danger" name="Delete" />
            </AccordionItemBody>
        </AccordionItem>

        <AccordionItem>
            <AccordionItemTitle>
                <span>Title</span>
                <div class="accordion__arrow">
                    <Icon icon="arrowDown" fill="#87888A" />
                </div>
            </AccordionItemTitle>
            <AccordionItemBody>
                <p>Has closed eyes but still sees you have a lot of grump in yourself because you can't forget to be grumpy and not be like king grumpy cat yet you have cat to be kitten me right meow licks paws chill on the couch table stare out the window flop over.</p>
            </AccordionItemBody>
        </AccordionItem>

        <AccordionItem>
            <AccordionItemTitle>
                <span>Title</span>
                <div class="accordion__arrow">
                    <Icon icon="arrowDown" fill="#87888A" />
                </div>
            </AccordionItemTitle>
            <AccordionItemBody>
                <p>Has closed eyes but still sees you have a lot of grump in yourself because you can't forget to be grumpy and not be like king grumpy cat yet you have cat to be kitten me right meow licks paws chill on the couch table stare out the window flop over.</p>
            </AccordionItemBody>
        </AccordionItem>
    </Accordion>
);

class ViewRecipe extends Component {
    render() {
        return (
            <div>
                <Timeline options={options} items={items} />
                <TimelineItems />
            </div>
        )
    }
}

export default ViewRecipe;
