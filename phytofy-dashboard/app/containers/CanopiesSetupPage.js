// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';

import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
import FiltersContainer from './FiltersContainer';
import GridContainer from './GridContainer';

import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as CanopiesActions from '../actions/canopiesSetup';
import * as ProfilesActions from '../actions/profiles'; 


const CanopyContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [], view: 'grid' };
  }

  componentDidMount() {
    this.props.canopiesActions.getObjects();
    this.props.profilesActions.getObjectsAll();
  }

  render() {
    const tableAttributes = {
      name: {name: "Name", configs: []},
      '_profile.name': {name: "Layout", configs: [], transform: (row) => `${row._profile.name} (${canopyConfigs[row._profile.config]})` },
      scheduleState: {name: "Schedule state", configs: ['status', 'nosort'], transform: (row) => {
        if(row.scheduleState){
          return <Icon fill="#009D3D" icon="checkedCircle" />
        } else {
          return <Icon fill="#FF6600" icon="warning" />
        }
      }}
    }

    let actionButtons = [
      { icon: 'add', onClick: (ids) => { this.props.canopiesActions.create() } },
    ];

    let actionButtonsWhenSelected = [
      { icon: 'play', onClick: () => { this.props.canopiesActions.resumeScheduleOnMultiple() } },
      { icon: 'pause', onClick: () => { this.props.canopiesActions.stopScheduleOnMultiple() } },
      { icon: 'delete', onClick: () => { this.props.canopiesActions.deleteMultiple() } },
    ];

    const getProfilesForSelect = (profiles) => {
      return profiles.map((profile) => {
          return {name: `${profile.name} (${canopyConfigs[profile.config]})`, value: profile._id}
      })
    };

    let profileFilter = [
      {value: -1, name: 'All'},
      ...getProfilesForSelect(this.props.profiles)
    ];

    const filters = [
      {label: 'Layout', options: profileFilter, attribute: 'profileId'}
    ]

    const rowOptions = [
      {optionMethod: this.props.canopiesActions.resumeScheduleOnCanopy, iconName: 'play'},
      {optionMethod: this.props.canopiesActions.stopScheduleOnCanopy, iconName: 'pause'},
      {optionMethod: this.props.canopiesActions.realtimeControl, iconName: 'recipes'},
      {optionMethod: this.props.canopiesActions.edit, iconName: 'edit'},
      {optionMethod: this.props.canopiesActions.duplicate, iconName: 'duplicate'},
      {optionMethod: this.props.canopiesActions.deleteObj, iconName: 'delete'}
    ];

    return (
      <div>
        <FiltersContainer search={(value) => {this.props.canopiesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.canopiesActions.addFilter} removeFilter={this.props.canopiesActions.removeFilter} />
        <Content>
        <ListContainer title="Canopies" actions={{...this.props.canopiesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
            actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} rowOptions={rowOptions} />
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.canopiesSetup.sortAttribute ? state.canopiesSetup.sortAttribute : null,
    sortType: state.canopiesSetup.sortType ? state.canopiesSetup.sortType : 'orderAsc',
    objects: state.canopiesSetup.objects,
    count: state.canopiesSetup.count,
    from: state.canopiesSetup.from,
    to: state.canopiesSetup.to,
    selected: state.canopiesSetup.selected,
    profiles: state.profiles.objectsAll
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    canopiesActions: {...bindActionCreators(CanopiesActions, dispatch)},
    profilesActions: {...bindActionCreators(ProfilesActions, dispatch)}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CanopyContainer);