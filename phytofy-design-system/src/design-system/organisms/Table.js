import React, { Component } from 'react';
import styled from 'styled-components';

import TableRow from '../molecules/TableRow';

const Table = styled.table `
  --selectable: 80px;
  --config: 100px;
  --measure: 100px;
  --status: 100px;
  --orderIcon: 18px;
  --rowHeight: 50px;

  position: relative;
  width: 100%;
  border-spacing: 0;
  text-align: left;
`;

const TableHead = styled.thead `
  font-weight: var(--bold, 700);
  color: var(--secondaryColor, #87888A);
  vertical-align: middle;
`

const TableBody = styled.tbody `
  tr {
  	transition-duration: .4s;

    &:nth-child(even) {
    	background-color: var(--backgroundColor, #FFFFFF);

      > div {
        background-color: var(--backgroundColor, #FFFFFF);
      }
    }

    &:nth-child(odd) {
    	background-color: var(--backgroundPrimaryMenu, #F5F6F8);

      > div {
        background-color: var(--backgroundPrimaryMenu, #F5F6F8);
      }
    }

    &:hover {
    	box-shadow: 0 0 20px rgba(0, 0, 0, .2);

      > div {
        opacity: 1;
      }
    }

    svg {
    	margin-top: 4px;
    	width: auto;
    	height: 26px;
    }
  }
`

export default class extends Component {

  render() {
    return (
      <Table>
        { this.props.header &&
          <TableHead>
            <TableRow>
              { this.props.header }
            </TableRow>
          </TableHead>
        }
        <TableBody>
          { this.props.children }
        </TableBody>
      </Table>
    );
  }
}
