import React, { Component } from 'react';
import styled from 'styled-components';

import SelectOption from './SelectOption';

const Select = styled.select`
  border: 0;
  border-bottom: 1px solid var(--sidebarPrimaryColor, #C5C6C8);
  border-radius: 0;
  background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='32' height='32' viewBox='0 0 32 32'><path fill='rgb(197,198,200)' d='M10.827 12.387l5.173 5.173 5.173-5.173c0.52-0.52 1.36-0.52 1.88 0v0c0.52 0.52 0.52 1.36 0 1.88l-6.12 6.12c-0.52 0.52-1.36 0.52-1.88 0l-6.12-6.12c-0.52-0.52-0.52-1.36 0-1.88v0c0.52-0.507 1.373-0.52 1.893 0z'></path></svg>");
	background-position: 100% -2px;
	background-repeat: no-repeat;
	background-size: 24px 24px;
  background-color: transparent;
  width: 100%;
  padding-bottom: 10px;
  font-size: 1rem;
  color: var(--textColor, #424242);
	appearance: none;

  &:-moz-focusring {
    color: transparent;
    text-shadow: 0 0 0 var(--textColor, #424242);
  }
`;

export default class extends Component {
	render() {
    const selectOptions = this.props.values.map((value) =>
      <SelectOption value={value.value} name={value.name}/>
    );

		return (
			<Select name={this.props.name} onChange={this.props.onChange} defaultValue={this.props.defaultValue} value={this.props.value}>
        {selectOptions}
      </Select>
		)
	}
}
