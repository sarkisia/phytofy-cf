import { DEFAULT_COLORSCALE, DEFAULT_IRRADIANCE_UNIT, DEFAULT_LENGTH_UNIT, DEFAULT_PAGE_SIZE } from '../models/Settings';

const defaultState = {
    pageSize: DEFAULT_PAGE_SIZE,
    irradianceUnit: DEFAULT_IRRADIANCE_UNIT, 
    lengthUnit: DEFAULT_LENGTH_UNIT,
    irradianceColorscale: DEFAULT_COLORSCALE
}

export default function settings (state = defaultState, action){
    switch(action.type) {
        case 'SETTINGS_GET':
            return {
                ...state,
                pageSize: action.pageSize,
                irradianceUnit: action.irradianceUnit,
                lengthUnit: action.lengthUnit,
                irradianceColorscale: action.irradianceColorscale,
            }
        case 'SETTINGS_SET':
            return {
                ...state,
                pageSize: action.pageSize,
                irradianceUnit: action.irradianceUnit,
                lengthUnit: action.lengthUnit,
                irradianceColorscale: action.irradianceColorscale,
            }
        default:
            return {
                ...state
            }
    }
}