import React, { Component } from 'react'
import styled, { css } from 'styled-components'

import MenuLink from '../molecules/MenuLink';

const PrimaryMenu = styled.div`
    --menuWidth: 80px;
    --menuLinkHeight: 40px;

    position: fixed;
    overflow: scroll;
    background-color: var(--backgroundPrimaryMenu, #F5F6F8);
    grid-row: 2 / 3;
    grid-column: 1 / 2;
    width: var(--menuWidth, 240px);
    height: 100vh;
    padding: calc(60px + var(--spacing, 30px)) 0 0 0;
    z-index: 3;
    transition-duration: 0.4s;

    &::-webkit-scrollbar {
        display: none;
    }

    &::-webkit-scrollbar {
        display: none;
    }

    ul {
        margin-bottom: var(--spacing, 30px);
    }
`

export default class extends Component {
    render() {
        const menuLink = this.props.values.map((value) =>
            <MenuLink to={value.location} icon={value.icon} name={value.name} isActive={this.props.checkIsActive(value.location)} />
            // onClick={() => this.props.renderRoute(value.location)}
        );

        return (
            <PrimaryMenu isOpen={this.props.isOpen}>
                <ul>
                    { menuLink }
                </ul>
            </PrimaryMenu>
        );
    }
}
