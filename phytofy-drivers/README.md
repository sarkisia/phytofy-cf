# phytofy-drivers

## Overview

This file documents the external driver modules under /lib, internal functions and modules under /src are not documented here. How to use instructions for all functionalities is also presented here.

## How to use

``` javascript
//Discover devices on the network

const discovery = require('./lib/moxa-discovery.js');

let moxaList = discovery.discoverMoxas(); // default port is 50001

console.log(moxaList);

/*
moxaList= [
   {ip: '192.168.1.20', ports: [4001, 4002]},
   {ip: '192.168.1.23', ports: [4001, 4002, 4003, 4004]}
]
*/
```

## Comissioning

### **discoverMoxas()**

Returns an array of objects containing the moxa servers connected on the network. It is an async function.

Return example:

``` javascript
moxaList= [
   {ip: '192.168.1.20', ports: [4001, 4002]},
   {ip: '192.168.1.23', ports: [4001, 4002, 4003, 4004]}
]
```
