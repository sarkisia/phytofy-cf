import React, { Component } from 'react';
import styled, { css } from 'styled-components';

import Icon from '../atoms/Icons';

const LineItem = styled.li`
  display: flex;
  cursor: pointer;
  height: 22px;

  &:not(:last-child) {
    margin-bottom: 20px;
  }

  ${props => props.isActive && css`
    &:before {
      position: absolute;
    	left: 0;
    	width: 5px;
    	height: 20px;
    	background-color: var(--primaryColor, #FF6600);
    	content: "";
    }
  `}
`

const Link = styled.a`
  width: 100%;
  font-weight: var(--bold, 700);
  color: var(--textColor, #424242);
  text-decoration: none;
`

export default class SecondaryMenuLink extends Component {
  render() {
    return (
			<LineItem isActive={this.props.isActive} onClick={this.props.onClick}>
				<Link isActive={this.props.isActive}>
					<span>{this.props.name}</span>
				</Link>
			</LineItem>
    );
  }
}
