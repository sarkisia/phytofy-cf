const discovery = require('../../lib/moxa-discovery.js');
const rs485 = require('../../lib/rs485-protocol.js');
const utils = require('../../src/moxa-discovery_utils.js');
const defines = require('../../src/defines.js');
const logger = require('../../src/logger.js');
const compute = require('../../src/compute.js');

async function testCommissioning(){
  //moxaList = await discovery.discoverMoxas();

  //logger.verbose(`[TEST] listing discovered moxas:`)
  //console.log(moxaList);

  const comm = new rs485.RS485protocol('192.168.1.119');

  comm.moxaList = [
    { ip: '192.168.1.191',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:166' },
  { ip: '192.168.1.193',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:241' }
  ];

  let addedFixtures = await comm.commissionFixtures('0:144:232:111:186:166', [4001]);
  console.log(comm.fixtureMap);

};

async function uncommission(serialNum, port, mac){
  const comm = new rs485.RS485protocol('192.168.1.119');

  comm.moxaList = [
    { ip: '192.168.1.191',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:166' },
  { ip: '192.168.1.193',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:241' }
  ];

  await comm.openConnection(port, mac);

  let ack = await comm.setSlaveAddress(serialNum, 3, 1);
  console.log(ack);

  await comm.closeConnection();
};

async function singleShotCommands(serialNum, port, mac, slave=1){

  const comm = new rs485.RS485protocol('192.168.1.119');

  comm.moxaList = [
    { ip: '192.168.1.191',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:166' },
  { ip: '192.168.1.193',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:241' }
  ];

  comm.fixtureMap = [
    {serial_num: serialNum, slave_addr: slave, moxa_mac: mac, port: port}
  ];

  let con = await comm.openConnection(port, mac);
  logger.debug(`connection: ${con}`);
  //let resp0 = await comm.setIlluminanceConfiguration(serialNum, [1.0, 1.0,1.0, 1.0,1.0, 1.0]);
  //console.log(resp0);

  //let resp = await comm.getFixtureInfo(serialNum);
  //let resp = await comm.getLEDstate(serialNum);
  //let resp = await comm.getIlluminanceConfiguration(serialNum);
  //console.log(resp);

  //let resp10 = await comm.resumeScheduling(serialNum);
  //let resp10 = await comm.stopScheduling(serialNum);
  //console.log(resp10);

  let resp2 = await comm.getScheduleState(serialNum);
  //let resp2 = await comm.getSchedule(serialNum, 33);
  console.log(resp2);

  //let resp8 = await comm.setSchedule(serialNum, 33, 1535646403,1535646643, [0,5,10,10,0,0]);
  //console.log(resp8);
  //let resp9 = await comm.deleteAllSchedules(serialNum);
  //let resp9 = await comm.deleteSchedule(serialNum, 101);
  //console.log(resp9);
  //let resp3 = await comm.getScheduleCount(serialNum);
  //console.log(resp3);

  /*let epoch = 1535636070;
  logger.verbose(`sending epoch: ${epoch}`);
  let resp5 = await comm.setTimeReference(serialNum, epoch);
  console.log(resp5);*/

  //let resp4 = await comm.getTimeReference(serialNum);
  //console.log(resp4);

  //let resp6 = await comm.getModuleTemperature(serialNum);
  //console.log(resp6);

  //let resp7 = await comm.setLEDrt(serialNum, [0, 0, 0, 0, 0, 0]);
  //console.log(resp7);

  //let resp11 = await comm.getSlaveAddress(serialNum);
  //let resp11 = await comm.setGroupId(serialNum, 101);
  //let resp11 = await comm.getGroupId(serialNum);
  //console.log(resp11);

  await comm.closeConnection();

};

//uncommission(1004, 4001, '0:144:232:111:186:166');
//uncommission(1005, 4001, '0:144:232:111:186:166');

//testCommissioning();

singleShotCommands(1004, 4001, '0:144:232:111:186:166', 1);
//singleShotCommands(1005, 4001, '0:144:232:111:186:166', 3);
