// @flow
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { I18nextProvider } from 'react-i18next';
import Routes from '../routes';

import i18n from '../../i18n';

type Props = {
  store: {},
  history: {}
};

export default class Root extends Component<Props> {
  render() {
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <I18nextProvider i18n={ i18n }>
            <Routes history={this.props.history} />
          </I18nextProvider>
        </ConnectedRouter>
      </Provider>
    );
  }
}
