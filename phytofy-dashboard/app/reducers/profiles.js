import { listReducer } from './helpers';

const defaultState = { from: 1, objects: [], objectsAll: [], sortAttribute: 'name', sortType: 'orderAsc', hasPrev: false, hasNext: false, selected: [], filters: {} };

export default function profiles(state = defaultState, action) {
    let newState = listReducer('PROFILES', state, action);

    if(newState) return newState;

    switch (action.type) {
        case "PROFILES_CREATE":
            return {
                ...state,
                profileObj: action.profileObj,
                validation: action.validation
            }
        case "PROFILES_SUBMIT_CREATE":
            return {
                ...state,
                profileObj: action.profileObj,
                validation: action.validation,
                selected: []
            }
        case "PROFILES_EDIT":
            return {
                ...state,
                profileObj: action.profileObj,
                editObjId: action.editObjId,
                validation: action.validation
            }
        case "PROFILES_SUBMIT_EDIT":
            return {
                ...state,
                profileObj: action.profileObj,
                editObjId: action.editObjId,
                validation: action.validation,
                selected: []
            }
        case "PROFILES_CANCEL_FORM":
            return {
                ...state,
                profileObj: action.profileObj,
                editObjId: action.editObjId,
                validation: action.validation
            }
        default:
            return state;
    }
}
