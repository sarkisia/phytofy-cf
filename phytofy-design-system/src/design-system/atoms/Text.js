import React, { Component } from 'react';
import styled from 'styled-components';

const Text = styled.span`
  font-size: 1rem;
  color: var(--textColor, #424242);
`;

export default class extends Component {
	render() {
		return (
			<Text>{this.props.children}</Text>
		)
	}
}
