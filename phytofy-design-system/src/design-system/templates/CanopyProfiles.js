import React, { Component } from 'react';

import Topbar from '../organisms/Topbar'
import TopbarTab from '../molecules/TopbarTab'
import FormGroup from '../molecules/FormGroup'
import Content from '../organisms/Content'
import CardTable from '../organisms/CardTable';
import Table from '../organisms/Table';
import TableHeaderCell from '../molecules/TableHeaderCell';
import TableRow from '../molecules/TableRow';
import TableRowOptions from '../molecules/TableRowOptions';
import TableData from '../molecules/TableData';
import Icon from '../atoms/Icons';
import Checkbox from '../atoms/Checkbox';

/* Card Action Buttons */

const actionButtons = [
  { icon: 'add', onclick: (ids) => { console.log("click") } },
];

/* Table Content */

const header = [
  <TableHeaderCell selectable><Checkbox /></TableHeaderCell>,
  <TableHeaderCell orderAsc>Name</TableHeaderCell>,
  <TableHeaderCell config>Config</TableHeaderCell>,
  <TableHeaderCell measure>Lenght</TableHeaderCell>,
  <TableHeaderCell measure>Width</TableHeaderCell>,
  <TableHeaderCell measure>Height</TableHeaderCell>,
  <TableHeaderCell measure>△L</TableHeaderCell>,
  <TableHeaderCell status>Status</TableHeaderCell>,
];

class CanopyProfiles extends Component {
	render() {
		return (
      <div>
        <Topbar>
          <TopbarTab name="cenas" />
          <TopbarTab name="cenas" />
        </Topbar>

        <Content>
          <CardTable title="Canopy Profiles" actions={actionButtons} goto="1" from="1" to="10" of="100" >
    				<Table header={header}>
    					<TableRow>
    						<TableData selectable>
    							<Checkbox />
    						</TableData>
    						<TableData>Profile #01</TableData>
    						<TableData config>2V</TableData>
    						<TableData measure>000 cm</TableData>
    						<TableData measure>000 cm</TableData>
    						<TableData measure>000 cm</TableData>
    						<TableData measure>00 cm</TableData>
    						<TableData status>
    							<Icon fill="#009D3D" icon="checkedCircle" />
    						</TableData>
    						<TableRowOptions>
    							<button>
    								<Icon icon="edit" />
    							</button>
    							<button>
    								<Icon icon="duplicate" />
    							</button>
    							<button>
    								<Icon icon="delete" />
    							</button>
    						</TableRowOptions>
    					</TableRow>
    				</Table>
    			</CardTable>
        </Content>
      </div>
		)
	}
}

export default CanopyProfiles;
