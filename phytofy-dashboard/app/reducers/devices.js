import { listReducer } from './helpers';

const defaultState = { objects: [], moxas: [], allFixtures: [], fixtures: [], moxaDisplay: [], sortAttribute: 'serial_num', sortType: 'orderAsc', hasPrev: false, hasNext: false, from: 1};

export default function devices(state = defaultState, action) {
    let newState = listReducer('DEVICES', state, action);

    if(newState) return newState;

    switch (action.type) {
        case "DEVICES_GET_MOXAS":
            return {
                ...state,
                moxas: action.moxas,
                moxaDisplay: action.moxaDisplay,
                comm: action.comm
            }
        case "DEVICES_GET_FIXTURES":
            return {
                ...state,
                fixtures: action.fixtures,
                allFixtures: action.allFixtures,
                moxaDisplay: action.moxaDisplay,
                moxaMac: action.moxaMac,
                count: action.count,
                from: action.from,
                to: action.to,
                hasNext: action.hasNext,
                hasPrev: action.hasPrev,
            }
        case "DEVICES_GET_ALL_FIXTURES":
            return {
                ...state,
                allFixtures: action.allFixtures,
            }
        case "DEVICES_GET_TEMPERATURES":
            return {
                ...state,
                fixtures: action.fixtures
            }
        default:
            return state;
    }
}
