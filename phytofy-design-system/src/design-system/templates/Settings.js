import React, { Component } from 'react'

import Topbar from '../organisms/Topbar'
import CheckboxGroup from '../molecules/CheckboxGroup'
import FormGroup from '../molecules/FormGroup'
import Content from '../organisms/Content'
import Card from '../organisms/Card'
import Title from '../atoms/Title'
import Paragraph from '../atoms/Paragraph'
import Button from '../atoms/Button'

const status = [
  { value: 'all', name: 'All' },
  { value: 'active', name: 'Active' },
  { value: 'inactive', name: 'Inactive' },
];

class CanopiesGridList extends Component {
	render() {
		return (
      <div>
        <Topbar />
        <Content cardList>
          <Card title="Units of Measure">
            <FormGroup type="select" label="Units" options={status} />
            <FormGroup type="select" label="Irradiance" options={status} />
            <FormGroup type="select" label="Irradiance Map Colors" options={status} />
            <FormGroup type="select" label="Results per Page" options={status} />
            <Button state="success" name="Save" onClick="console.log('click')" />
          </Card>

          <Card title="Export">
            <CheckboxGroup>
              <FormGroup type="checkbox" checkboxLabel="Canopy Profiles" />
              <FormGroup type="checkbox" checkboxLabel="Recipes" />
            </CheckboxGroup>
            <Button state="success" name="Download" onClick="console.log('click')" />
          </Card>

          <Card title="Import">
            <FormGroup type="file" label="Maximum file size is 1GB" />
            <Button state="success" name="Upload" onClick="console.log('click')" />
          </Card>

          <Card state="danger" title="Reset">
            <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit vivamus pharetra sed elit eget sollicitudin.</Paragraph>
            <Button state="danger" name="Reset" onClick="console.log('click')" />
          </Card>
        </Content>
      </div>
		)
	}
}

export default CanopiesGridList;
