import React,{Component} from 'react';
import { translate } from 'react-i18next';

import CanopyConfig from 'phytofy-design-system/src/design-system/atoms/CanopyConfig';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import FormActions from 'phytofy-design-system/src/design-system/molecules/FormActions';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import FormContainer from '../containers/FormContainer';
import { getCanopyConfigsForSelect, getImageForConfig, configsWithDeltaW, configsWithDeltaL } from '../models/Profile';

const CanopyProfileForm = class extends Component {
    constructor(props){
        super();
        this.props = props;
        this.state = {...this.props.obj}
        this.configChangeHandler = this.configChangeHandler.bind(this);
    }

    getData() {
        return this._form.getData();
    }

    configChangeHandler(event) {
        this.setState({config: event.target.value});
    }

    componentDidUpdate(prevProps){
        if(this.props.obj != prevProps.obj){
            this.setState({...this.props.obj})
        }
    }

    render() {
        return (
            <FormContainer obj={this.props.obj} validation={this.props.validation} actions={this.props.actions} ref={(ref) => this._form = ref}>
                <CanopyConfig config={getImageForConfig(this.state.config)} />
                <FormGroup type="select" label="Orientation" name="config" options={getCanopyConfigsForSelect()} onChange={this.configChangeHandler} />
                <FormGroup type="text" label="Name" name="name" placeholder="" error={this.props.validation.name && this.props.validation.name.message}/>
                <FormGroup type="number" label={`Length (${this.props.lengthUnit})`} name="length" placeholder="Canopy length" error={this.props.validation.length && this.props.validation.length.message}/>
                <FormGroup type="number" label={`Width (${this.props.lengthUnit})`} name="width" placeholder="Canopy width" error={this.props.validation.width && this.props.validation.width.message}/>
                <FormGroup type="number" label={`Height (${this.props.lengthUnit})`} name="height" placeholder="Canopy height" error={this.props.validation.height && this.props.validation.height.message}/>
                <FormGroup type="number" label={`△ Length (${this.props.lengthUnit})`} name="deltaLength" placeholder="Fixture spacing length" hidden={!configsWithDeltaL.find((el) => { return el == this.state.config })} error={this.props.validation.deltaLength && this.props.validation.deltaLength.message}/>
                <FormGroup type="number" label={`△ Width (${this.props.lengthUnit})`} name="deltaWidth" placeholder="Fixture spacing width" hidden={!configsWithDeltaW.find((el) => { return el == this.state.config })} error={this.props.validation.deltaWidth && this.props.validation.deltaWidth.message}/>
            </FormContainer>
        )
    }
}

export default CanopyProfileForm;