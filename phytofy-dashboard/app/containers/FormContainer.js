import React,{Component} from 'react';
import ReactDOM from 'react-dom';

import Form from 'phytofy-design-system/src/design-system/organisms/Form';
import FormActions from 'phytofy-design-system/src/design-system/molecules/FormActions';

const FormContainer = class extends Component {
    constructor(props){
        super();
        this.props = props;
        this.state = {...props.obj};
        this.getHandleInputChange = this.getHandleInputChange.bind(this);
        this.getData = this.getData.bind(this);
    }

    componentDidUpdate(prevProps){
        if(this.props.obj != prevProps.obj){
            this.setState({...this.props.obj})
        }
    }

    getHandleInputChange(handler) {
        return (event) => {
            const target = event.target;
            const name = target.name;
            let value;
            switch(target.type) {
                case 'checkbox':
                    value = target.checked;
                    break;
                case 'number':
                    if(!isNaN(parseFloat(target.value))) value = parseFloat(target.value);
                    else value = target.value;
                    break;
                default:
                    value = target.value;
            }
            this.setState({
              [name]: value
            });

            if(handler) {
                handler(event);
            }
        }
    }   

    getData() {
        return this.state;
    }

    render() {
        const { children } = this.props;

        const childrenWithProps = this.props.children.map((child) => {
            if(child.props.type) return React.cloneElement(child, { value: this.state[child.props.name], onChange: this.getHandleInputChange(child.props.onChange) });
            return React.cloneElement(child);
        });

        return (
            <Form>
                { childrenWithProps }
                <FormActions>
                    { this.props.actions }
                </FormActions>
            </Form>
        )
    }
}

export default FormContainer;