import React, { Component } from 'react';

import Topbar from '../organisms/Topbar'
import Content from '../organisms/Content'
import CardCanopy from '../organisms/CardCanopy';

import Config1H from '../../images/1h.svg';
import Config1V from '../../images/1v.svg';
import Config2H from '../../images/2h.svg';
import Config2V from '../../images/2v.svg';
import Config2x2H from '../../images/2x2h.svg';
// import Config3V from '../../images/3v.svg';
// import Config4V from '../../images/4v.svg';

/* Tabs */

const tabList = [
    { name: 'Tab 1', location: '/tab1' },
    { name: 'Tab 2', location: '/tab2' },
    { name: 'Tab 3', location: '/tab3' },
    { name: 'Tab 4', location: '/tab4' },
];

/* Card Values */

const values = [
    { icon: 'fixture', value: '100', unit: '%', legend: 'μmol/m<sup>2</sup>/s' },
    { icon: 'time', value: '2', unit: '', legend: 'Days Left' },
    { icon: 'irradiance', value: '100', unit: '%', legend: 'μmol/m<sup>-2</sup>/d<sup>-1</sup>' }
];

class CanopiesGridList extends Component {
    render() {
        return (
            <div>
                <Topbar values={tabList} />

                <Content cardList>
                    <CardCanopy title="Canopy #01" configImage={Config1H} configDesc="1H" values={values} />
                    <CardCanopy title="Canopy #02" configImage={Config1V} configDesc="1V" values={values} />
                    <CardCanopy title="Canopy #03" configImage={Config2H} configDesc="2H" values={values} />
                    <CardCanopy title="Canopy #04" configImage={Config2V} configDesc="2V" values={values} />
                    <CardCanopy title="Canopy #05" configImage={Config2x2H} configDesc="2x2H" values={values} />
                </Content>
            </div>
        )
    }
}

export default CanopiesGridList;
