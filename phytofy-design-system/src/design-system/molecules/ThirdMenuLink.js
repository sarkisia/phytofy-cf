import React, { Component } from 'react';
import styled, { css } from 'styled-components';

import Icon from '../atoms/Icons';

const LineItem = styled.li`
  display: flex;
  cursor: pointer;

  &:not(:last-child) {
    margin-bottom: 20px;
  }

  ${props => props.isActive && css`
    &:before {
      position: absolute;
    	left: 0;
    	width: 5px;
    	height: var(--menuLinkHeight, 40px);
    	background-color: var(--primaryColor, #FF6600);
    	content: "";
    }
  `}
`

const Link = styled.a`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  height: var(--menuLinkHeight, 40px);
  font-weight: var(--bold, 700);
  color: var(--textColor, #424242);
  text-decoration: none;

  > div {
    ul {
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;

      li {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        list-style: none;
        font-weight: var(--regular, 400);
        color: var(--secondaryColor, #87888A);

        &:not(:last-child) {
          margin-right: calc(var(--spacing, 30px) / 2);
        }

        svg {
          margin-right: 5px;
          width: auto;
          height: 15px;
          fill: var(--secondaryColor, #87888A);
        }
      }
    }
  }
`

export default class SecondaryMenuLink extends Component {
  render() {
    return (
			<LineItem isActive={this.props.isActive} onClick={this.props.onClick}>
				<Link to={this.props.to} isActive={this.props.isActive}>
					<span>{this.props.name}</span>
          <div>
            <ul>
              <li>
                <Icon icon="ports" />
                <span>{this.props.ports}</span>
              </li>
              <li>
                <Icon icon="fixture" />
                <span>{this.props.fixtures}</span>
              </li>
            </ul>
          </div>
				</Link>
			</LineItem>
    );
  }
}
