import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { BrowserRouter as Router, Route, Link } from "react-router-dom"

import Icon from '../atoms/Icons';

const LineItem = styled.li`
    display: flex;
    cursor: pointer;

    &:not(:last-child) {
        margin-bottom: 20px;
    }

    ${props => props.isActive && css`
        &:before {
            position: absolute;
            left: 0;
            width: 5px;
            height: var(--menuLinkHeight, 40px);
            background-color: var(--primaryColor, #FF6600);
            content: "";
        }
    `}

    a {
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: var(--menuLinkHeight, 40px);
        font-weight: var(--bold, 700);
        color: var(--secondaryColor, #87888A);
        text-decoration: none;

        svg {
            height: var(--menuLinkHeight, 40px);
            width: auto;
            fill: var(--secondaryColor, #87888A);
        }

        ${props => props.isActive && css`
            color: var(--primaryColor, #FF6600);

            > svg {
                fill: var(--primaryColor, #FF6600);
            }
        `}
    }
`

export default class MenuLink extends Component {
    render() {
        return (
            <LineItem isActive={this.props.isActive} onClick={this.props.onClick}>
                <Link to={this.props.to} isActive={this.props.isActive}>
                    <Icon icon={this.props.icon} />
                </Link>
            </LineItem>
        );
    }
}
