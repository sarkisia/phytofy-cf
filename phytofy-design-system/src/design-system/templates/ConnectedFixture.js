import React, { Component } from "react";

import Form from "../organisms/Form";
import FormGroup from "../molecules/FormGroup";
import FormGroupList from "../molecules/FormGroupList";
import ListItem from "../atoms/ListItem";

class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = { isRightSidebarClosed: true, isSecondaryMenuClosed: true };
    }

    render() {
        return (
            <div>
                <Form>
                    <FormGroup
                        type="search"
                        label="Fixture"
                        placeholder="Search Fixture"
                        name="Search Fixture"
                        value={this.state.text}
                        error=""
                        onChange={this.handleInputChange}
                    />

                    <FormGroupList label="Results">
                        <ListItem value="Canopy #30" onClick={console.log("click")} />
                        <ListItem value="Canopy #20" onClick={console.log("click")} />
                        <ListItem value="Canopy #10" onClick={console.log("click")} />
                        <ListItem value="Canopy #40" onClick={console.log("click")} />
                        <ListItem type="add" value="Add new" onClick={console.log("click")} />
                    </FormGroupList>
                </Form>
            </div>
        );
    }
}

export default EditProfile;
