import React, { Component } from 'react'
import styled from 'styled-components'

import Label from '../atoms/Label'

const FormGroupList = styled.div ``
const ListItems = styled.ul `
    margin-top: 8px;
`

export default class extends Component {
    render() {
        return (
            <FormGroupList>
                <Label label={this.props.label} />

                <ListItems>
                    {this.props.children}
                </ListItems>
            </FormGroupList>
        );
    }
}
