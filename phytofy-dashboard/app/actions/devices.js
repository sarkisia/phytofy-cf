import discovery from 'phytofy-drivers/lib/moxa-discovery';
import { RS485protocol } from 'phytofy-drivers/lib/rs485-protocol';
import Notifications from 'react-notification-system-redux';

import { getPageSize } from '../models/Settings';
import { makeSortAction, makePageBack, makePageNext, makePageGoTo, getPaginationInfo, runCommandOnFixtures } from './helpers';
import Moxa from '../models/Moxa';
import Fixture from '../models/Fixture';

const queryDevices = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
    let canopies;
  
    let sortVal = sortType == 'orderAsc' ? 1 : -1;
    let sortObj = {};
    if(sortAttribute){
      sortObj = {[sortAttribute]: sortVal}
    }
  
    let pageSize = await getPageSize();
  
    // let skip = await ( page ? (page - 1) * pageSize : 0);
    // let limit = await ( page ? (skip + pageSize) : pageSize);
    let skip = from - 1;
    let limit = pageSize;
    if(ignoreLimit){
      limit = 0;
    }
  
    if(Object.keys(sortObj).indexOf('name') == -1){
      sortObj['name'] = 1;
    }
  
    try {
      canopies = await Canopy.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
    } catch(err) {
      console.error(err);
      return [];
    }
  
    return canopies;
}


export const getObjects = (obj) => {
  return async (dispatch, getState) => {
    await dispatch({
      type: 'DEVICES_GET'
    })
  }
};

const createMoxaDisplay = async (moxas) => {
  let moxaDisplay = []; 
    for(let i in moxas) {
      let moxa = moxas[i];
      let nFixtures = (await Fixture.cfind({moxa_mac: moxa.mac}).exec()).length;
      moxaDisplay.push({
        name: moxa.ip,
        ports: moxa.ports.length,
        fixtures: nFixtures,
        location: `/setup/devices/${moxa.mac}`
      });

  }

  return moxaDisplay;
}

export const getMoxas = (history) => {
  return async (dispatch, getState) => {
    let moxaList = await discovery.discoverMoxas();

    await Moxa.remove({}, {multi: true});
    await Moxa.insert(moxaList);

    let moxas = await Moxa.cfind({}).sort({'ip': 1}).exec()

    if(moxaList.length > 0 && history.location.pathname.split('/')[1] == 'setup' && history.location.pathname.split('/')[2] == 'devices'){
      history.replace(`/setup/devices/${moxas[0].mac}`)
      dispatch(getFixtures(moxas[0].mac));
    }

    let moxaDisplay = await createMoxaDisplay(moxas);
    // let fixtures = await Fixture.cfind({}).execute();

    let comm = getState().devices.comm;

    if(!comm) {
      comm = new RS485protocol('192.168.1.194');
      comm.fixtureMap = await Fixture.cfind({}).exec();
    }

    comm.moxaList = moxas;
    
    if(moxas.length == 0){
      dispatch(Notifications.warning({
        title: 'Connection warning',
        message: `No Moxa servers found. `
      }));
    }

    await dispatch({
      type: 'DEVICES_GET_MOXAS',
      moxas,
      moxaDisplay,
      comm
    })
  }
}

const queryFixtures = async (sortAttribute, sortType, from, moxaMac, ignoreLimit) => {
  let fixtures;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();
  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    fixtures = await Fixture.cfind({moxa_mac: moxaMac}).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return fixtures;

}

export const getTemperatures = (mac) => {
  return async (dispatch, getState) => {
    let comm = getState().devices.comm;

    let tmpFixtures = getState().devices.fixtures.sort(function(a,b) {return (a.port > b.port) ? 1 : ((b.port > a.port) ? -1 : 0);} ); 

    let fixturesTemperatures = {};
    let fixturesInfo = {};

    
    await runCommandOnFixtures(tmpFixtures, comm, dispatch, async (fixture) => {
      let temperatures = await comm.getModuleTemperature(fixture.serial_num);
      let sum = 0;
      let validTempsCount = 0.0;

      if(Array.isArray(temperatures)) {
        temperatures.forEach((temp) => {
          if(isNaN(temp)) return;
  
          sum += temp;
          validTempsCount += 1;
        })

        let avg = '-';
        if(validTempsCount > 0) avg = (sum/validTempsCount).toFixed(2);
        fixturesTemperatures[fixture.serial_num] = avg;  
      }

      let info = await comm.getFixtureInfo(fixture.serial_num);
      fixturesInfo[fixture.serial_num] = info;
    })

    let fixtures = [];
    
    getState().devices.fixtures.forEach((fixture) => {
      let tmp;
      
      if(fixturesTemperatures[fixture.serial_num] != undefined){
        tmp = {...fixture, temperature: fixturesTemperatures[fixture.serial_num], status: 0};
      } else {
        tmp = {...fixture, temperature: '-', status: 2};
      }

      if(fixturesInfo[fixture.serial_num] != undefined){
        tmp = {...tmp, firmware: fixturesInfo[fixture.serial_num].FW, hardware: fixturesInfo[fixture.serial_num].HW};
      } else {
        tmp = {...tmp, firmware: '-', hardware: '-', status: 2};
      }

      fixtures.push(tmp);
    })

    await dispatch({
      type: 'DEVICES_GET_TEMPERATURES',
      fixtures,
    })
  }
}

export const getFixtures = (moxaMac) => {
  return async (dispatch, getState) => {
    let { sortAttribute, sortType, from } = getState().devices;
    if(from == 0) from = 1;
    if(!moxaMac) moxaMac = getState().devices.moxaMac;

    let count = (await Fixture.cfind({moxa_mac: moxaMac}).exec()).length;
    let moxas = await Moxa.cfind({}).sort({'ip': 1}).exec()
    let moxaDisplay = await createMoxaDisplay(moxas);

    let fixtures = await queryFixtures(sortAttribute, sortType, from, moxaMac);

    fixtures = fixtures.map((fixture) => {
      return {...fixture, temperature: '-', firmware: '-', hardware: '-', status: 1}
    })

    let allFixtures = await Fixture.cfind({}).exec();

    await dispatch({
      type: 'DEVICES_GET_FIXTURES',
      fixtures,
      allFixtures,
      moxaDisplay,
      moxaMac,
      count,
      ...await getPaginationInfo(from, count)
    })

    await dispatch(getTemperatures(moxaMac));
  }
}

export const getAllFixtures = () => {
  return async (dispatch, getState) => {
    let allFixtures = await Fixture.cfind({}).exec();
    
    await dispatch({
      type: 'DEVICES_GET_ALL_FIXTURES',
      allFixtures
    })
  };
}

export const commissionFixtures = (mac) => {
  return async (dispatch, getState) => {

    let moxaList = await discovery.discoverMoxas();
    let filtered = moxaList.filter((moxa) => {
      return moxa.mac == mac;
    })

    if(filtered.length == 0) {
      dispatch(Notifications.error({
        title: "Cannot reach Moxa server",
        message: `Impossible to connect to Moxa server`
      }));
      return;
    }

    let comm = getState().devices.comm;
    let moxaPorts = [];
    getState().devices.moxas.map(async (moxa) => {
      if(moxa.mac == mac) moxaPorts = moxa.ports;
    })

    await comm.closeConnection();
    
    // to remove
    await Fixture.remove({moxa_mac: mac}, {multi: true});
    comm.fixtureMap = await Fixture.cfind().exec();
    
    let nFixtures = await comm.commissionFixtures(mac, moxaPorts);
    await Fixture.remove({moxa_mac: mac}, {multi: true});
    await Fixture.insert(comm.fixtureMap.filter((obj) => {return obj.moxa_mac == mac}).map((obj) => {return {...obj, name: obj.serial_num.toString()}}));
    dispatch(getFixtures(mac));
  }
}

export const sort = makeSortAction('DEVICES_SORT', 'devices', getFixtures);
export const pageBack = makePageBack('DEVICES_PAGE_BACK', 'devices', getFixtures, Fixture);
export const pageNext = makePageNext('DEVICES_PAGE_NEXT', 'devices', getFixtures, Fixture);
export const goto = makePageGoTo('DEVICES_PAGE_GOTO', 'devices', getFixtures, Fixture);
