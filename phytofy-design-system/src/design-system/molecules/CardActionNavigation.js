import React, { Component } from 'react';
import styled from 'styled-components';

import ActionButton from '../atoms/ActionButton';

const Navigation = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  span {
    margin-right: var(--spacing, 30px);
  }
`;

export default class CardActionNavigation extends Component {
  render() {
    return (
      <div>
      { this.props.from != null && this.props.to != null && this.props.of != null && (
        <Navigation>
          <span>{this.props.from}-{this.props.to} of {this.props.of}</span>
          <ActionButton icon="arrowLeft" onClick={this.props.pageBack}/>
          <ActionButton icon="arrowRight" onClick={this.props.pageNext} />
        </Navigation>
      )}
      </div>
    );
  }
}
