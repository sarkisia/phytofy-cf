// @flow
import validator from 'validator';
import Notifications from 'react-notification-system-redux';

import Recipe, {RecipeFormValidator} from '../models/Recipe';
import { getPageSize } from '../models/Settings';
import Schedule from '../models/Schedule';

import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
         makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext, makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo } from './helpers';
import { openSidebar, closeSidebar } from './sidebar';
import FormValidator from '../utils/FormValidator';

const defaultRecipe = {name: ""};

const queryRecipes = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let recipes;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();
  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    recipes = await Recipe.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return recipes;
}

export const getObjects = makeGetObjectsAction('RECIPES_GET', 'recipes', Recipe, queryRecipes);
export const getObjectsAll = makeGetObjectsAllAction('RECIPES_GET_ALL', 'recipes', Recipe, queryRecipes);
export const sort = makeSortAction('RECIPES_SORT', 'recipes', getObjects);
export const addFilter = makeAddFilterAction('RECIPES_ADD_FILTER', 'recipes', getObjects);
export const removeFilter = makeRemoveFilterAction('RECIPES_REMOVE_FILTER', 'recipes', getObjects);
export const pageBack = makePageBack('RECIPES_PAGE_BACK', 'recipes', getObjects, Recipe);
export const pageNext = makePageNext('RECIPES_PAGE_NEXT', 'recipes', getObjects, Recipe);
export const select = makeSelectObject('RECIPES_SELECT', 'recipes');
export const deselect = makeDeselectObject('RECIPES_DESELECT', 'recipes');
export const selectAll = makeSelectAll('RECIPES_SELECT_ALL', 'recipes');
export const deselectAll = makeDeselectAll('RECIPES_DESELECT_ALL');
export const goto = makePageGoTo('RECIPES_PAGE_GOTO', 'recipes', getObjects, Recipe);


export const create = (obj) => {
  return async (dispatch, getState) => {
    if(obj){
      obj.name = `Copy of ${obj.name}`;
    }

    let recipeObj = obj ? obj : {...defaultRecipe};
    await dispatch({
      type: 'RECIPES_CREATE',
      recipeObj: recipeObj,
      validation: {}
    })
    dispatch(openSidebar('CREATE_RECIPE_FORM', 'Create', 'New recipe', 'xl'));
  }
};

export const submitCreate = (obj) => {
  return async (dispatch, getState) => {
    let validations = await RecipeFormValidator.validate(obj);

    if(validations.isValid){
      try {
        await Recipe.insert(obj);
      } catch(err){
        console.error(err);
      }
      obj = {...defaultRecipe};
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Recipe created',
        message: `Recipe ${obj.name} created with success.`
      }));
    }

    dispatch(getObjects())

    dispatch({
      type: 'RECIPES_SUBMIT_CREATE',
      recipeObj: obj,
      validation: validations,
    })
  }
}

export const duplicate = (obj) => {
  return async (dispatch, getState) => {
    let newObj = {...obj};
    delete newObj._id;

    dispatch(create(newObj));
  }
} 

export const edit = (obj) => {
  return async (dispatch, getState) => {
    let recipeId = obj._id;

    await dispatch({
      type: 'RECIPES_EDIT',
      recipeObj: obj,
      editObjId: obj._id,
      validation: {}
    })

    dispatch(openSidebar('EDIT_RECIPE_FORM', 'Edit', `${obj.name}`, 'xl'));
  }
}

export const submitEdit = (obj) => {
  return async (dispatch, getState) => {
    let count = await Schedule.count({recipeId: obj._id});

    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot edit ${obj.name}!`,
        message: `The recipe ${obj.name} has one or more schedules associated to it.`,
      }));

      return;
    }

    let validations = await RecipeFormValidator.validate(obj, getState().recipes.recipeObj);

    if(validations.isValid){
      try {
        await Recipe.update({_id: getState().recipes.editObjId}, obj);
      } catch(err){
        console.error(err);
      }
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Recipe edited',
        message: `Recipe ${obj.name} edited with success.`
      }));
    }

    dispatch(getObjects())
    dispatch({
      type: 'RECIPES_SUBMIT_EDIT',
      recipeObj: obj,
      editObjId: validations.isValid ? null : getState().recipes.editObjId,
      validation: validations
    })
  }
}

export const cancelForm = () => {
  return async (dispatch, getState) => {
    dispatch(closeSidebar())
    let obj = {...defaultRecipe};
    dispatch({
      type: 'RECIPES_CANCEL_FORM',
      recipeObj: obj,
      editObjId: null,
      validation: {}
    })
  }
}

export const confirmDelete = (obj) => {
  return async (dispatch, getState) => {
    try {
      await Recipe.remove({_id: obj._id})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteObj = (obj) => {
  return async (dispatch, getState) => {
    let recipeId = obj._id;
    let count = await Schedule.count({recipeId: recipeId});
    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot delete ${obj.name}!`,
        message: `The recipe ${obj.name} has one or more schedules associated to it.`,
      }));
    } else {
      dispatch(Notifications.warning({
        title: `Delete ${obj.name}`,
        message: `Are you sure you want to delete ${obj.name}?`,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDelete(obj))
          }
        }
      }));
    }
  };
}

export const confirmDeleteMultiple = () => {
  return async (dispatch, getState) => {
    try {
      obj = await Recipe.remove({_id: {$in: getState().recipes.selected}}, {multi: true})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteMultiple = () => {
  return async (dispatch, getState) => {
    let count = await Schedule.count({recipeId: {$in: getState().recipes.selected}});
    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot delete recipes!`,
        message: `The selected recipes have one or more schedules associated to them.`
      }));
    } else {
      dispatch(Notifications.warning({
        title: `Delete ${getState().recipes.selected.length} recipes`,
        message: `Are you sure you want to delete ${getState().recipes.selected.length} recipes?`,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDeleteMultiple())
          }
        }
      }));
    }
  }
}
