// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
import FiltersContainer from './FiltersContainer';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as SchedulesActions from '../actions/schedules'; 


const SchedulesContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [] };
  }

  componentDidMount() {
    this.props.schedulesActions.getObjects();
  }

  render() {
    let { t } = this.props;

    const tableAttributes = {
      start_date: {name: t('start_date'), configs: []},
      end_date: {name: t('end_date'), configs: []},
      '_recipe.name': {name: "Recipe", configs: [], transform: (row) => `${row._recipe.name}` }
    }

    let actionButtons = [
      { icon: 'add', onClick: (ids) => { this.props.schedulesActions.create() } },
    ];

    let actionButtonsWhenSelected = [{ icon: 'delete', onClick: () => { this.props.schedulesActions.deleteMultiple() } }];

    // let configFilter = [
    //   {value: -1, name: 'All'},
    //   ...getCanopyConfigsForSelect()
    // ]

    const filters = [
      // {label: t('orientation'), options: configFilter, attribute: 'config'}
    ]

    const rowOptions = [
      {optionMethod: this.props.schedulesActions.edit, iconName: 'edit'},
      {optionMethod: this.props.schedulesActions.duplicate, iconName: 'duplicate'},
      {optionMethod: this.props.schedulesActions.deleteObj, iconName: 'delete'}
    ];

    return (
      <div>
        <FiltersContainer filters={filters} addFilter={this.props.schedulesActions.addFilter} removeFilter={this.props.schedulesActions.removeFilter} />
        <Content>
          {this.props.objects != null &&
            <ListContainer title={t('schedules')} actions={{...this.props.schedulesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
              actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} rowOptions={rowOptions} />
          }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.schedules.sortAttribute ? state.schedules.sortAttribute : null,
    sortType: state.schedules.sortType ? state.schedules.sortType : 'orderAsc',
    objects: state.schedules.objects,
    count: state.schedules.count,
    from: state.schedules.from,
    to: state.schedules.to,
    selected: state.schedules.selected,
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    schedulesActions: {...bindActionCreators(SchedulesActions, dispatch)}

  }
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(SchedulesContainer));