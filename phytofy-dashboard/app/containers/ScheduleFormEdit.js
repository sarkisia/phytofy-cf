import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import * as SchedulesActions from '../actions/schedules';  
import ScheduleForm from './ScheduleForm';

const ScheduleFormEdit = class extends Component {
    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props.obj});
        }
    }

    render() {
        const actions = [
            <Button type="cancel" name="Cancel" onClick={() => this.props.cancelForm() }/>,
            <Button type="success" name="Submit" onClick={() => this.props.submitEdit(this._form.getData())} />
        ]

        return (
            <ScheduleForm actions={actions} recipes={this.props.recipes} validation={this.props.validation} addCanopy={this.props.addCanopy} removeCanopy={this.props.removeCanopy} canopies={this.props.canopies} obj={this.props.obj} ref={(ref) => this._form = ref}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        obj: state.schedules.scheduleObj,
        validation: state.schedules.validation,
        recipes: state.schedules.recipes,
        canopies: state.schedules.canopies
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(SchedulesActions, dispatch);
  }
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ScheduleFormEdit));