// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import CardCanopy from 'phytofy-design-system/src/design-system/organisms/CardCanopy';

import { getTableHeaderCellFromObj } from './helpers';

import { getPageSize, irradianceUnitOptions } from '../models/Settings';
import { getImageForConfig, canopyConfigs } from '../models/Profile';

const GridContainer = class extends Component {

    constructor(props) {
        super(props);
    }

    getCards(data) {
        return data.map((row) => {
            let irradiance = row.irradiance || row.irradiance == 0 ? row.irradiance : '-';
            let test = row.test || row.test == 0 ? row.test : '-';
            let scheduleTime = row.scheduleTime ? row.scheduleTime : '-';
            let scheduleTimeUnit = row.scheduleTimeUnit ? row.scheduleTimeUnit : 'days';

            let values = [
                { icon: 'fixture', value: irradiance, unit: '', legend: irradianceUnitOptions[this.props.settings.irradianceUnit] },
                { icon: 'time', value: scheduleTime, unit: '', legend: scheduleTimeUnit },
                { icon: 'irradiance', value: test, unit: '', legend: 'μmol/m<sup>-2</sup>/d<sup>-1</sup>' }
            ]
            return (
                <CardCanopy title={row.name} configImage={getImageForConfig(row._profile.config)} configDesc={`${row._profile.name} (${canopyConfigs[row._profile.config]})`} values={values} />
            )
        })
    }

    render() {
        return (
            <Content cardList>
                {this.props.objects != null && this.getCards(this.props.objects)}
            </Content>
        )
    }
}

function mapStateToProps(state) {
    return {
      settings: state.settings
    };
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(GridContainer);
