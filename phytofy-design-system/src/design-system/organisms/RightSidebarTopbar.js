import React, { Component } from 'react';
import styled from 'styled-components';

import Label from '../atoms/Label';
import Title from '../atoms/Title';
import Button from '../atoms/Button';

const Topbar = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid var(--topbarBorder, #ECEDED);
    background-color: var(--backgroundColor, #FFFFFF);
    width: 100%;
    height: var(--topBarHeight, 60px);
`

const Header = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    padding: 0 var(--spacing, 30px);
`

const HeaderCol = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;

    button {
        background: transparent;
        border-radius: 0;
        width: 26px;
        height: 26px;
        padding: 0;

        svg {
            fill: var(--sidebarPrimaryColor, #C5C6C8);
        }
    }
`

export default class RightSidebarTopbar extends Component {
    render() {
        const label = this.props.label;
        const title = this.props.title;

        return (
            <Topbar>
                <Header>
                    {
                        label || title ?
                            <HeaderCol>
                                <Label label={this.props.label} />
                                <Title title={this.props.title} />
                            </HeaderCol>
                        : <HeaderCol><Button icon="arrowBack" onClick={this.props.goBack} /></HeaderCol>
                    }
                    <HeaderCol>
                        <Button icon="close" onClick={this.props.closeRightSidebar} />
                    </HeaderCol>
                </Header>
            </Topbar>
        );
    }
}
