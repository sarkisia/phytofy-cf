# Changelog
All notable changes to the OSRAM - Phytofy RL project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [ 0.4.2 ] - 2018-09-14
### Added
- Time validation on recipes forms
- Date validation on schedules forms
- Moved recipe validation when being used so the user can view the recipe

## [ 0.4.1 ] - 2018-09-03
### Added
- Schedules
- Recipes
- Irradiance on Canopies view
- Schedules remaining time on recipes view
- Canopy comissioning
- Realtime control


## [ 0.3.1 ] - 2018-08-27
### Added
- Export layouts
- Import layouts (format to be reviewed)
- Reset database
- Discover Moxas
- Comission Fixtures
- Get features temperatue, hardware viersion and firmware version

### Fixed
- Pagination issue on Canopies
- Delete canopies


## [ 0.2.1 ] - 2018-08-06
### Added
- Devices page layout for testing purposes
- Settings preferences
- Search filters by name

### Changed
- Navigation layout
- Moved canopies creation, edition, deletion to a separated canopy menu within the setup tab

### Fixed
- Removed limit (pagination) from profiles list on Canopies form
- "Configuration" filter should read "Orientation"
- Fixed application logos


## [0.1.1] - 2018-07-24
### Added
- Canopies grid view (with dummy data)
- Go To a specific element on paginated lists
- Max length validation on names fields
- Max length validation on numbers fields
- Support for floats on numbers fields
- Fields description as a placeholder

### Changed
- Renamed "Canopy Profile" to "Layout"
- Renamed "Configuration" to "Orientation"
- Merged "Layout" and "Orientation" on canopies' list 
- No uppercase titles
- Application icon

### Fixed
- Removed useless items from topbar menu on Windows and Linux
- Submiting a form after a validation error was not changing the data
- "Delta" character line height
- Hide delta width/delta length for orientations that doesn't support these values
- Cosmetic issue on "3 vertical" orientation image
- Removed sidebar scrolls on windows