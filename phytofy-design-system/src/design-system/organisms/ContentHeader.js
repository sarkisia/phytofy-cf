import React, { Component } from 'react'
import styled from 'styled-components'

const ContentHeader = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-bottom: var(--spacing, 30px);
    color: var(--secondaryColor, #87888A);

    > div {
        display: flex;
        flex-direction: row;
        align-items: center;

        &:last-child {
            > div {
                &:not(:last-child) {
                    margin-right: var(--spacing, 30px);
                }

                &:last-child {
                    min-width: 178px;
                }
            }
        }

        > div {
            margin-bottom: 0 !important;
        }
    }
`

export default class extends Component {
    render() {
        return (
            <ContentHeader>
                {this.props.children}
            </ContentHeader>
        )
    }
}
