import React, { Component } from 'react'

import Topbar from '../organisms/Topbar'
import Content from '../organisms/Content'
import CardTable from '../organisms/CardTable'
import Table from '../organisms/Table'
import TableHeaderCell from '../molecules/TableHeaderCell'
import TableRow from '../molecules/TableRow'
import TableRowOptions from '../molecules/TableRowOptions'
import TableData from '../molecules/TableData'
import Icon from '../atoms/Icons'
import Checkbox from '../atoms/Checkbox'
import Tag from '../atoms/Tag'

/* Top filter */

const configuration = [
  { value: 'all', name: 'All' },
  { value: '1v', name: '1V' },
  { value: '1h', name: '1H' },
  { value: '2v', name: '2V' },
  { value: '2h', name: '2H' },
  { value: '3v', name: '3V' },
  { value: '2x2h', name: '2x2H' },
  { value: '4v', name: '4V' },
];

const status = [
  { value: 'all', name: 'All' },
  { value: 'active', name: 'Active' },
  { value: 'inactive', name: 'Inactive' },
];

/* Card Action Buttons */

const actionButtons = [
  { icon: 'add', onclick: (ids) => { console.log("click") } },
];

/* Table Content */

const header = [
  <TableHeaderCell selectable><Checkbox /></TableHeaderCell>,
  <TableHeaderCell orderAsc>Recipe</TableHeaderCell>,
  <TableHeaderCell>Canopy</TableHeaderCell>,
];

class CanopiesList extends Component {
	render() {
		return (
      <div>
        <Topbar />

        <Content>
          <CardTable title="Canopies List" actions={actionButtons} goto="1" from="1" to="10" of="100" >
    				<Table header={header}>
    					<TableRow>
    						<TableData selectable>
    							<Checkbox />
    						</TableData>
    						<TableData>Recipe #01</TableData>
    						<TableData>
                  <Tag tag="Canopy #01" />
                  <Tag tag="Canopy #09" />
                  <Tag tag="Canopy #10" />
                </TableData>
    						<TableRowOptions>
    							<button>
    								<Icon icon="edit" />
    							</button>
    							<button>
    								<Icon icon="duplicate" />
    							</button>
    							<button>
    								<Icon icon="delete" />
    							</button>
    						</TableRowOptions>
    					</TableRow>
    				</Table>
    			</CardTable>
        </Content>
      </div>
		)
	}
}

export default CanopiesList;
