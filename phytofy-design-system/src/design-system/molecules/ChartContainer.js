import React, { Component } from "react";
import styled from 'styled-components';

const ChartContainer = styled.div `
    margin-top: var(--spacing, 30px);
    border: 1px solid var(--topbarBorder, #ECEDED);
    border-radius: 4px;
    width: 100%;

    > span {
        position: absolute;
        margin: -11px 0 0 var(--spacing, 30px);
        background-color: var(--backgroundColor, #FFFFFF);
        padding: 0 10px;
        color: var(--topbarBorder, #ECEDED);
    }
`

const Chart = styled.div `
    padding: var(--spacing, 30px);
`

export default class extends Component {
    render() {
        return (
            <ChartContainer>
                <span>{this.props.title}</span>

                <Chart>
                    {this.props.children}
                </Chart>
            </ChartContainer>
        );
    }
}
