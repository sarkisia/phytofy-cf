import React, { Component } from 'react';
import TimePicker from 'rc-time-picker';
import { Accordion, AccordionItem, AccordionItemTitle, AccordionItemBody } from 'react-accessible-accordion';
import moment from 'moment';

import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import ItemTimeContainer from 'phytofy-design-system/src/design-system/molecules/ItemTimeContainer';
import LightController from 'phytofy-design-system/src/design-system/organisms/LightController';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import 'rc-time-picker/assets/index.css';

class RecipeItemContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {...this.props.obj, errors: {start: '', end: ''}, isOpen: this.props.isOpen};

        this.onClickHandler = this.onClickHandler.bind(this);
    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props.obj, isOpen: this.props.isOpen})
        }
    }

    onClickHandler() {
        let errorStart = false;
        let errorEnd = false;
        if(!this.state.start || this.state.start > this.state.end) {
            errorStart = true
        }

        if(!this.state.end || this.state.start > this.state.end) {
            errorEnd = true;
        }

        this.setState({
            errors: {
                start: errorStart ? 'Invalid time' : '',
                end: errorEnd ? 'Invalid time' : '',
            }
        })

        if(errorStart || errorEnd) return;

        this.props.action(this.state, this.props.i);

        if(this.props.new) {
            let keys = ['start', 'end', 'uv', 'blue', 'green', 'hyperRed', 'farRed', 'white']
            let newPeriodInitValues = {};
            keys.forEach((key) => {
                newPeriodInitValues[key] = "";
            })

            this.setState({...newPeriodInitValues, errors: {start: '', end: ''}});
        }

    }

    render() {
        let title = "Add period";
        if(this.props.obj && this.props.obj.start && this.props.obj.end) {
            title = `${this.props.obj.start} - ${this.props.obj.end}`
        }

        return (
            <AccordionItem uuid={this.props.uuid} expanded={this.props.isOpen}>
                <AccordionItemTitle>
                    <span>{title}</span>
                    <div class="accordion__arrow">
                        <Icon icon="arrowDown" fill="#87888A" />
                    </div>
                </AccordionItemTitle>
                <AccordionItemBody>
                    <ItemTimeContainer title="Time">
                        <FormGroup type="time" label="Start" value={this.state.start} onChange={(event) => this.setState({start: event.target.value})} error={this.state.errors.start} />
                        <FormGroup type="time" label="End" value={this.state.end} onChange={(event) => this.setState({end: event.target.value}) }  error={this.state.errors.end} />
                    </ItemTimeContainer>
                    <LightController {...this.state} onAfterChange={(values) => this.setState({...values})} />
                    <Button state="success" name={this.props.new ? "Add" : "Edit"} onClick={this.onClickHandler} />
                    {!this.props.new && <Button state="danger" name={"Delete"} onClick={() => this.props.deletePeriod(this.props.i)} />}
                </AccordionItemBody>
            </AccordionItem>
        )
    }
}

export default RecipeItemContainer;