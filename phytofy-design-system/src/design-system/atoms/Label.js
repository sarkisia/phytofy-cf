import React, { Component } from 'react';
import styled from 'styled-components';

const Label = styled.label`
	display: block;
	font-size: .75rem;
	color: var(--sidebarPrimaryColor, #C5C6C8);
`

export default class extends Component {
	render() {
		return (
			<Label>{this.props.label}</Label>
		)
	}
}
