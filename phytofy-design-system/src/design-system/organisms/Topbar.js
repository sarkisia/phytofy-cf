import React, { Component } from 'react'
import styled from 'styled-components';

import Logo from '../atoms/Logos';
import TopbarTab from '../molecules/TopbarTab';

const Topbar = styled.div`
    left: 0;
    grid-row: 1 / 2;
    grid-column: 1 / 4;
    position: fixed;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid var(--topbarBorder, #ECEDED);
    background-color: var(--backgroundColor, #FFFFFF);
    width: 100%;
    height: var(--topBarHeight, 60px);
`

const LogoContainer = styled.div `
    height: 20px;
    padding: 0 var(--spacing, 30px);
    box-sizing: border-box;
`

const TabList = styled.ul `
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    width: 100%;
    height: 100%;
`

export default class extends Component {
    render() {
        const tabList = this.props.values.map((value) =>
            <TopbarTab to={value.location} name={value.name} isActive={this.props.isActive} />
        );

        return (
            <Topbar>
                <LogoContainer>
                    <Logo logo="osramLogo" />
                </LogoContainer>

                <TabList>
                    { tabList }
                </TabList>
            </Topbar>
        );
    }
}
