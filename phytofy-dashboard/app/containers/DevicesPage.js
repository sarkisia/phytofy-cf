// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import Table from 'phytofy-design-system/src/design-system/organisms/Table';
import TableData from 'phytofy-design-system/src/design-system/molecules/TableData';
import TableRow from 'phytofy-design-system/src/design-system/molecules/TableRow';
// import Card from 'phytofy-design-system/src/design-system/organisms/Card';
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell';
import TableRowOptions from 'phytofy-design-system/src/design-system/molecules/TableRowOptions';
import Checkbox from 'phytofy-design-system/src/design-system/atoms/Checkbox';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';


import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
import FiltersContainer from './FiltersContainer';

import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as DevicesActions from '../actions/devices'; 


const DevicesContainer = class extends Component {
  constructor(props){
    super(props);
    this.getCurrentMoxaMac = this.getCurrentMoxaMac.bind(this);
  }

  componentWillMount() {
    if(!this.getCurrentMoxaMac() && this.props.moxas.length > 0) {
      this.props.history.replace(`/setup/devices/${this.props.moxas[0].mac}`)
    }
  }

  getCurrentMoxaMac() {
    return this.props.history.location.pathname.split('/')[3];
  }

  render() {
    const tableAttributes = {
      serial_num: {name: "Serial Number", configs: []},
      firmware: {name: "Firmware", configs: ['measure', 'nosort']},
      hardware: {name: "Hardware", configs: ['measure', 'nosort']},
      port: {name: "Port", configs: ['measure'] },
      temperature: {name: "Temperature", configs: ['measure', 'nosort']},
      status: {name: "Status", configs: ['status', 'nosort'], transform: (row) => {
        if(row.status == 0){
          return <Icon fill="#009D3D" icon="checkedCircle" />
        } else if(row.status == 1){
          return <Icon fill="#009D3D" icon="uncheckedCircle" />
        } else {
          return <Icon fill="#FF6600" icon="warning" />
        }
      }}
    }

    let actionButtons = [
      { icon: 'refresh', onClick: (ids) => { this.props.devicesActions.commissionFixtures(this.getCurrentMoxaMac()) } },
    ];

    return (
      <div>
        <FiltersContainer filters={[]} />
        <Content>
        {this.props.moxas.length > 0 &&
        <ListContainer title="Fixtures" actions={{...this.props.devicesActions}} {...this.props} tableAttributes={tableAttributes}
            actionButtons={actionButtons} rowOptions={[]} objects={this.props.fixtures} selected={[]} />
        }
        </Content>
      </div>
    )  
  }
}

function mapStateToProps(state) {
  return {
    moxas: state.devices.moxas,
    fixtures: state.devices.fixtures,
    sortAttribute: state.devices.sortAttribute ? state.devices.sortAttribute : null,
    sortType: state.devices.sortType ? state.devices.sortType : 'orderAsc',
    count: state.devices.count,
    from: state.devices.from,
    to: state.devices.to,
    hasPrev: state.devices.hasPrev,
    hasNext: state.devices.hasNext,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    devicesActions: {...bindActionCreators(DevicesActions, dispatch)}
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DevicesContainer));