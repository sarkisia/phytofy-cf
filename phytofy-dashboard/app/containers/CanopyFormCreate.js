import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import * as CanopiesActions from '../actions/canopiesSetup';  
import CanopyForm from './CanopyForm';

const CanopyFormCreate = class extends Component {
    render() {
        const actions = [
            <Button type="cancel" name="Cancel" onClick={() => this.props.cancelForm() }/>,
            <Button type="success" name="Submit" onClick={() => {this.props.submitCreate(this._form.getData())}} />
        ]

        return (
            <CanopyForm actions={actions} validation={this.props.validation} obj={this.props.obj} fixtures={this.props.allFixtures} addFixture={this.props.addFixture} profiles={this.props.profiles} ref={(ref) => this._form = ref}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        obj: state.canopiesSetup.canopyObj,
        validation: state.canopiesSetup.validation,
        profiles: state.profiles.objectsAll,
        allFixtures: state.devices.allFixtures
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(CanopiesActions, dispatch);
  }
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CanopyFormCreate));