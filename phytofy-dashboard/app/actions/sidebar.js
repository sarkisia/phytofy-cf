export function openSidebar(component, label, title, type) {
  return (dispatch, getState) => {
    if(!type) type = 'normal';

    return dispatch({
      type: 'OPEN_SIDEBAR',
      component: component,
      label: label,
      title: title,
      sidebarType: type,
      prevState: getState().sidebar
    });
  }
}

export function closeSidebar() {
  return {
    type: 'CLOSE_SIDEBAR'
  };
}