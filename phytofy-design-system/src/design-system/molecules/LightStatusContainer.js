import React, { Component } from "react";
import styled from 'styled-components';

const TotalContainer = styled.div `
    border: 1px solid var(--topbarBorder, #ECEDED);
    border-radius: 4px;
    width: 100%;

    > span {
        position: absolute;
        margin: -11px 0 0 var(--spacing, 30px);
        background-color: var(--backgroundColor, #FFFFFF);
        padding: 0 10px;
        color: var(--topbarBorder, #ECEDED);
    }

    > div {
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 50px var(--spacing, 30px);
    }
`

export default class extends Component {
    render() {
        return (
            <TotalContainer>
                <span>{this.props.title}</span>

                <div>
                    {this.props.children}
                </div>
            </TotalContainer>
        );
    }
}
