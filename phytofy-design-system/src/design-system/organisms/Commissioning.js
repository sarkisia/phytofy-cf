import React, { Component } from 'react'
import styled, { css } from 'styled-components'

import LoadingBar from '../molecules/LoadingBar'
import Button from '../atoms/Button'

const Commissioning = styled.div `
 	background-color: rgba(255,255,255,.85);
	position: absolute;
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100vw;
	height: 100vh;
	z-index: 2;

 @supports (-webkit-backdrop-filter: none) or (backdrop-filter: none) {
    background-color: transparent;
    -webkit-backdrop-filter: blur(8px);
  }

	button {
		position: absolute;
		bottom: 150px;
		align-self: flex-end;
	}
`

export default class extends Component {
  render() {
    return (
			<Commissioning>
        <LoadingBar progress={this.props.progress} />
				<Button type="cancel" name="Cancel" onClick={this.props.cancel} />
      </Commissioning>
    );
  }
}
