import React, { Component } from 'react';
import styled from 'styled-components';

const Tag = styled.span`
	display: inline-block;
	background-color: var(--topbarBorder, #ECEDED);
	border-radius: calc(25px / 2);
	height: 25px;
	padding: 0 15px;
	line-height: 25px;
	vertical-align: middle;

	&:not(:last-child) {
		margin-right: 15px;
	}
`

export default class extends Component {
	render() {
		return (
			<Tag>{this.props.tag}</Tag>
		)
	}
}
