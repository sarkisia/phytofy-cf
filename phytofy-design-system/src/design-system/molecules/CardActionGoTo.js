import React, { Component } from 'react';
import styled from 'styled-components';

const GoTo = styled.div`
  margin-right: var(--spacing, 30px);
  border-right: 1px solid var(--topbarBorder, #ECEDED);
  height: var(--spacing, 30px);
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-right: var(--spacing, 30px);

  input {
    margin-left: 5px;
    border: 1px solid var(--secondaryColor, #87888A);
    border-radius: 4px;
    width: 38px;
    height: 24px;
    padding: 0 5px;
  }
`;

export default class extends Component {
  render() {
    return (
      <GoTo>
        Go to:
        <input type="text" value={this.props.goto} maxLength="3" onChange={this.props.onChange} onKeyPress={this.props.onKeyPress} />
      </GoTo>
    );
  }
}
