/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import CanopiesSetupPage from './containers/CanopiesSetupPage';
import DevicesPage from './containers/DevicesPage';
import ProfilesPage from './containers/ProfilesPage';
import RecipesPage from './containers/RecipesPage';
import SchedulesPage from './containers/SchedulesPage';
import DiagnosticPage from './containers/DiagnosticPage';
import SettingsPage from './containers/SettingsPage';

export default (props) => (
  <App history={props.history}>
    <Switch>
      <Route path="/setup/devices" component={DevicesPage} />
      <Route path="/setup/profiles" component={ProfilesPage} />
      <Route path="/setup/diagnostic" component={DiagnosticPage} />
      <Route path="/recipes" component={RecipesPage} />
      <Route path="/schedules" component={SchedulesPage} />
      <Route path="/setup/settings" component={SettingsPage} />
      <Route path="/setup/canopies" component={CanopiesSetupPage} />
      <Route path="/" component={HomePage} />
    </Switch>
  </App>
);
