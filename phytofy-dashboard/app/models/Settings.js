const electron = require('electron');
import Datastore from 'nedb-promise';

let Settings;

export const DEFAULT_PAGE_SIZE = 10;
export const DEFAULT_LENGTH_UNIT = 'mm';
export const DEFAULT_IRRADIANCE_UNIT = 'mol';
export const DEFAULT_COLORSCALE = 'rgb';

export const pageSizeOptions = {
    10: 10,
    20: 20,
    50: 50,
    100: 100
}

export const lengthUnitOptions = {
    'mm': 'mm',
    'in': 'in'
}

export const irradianceUnitOptions = {
    'mol': `μmol/m${String.fromCharCode(178)}/s`,
    '%': '%'
}

export const colorscaleOptions = {
    'rgb': "RGB",
    'grayscale': "Gray Scale",
    'colorblind': "Color Blind"
}

const createDatastore = () => {
    const userDataPath = (electron.app || electron.remote.app).getPath('userData');
    Settings = new Datastore({ filename: `${userDataPath}/settings.db`, autoload: true });
    try {
        Settings.ensureIndex({ fieldName: 'name', unique: true });
    } catch (err) {
        // console.error(err);
    }


    return Settings;
}

export const getPageSize = async () => {
    try {
        return (await Settings.cfind({name: 'pageSize'}).exec())[0].value;
    } catch (err) {
        // console.error(err)
    }

    return DEFAULT_PAGE_SIZE;
}

export const getLengthUnit = async () => {
    try {
        return (await Settings.cfind({name: 'lengthUnit'}).exec())[0].value;
    } catch (err) {
        // console.error(err)
    }

    return DEFAULT_LENGTH_UNIT;
}

export const getLengthToStore = (val, unit) => {
    if(unit == 'mm') {
        return val;
    } else {
        return val*25.4;
    }
}

export const getLengthConverted = (val, unit) => {
    if(unit == 'mm') {
        return val;
    } else {
        return (val/25.4).toFixed(2);
    }
}

export const getLengthToRead = (val, unit) => {
    if(unit == 'mm') {
        return `${val} ${unit}`;
    } else {
        return `${(val/25.4).toFixed(2)} ${unit}`;
    }
}

export const getIrradianceUnit = async () => {
    try {
        return (await Settings.cfind({name: 'irradianceUnit'}).exec())[0].value;
    } catch (err) {
        // console.error(err)
    }

    return DEFAULT_IRRADIANCE_UNIT;
}

export const getIrradianceColorscale = async () => {
    try {
        return (await Settings.cfind({name: 'irradianceColorscale'}).exec())[0].value;
    } catch (err) {
        // console.error(err)
    }

    return DEFAULT_COLORSCALE;
}

export default Settings ? Settings : createDatastore();