import React, { Component } from "react";
import styled from 'styled-components';

const ChannelContainer = styled.div `
    border: 1px solid var(--topbarBorder, #ECEDED);
    border-radius: 4px;
    width: 100%;

    > span {
        position: absolute;
        margin: -11px 0 0 var(--spacing, 30px);
        background-color: var(--backgroundColor, #FFFFFF);
        padding: 0 10px;
        color: var(--topbarBorder, #ECEDED);
    }
`
const SlidersContainer = styled.div `
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 50px var(--spacing, 30px);
`

const Sliders = styled.div `
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    flex-wrap: nowrap;
    width: 100%;
`

const Percentage = styled.div `
    margin-bottom: 10px;
    display: flex;
    justify-content: center;
    border-width: 1px 1px 0 1px;
    border-style: solid;
    border-color: var(--textColor, #424242);
    border-radius: 4px 4px 0 0;
    width: 456px;
    height: 10px;
    text-align: center;

    span {
        position: absolute;
        margin-top: -11px;
        background-color: var(--backgroundColor, #FFFFFF);
        padding: 0 10px;
        color: var(--textColor, #424242);
    }
`

const Unit = styled.div `
    margin-top: 10px;
    display: flex;
    justify-content: center;
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: var(--textColor, #424242);
    border-radius: 0 0 4px 4px;
    width: 456px;
    height: 10px;
    text-align: center;

    span {
        position: absolute;
        margin-top: -2px;
        background-color: var(--backgroundColor, #FFFFFF);
        padding: 0 10px;
        color: var(--textColor, #424242);
    }
`

export default class extends Component {
    render() {
        return (
            <ChannelContainer>
                <span>{this.props.title}</span>

                <SlidersContainer>
                    <Percentage>
                        <span>%</span>
                    </Percentage>

                    <Sliders>
                        {this.props.children}
                    </Sliders>

                    <Unit>
                        <span>{this.props.unit}</span>
                    </Unit>
                </SlidersContainer>
            </ChannelContainer>
        );
    }
}