// @flow
import validator from 'validator';
import Notifications from 'react-notification-system-redux';
import moment from 'moment';

import Schedule, {ScheduleFormValidator, addScheduleToFixtures, removeScheduleFromFixtures} from '../models/Schedule';
import { getPageSize } from '../models/Settings';
import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
         makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext, makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo, runCommandOnFixtures} from './helpers';
import { openSidebar, closeSidebar } from './sidebar';
import FormValidator from '../utils/FormValidator';
import Canopy, { getFixturesFromCanopy } from '../models/Canopy';
import Recipe from '../models/Recipe';
import Fixture from '../models/Fixture';


const defaultSchedule = {name: "", canopies: [], start_date: '', end_date: ''};

const querySchedules = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let schedules;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();
  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    schedules = await Schedule.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return schedules;
}

export const getObjects = makeGetObjectsAction('SCHEDULES_GET', 'schedules', Schedule, querySchedules);
export const getObjectsAll = makeGetObjectsAllAction('SCHEDULES_GET_ALL', 'schedules', Schedule, querySchedules);
export const sort = makeSortAction('SCHEDULES_SORT', 'schedules', getObjects);
export const addFilter = makeAddFilterAction('SCHEDULES_ADD_FILTER', 'schedules', getObjects);
export const removeFilter = makeRemoveFilterAction('SCHEDULES_REMOVE_FILTER', 'schedules', getObjects);
export const pageBack = makePageBack('SCHEDULES_PAGE_BACK', 'schedules', getObjects, Schedule);
export const pageNext = makePageNext('SCHEDULES_PAGE_NEXT', 'schedules', getObjects, Schedule);
export const select = makeSelectObject('SCHEDULES_SELECT', 'schedules');
export const deselect = makeDeselectObject('SCHEDULES_DESELECT', 'schedules');
export const selectAll = makeSelectAll('SCHEDULES_SELECT_ALL', 'schedules');
export const deselectAll = makeDeselectAll('SCHEDULES_DESELECT_ALL');
export const goto = makePageGoTo('SCHEDULES_PAGE_GOTO', 'schedules', getObjects, Schedule);

export const addCanopy = (obj) => {
    return async (dispatch, getState) => {
        await dispatch({
            type: 'SCHEDULES_ADD_CANOPY',
            canopiesSearch: [],
            scheduleObj: obj,
        })

        dispatch(openSidebar('ADD_CANOPY_TO_SCHEDULE_FORM'));
    }
}

export const getCanopies = (search) => {
    return async (dispatch, getState) => {
        let canopies = await Canopy.cfind({_id: {$nin: getState().schedules.scheduleObj.canopies}, name: new RegExp(search, 'i')}).sort({'name': 1}).exec();

        await dispatch({
            type: 'SCHEDULES_GET_CANOPIES',
            canopiesSearch: canopies
        })
    }
}

export const submitAddCanopy = (canopyId) => {
    return async (dispatch, getState) => {
        let scheduleObj = getState().schedules.scheduleObj;
        scheduleObj.canopies.push(canopyId);


        await dispatch({
            type: 'SCHEDULES_SUBMIT_EDIT_CANOPY',
            scheduleObj
        })

        let prevState = getState().sidebar.prevState;
        dispatch(openSidebar(prevState.component, prevState.label, prevState.title));
    }
}

export const create = (obj) => {
  return async (dispatch, getState) => {
    if(obj){
      obj.name = `Copy of ${obj.name}`;
    }

    let scheduleObj = obj ? obj : {...defaultSchedule};

    let recipes = await Recipe.cfind({}).sort({'name': 1}).exec();
    let canopies = await Canopy.cfind({}).sort({'name': 1}).exec();

    if(recipes.length == 0){
        dispatch(Notifications.error({
          title: `No recipes found!`,
          message: `Please create a recipe before trying to add a new schedule.`
        }));
        return;
    }

    if(!scheduleObj.recipeId){
        scheduleObj.recipeId = recipes[0]._id
    }

    await dispatch({
      type: 'SCHEDULES_CREATE',
      scheduleObj: scheduleObj,
      recipes,
      canopies,
      validation: {}
    })
    dispatch(openSidebar('CREATE_SCHEDULE_FORM', 'Create', 'New schedule'));
  }
};

export const submitCreate = (obj) => {
  return async (dispatch, getState) => {
    let validations = await ScheduleFormValidator.validate(obj);

    if(validations.isValid){
      try {
        obj._recipe = await Recipe.findOne({_id: obj.recipeId});
        obj = await Schedule.insert(obj);
      } catch(err){
        console.error(err);
      }

      let canopies = (await Canopy.cfind({_id: {$in: obj.canopies}}).exec());

      for(let canopyI=0; canopyI<canopies.length; canopyI++){
        let canopy = canopies[canopyI]; 
        let fixtures = await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).exec();
        await addScheduleToFixtures(obj, fixtures, getState().devices.comm, dispatch);
      }
      
      obj = {...defaultSchedule};
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Schedule created',
        message: `Schedule ${obj.name} created with success.`
      }));
    }

    

    dispatch(getObjects())

    dispatch({
      type: 'SCHEDULES_SUBMIT_CREATE',
      scheduleObj: obj,
      validation: validations,
    })
  }
}

export const duplicate = (obj) => {
  return async (dispatch, getState) => {
    let newObj = {...obj};
    delete newObj._id;

    dispatch(create(newObj));
  }
} 

export const edit = (obj) => {
  return async (dispatch, getState) => {

    let recipes = await Recipe.cfind({}).sort({'name': 1}).exec();
    let canopies = await Canopy.cfind({}).sort({'name': 1}).exec();

    await dispatch({
      type: 'SCHEDULES_EDIT',
      scheduleObj: obj,
      editObjId: obj._id,
      recipes,
      canopies,
      validation: {}
    })

    dispatch(openSidebar('EDIT_SCHEDULE_FORM', 'Edit', `${obj.name}`));
  }
}

export const submitEdit = (obj) => {
  return async (dispatch, getState) => {
    let original = await Schedule.findOne({_id: getState().schedules.editObjId});
    let validations = await ScheduleFormValidator.validate(obj, getState().schedules.scheduleObj);

    if(validations.isValid){
      try {
        obj._recipe = await Recipe.findOne({_id: obj.recipeId});
        await Schedule.update({_id: getState().schedules.editObjId}, obj);
      } catch(err){
        console.error(err);
      }

      for(let i=0; i<original.canopies.length; i++) {
        let canopyId = original.canopies[i];
        let canopy = await Canopy.findOne({_id: canopyId});

        if(!canopy) continue;

        let fixtures = await getFixturesFromCanopy(canopy);
        await removeScheduleFromFixtures(obj, fixtures, getState().devices.comm, dispatch); 
      }

      for(let i=0; i<obj.canopies.length; i++) {
        let canopyId = obj.canopies[i];
        let canopy = await Canopy.findOne({_id: canopyId});

        if(!canopy) continue;

        let fixtures = await getFixturesFromCanopy(canopy);
        await addScheduleToFixtures(obj, fixtures, getState().devices.comm, dispatch); 
      }

      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Schedule edited',
        message: `Schedule ${obj.name} edited with success.`
      }));
    }

    dispatch(getObjects());
    dispatch({
      type: 'SCHEDULES_SUBMIT_EDIT',
      scheduleObj: obj,
      editObjId: validations.isValid ? null : getState().schedules.editObjId,
      validation: validations
    })
  }
}

export const cancelForm = () => {
  return async (dispatch, getState) => {
    dispatch(closeSidebar())
    let obj = {...defaultSchedule};
    dispatch({
      type: 'SCHEDULES_CANCEL_FORM',
      scheduleObj: obj,
      editObjId: null,
      validation: {}
    })
  }
}

export const confirmDelete = (obj) => {
  return async (dispatch, getState) => {
    try {
      let fixtures = await Fixture.cfind({name: {$in: Object.keys(obj.fixtures)}}).exec();
      await removeScheduleFromFixtures(obj, fixtures, getState().devices.comm, dispatch);
      await Schedule.remove({_id: obj._id})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteObj = (obj) => {
  return async (dispatch, getState) => {
    let scheduleId = obj._id;
    // let count = await Canopy.count({scheduleId: scheduleId});
    // if(count > 0){
    //   dispatch(Notifications.error({
    //     title: `Cannot delete ${obj.name}!`,
    //     message: `The schedule ${obj.name} has one or more canopies associated to it.`,
    //   }));
    // } else {
      dispatch(Notifications.warning({
        title: `Delete ${obj.name}`,
        message: `Are you sure you want to delete ${obj.name}?`,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDelete(obj))
          }
        }
      }));
    // }
  };
}

export const confirmDeleteMultiple = () => {
  return async (dispatch, getState) => {
    try {
      let schedules = await Schedule.cfind({_id: {$in: getState().schedules.selected}}).exec();
      for(var i=0; i<schedules.length; i++) {
        let schedule = schedules[i];
        let fixtures = await Fixture.cfind({name: {$in: Object.keys(schedule.fixtures)}}).exec();
        await removeScheduleFromFixtures(schedule, fixtures, getState().devices.comm, dispatch);
      }
      let obj = await Schedule.remove({_id: {$in: getState().schedules.selected}}, {multi: true})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteMultiple = () => {
  return async (dispatch, getState) => {
    // let count = await Canopy.count({scheduleId: {$in: getState().schedules.selected}});
    // if(count > 0){
    //   dispatch(Notifications.error({
    //     title: `Cannot delete schedules!`,
    //     message: `The selected schedules have one or more canopies associated to them.`
    //   }));
    // } else {
      dispatch(Notifications.warning({
        title: `Delete ${getState().schedules.selected.length} schedules`,
        message: `Are you sure you want to delete ${getState().schedules.selected.length} schedules?`,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDeleteMultiple())
          }
        }
      }));
    // }
  }
}
