// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
import FiltersContainer from './FiltersContainer';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as RecipesActions from '../actions/recipes'; 


const RecipesContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [] };
  }

  componentDidMount() {
    this.props.recipesActions.getObjects();
  }

  render() {
    let { t } = this.props;

    const tableAttributes = {
      name: {name: t('name'), configs: []},
    }

    let actionButtons = [
      { icon: 'add', onClick: (ids) => { this.props.recipesActions.create() } },
    ];

    let actionButtonsWhenSelected = [{ icon: 'delete', onClick: () => { this.props.recipesActions.deleteMultiple() } }];

    // let configFilter = [
    //   {value: -1, name: 'All'},
    //   ...getCanopyConfigsForSelect()
    // ]

    const filters = [
      // {label: t('orientation'), options: configFilter, attribute: 'config'}
    ]

    const rowOptions = [
      {optionMethod: this.props.recipesActions.edit, iconName: 'edit'},
      {optionMethod: this.props.recipesActions.duplicate, iconName: 'duplicate'},
      {optionMethod: this.props.recipesActions.deleteObj, iconName: 'delete'}
    ];

    return (
      <div>
        <FiltersContainer search={(value) => {this.props.recipesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.recipesActions.addFilter} removeFilter={this.props.recipesActions.removeFilter} />
        <Content>
          {this.props.objects != null &&
            <ListContainer title={t('recipes')} actions={{...this.props.recipesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
              actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} rowOptions={rowOptions} />
          }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.recipes.sortAttribute ? state.recipes.sortAttribute : null,
    sortType: state.recipes.sortType ? state.recipes.sortType : 'orderAsc',
    objects: state.recipes.objects,
    count: state.recipes.count,
    from: state.recipes.from,
    to: state.recipes.to,
    selected: state.recipes.selected,
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    recipesActions: {...bindActionCreators(RecipesActions, dispatch)}

  }
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(RecipesContainer));