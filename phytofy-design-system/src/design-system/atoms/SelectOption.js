import React from 'react';

function SelectOption(props) {
  return (
    <option value={props.value}>{props.name}</option>
  );
}

export default SelectOption;
