import React, { Component } from 'react';
import styled from 'styled-components';

// import CardActionButton from '../molecules/CardActionButtons';
// import CardActionGoTo from '../molecules/CardActionGoTo';
// import CardActionNavigation from '../molecules/CardActionNavigation';

const CardTable = styled.div`
    background-color: var(--backgroundColor, #FFFFFF);
    border-width: 4px 1px 1px 1px;
    border: 1px solid var(--topbarBorder, #ECEDED);
    border-top: 4px solid var(--primaryColor, #FF6600);
    border-radius: 4px;
`

// const Header = styled.div`
//     display: flex;
//     flex-direction: row;
//     flex-wrap: nowrap;
//     justify-content: space-between;
//     margin-bottom: var(--spacing, 30px);
//     padding: var(--spacing, 30px) var(--spacing, 30px) 0 var(--spacing, 30px);

//     > div {
//         display: flex;
//         flex-direction: row;
//         align-items: center;

//         h1 {
//             color: var(--primaryColor, #FF6600);
//         }
//     }
// `

// const Actions = styled.div`
//     justify-content: flex-end;
//     color: var(--secondaryColor, #87888A);
// `

const Body = styled.div`
    padding: 0;
`

export default class extends Component {
    constructor(props){
        super(props);
        this.state = {goto: props.from};
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidUpdate(prevProps){
        if(this.props.from != prevProps.from){
            this.setState({goto: this.props.from})
        }
    }

    handleKeyPress(event) {
        if (event.key === 'Enter') {
            let page = parseInt(event.target.value);
            if(!isNaN(page)) this.props.pageGoTo(parseInt(event.target.value));
        }
    }

    handleChange(event) {
        this.setState({goto: event.target.value});
    }

    render() {
        return (
            <CardTable>
                {/* <Header>
                    <div>
                        <h1>{this.props.title}</h1>
                    </div>

                    <Actions>
                        <CardActionButton values={this.props.actions} />
                        <CardActionGoTo goto={this.state.goto} onKeyPress={this.handleKeyPress} onChange={this.handleChange} />
                        <CardActionNavigation from={this.props.from} to={this.props.to} of={this.props.of} pageBack={this.props.pageBack} pageNext={this.props.pageNext}/>
                    </Actions>
                </Header> */}
                <Body >
                    { this.props.children }
                </Body>
            </CardTable>
        )
    }
}
