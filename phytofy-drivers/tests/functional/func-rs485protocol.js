const compute = require('../../src/compute.js');
const proto = require('../../lib/rs485-protocol.js');
const logger = require('../../src/logger.js');

/*usr localhost
run with ncat TCP discard server:
ncat -l --keep-open 50001 --receive-only (print on terminal received chars)
and on another terminal
ncat -l --keep-open 50002 --receive-only
or

run with ncat TCP echo server:
ncat -l  50001 --exec "/bin/cat" --keep-open
and on another terminal
ncat -l 50002 --exec "/bin/cat" --keep-open

*/

const comm = new proto.RS485protocol('127.0.0.1');

comm.fixtureMap = [
  {serial_num: 1937465, slave_addr: 1, moxa_addr: '127.0.0.1', port: 50001},
  {serial_num: 9972445, slave_addr: 2, moxa_addr: '127.0.0.1', port: 50001},
  {serial_num: 6052331, slave_addr: 1, moxa_addr: '127.0.0.1', port: 50002},
  {serial_num: 1200339, slave_addr: 1, moxa_addr: '127.0.0.1', port: 50002},
];

async function test(){
  await comm.openConnection(50001, '127.0.0.1');

  //let resp = await comm.getFixtureInfo(1937465);
  //console.log(resp);
  //let resp2 = await comm.getSchedule(9972445);
  //console.log(resp2);
  let resp3 = await comm.setTimeReference(1937465);
  console.log(resp3);
  resp4 = await comm.getScheduleCount(9972445);
  console.log(resp4);

  await comm.closeConnection();

  await comm.openConnection(50002, '127.0.0.1');

  //resp = await comm.getFixtureInfo(6052331);
  //console.log(resp);
  //resp2 = await comm.getSchedule(6052331);
  //console.log(resp2);
  resp3 = await comm.getLEDstate(6052331);
  console.log(resp3);
  resp4 = await comm.getTimeReference(1200339);
  console.log(resp4);

  await comm.closeConnection();
};

async function testCommission(){

  let addedFixtures = await comm.commissionFixtures('127.0.0.1', [50001, 50002]);

  console.log(addedFixtures);

};

test();

//testCommission();
