import React, { Component } from 'react';
import styled from 'styled-components';

import ActionButton from '../atoms/ActionButton';

const ActionButtons = styled.div`
    margin-left: var(--spacing, 30px);
    border-left: 1px solid var(--topbarBorder, #ECEDED);
    display: flex;
    flex-direction: row;
    align-items: center;
    padding-left: var(--spacing, 30px);
`;

export default class CardActionButtons extends Component {
    render() {
        let values = this.props.values ? this.props.values : [];

        const actionButtons = values.map((value) =>
            <ActionButton icon={value.icon} onClick={value.onClick} />
        );

        return (
            <ActionButtons>
                { actionButtons }
            </ActionButtons>
        );
    }
}
