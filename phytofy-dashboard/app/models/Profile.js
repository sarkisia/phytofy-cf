const electron = require('electron');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

import FormValidator from '../utils/FormValidator';
import i18n from '../../i18n';

import { getLengthConverted, getLengthToStore } from './Settings';

let Profile;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Profile = new Datastore({ filename: `${userDataPath}/profiles.db`, autoload: true });
  try {
    Profile.ensureIndex({ fieldName: 'name', unique: true });
  } catch (err) {
    console.error(err);
  }

  return Profile;
}

export default Profile ? Profile : createDatastore();

export const canopyConfigs = {
  '1v': `1 ${i18n.t('vertical')}`,
  '1h': `1 ${i18n.t('horizontal')}`,
  '2v': `2 ${i18n.t('vertical')}`,
  '2h': `2 ${i18n.t('horizontal')}`,
  '3v': `3 ${i18n.t('vertical')}`,
  '2x2h': `2x2 ${i18n.t('horizontal')}`,
  '4v': `4 ${i18n.t('vertical')}`
}

export const canopyConfigsNFixtures = {
  '1v': 1,
  '1h': 1,
  '2v': 2,
  '2h': 2,
  '3v': 3,
  '2x2h': 4,
  '4v': 4,
}

export const configsWithDeltaL = ['2v', '2h', '3v', '2x2h', '4v'];
export const configsWithDeltaW = ['2x2h'];

export const getCanopyConfigsForSelect = () => {
  return Object.keys(canopyConfigs).map((value) => {
    return {value: value, name: canopyConfigs[value]}
  })
}

export const getImageForConfig = (config) => {
  if(config){
    return require(`phytofy-design-system/src/images/${config}.svg`);
  } else {
    return null;
  }
  
}

export const convertLengthsToRead = (obj, unit) => {
  if(unit == 'mm'){
    return obj;
  }

  let newObj = {...obj};

  newObj.length = getLengthConverted(newObj.length, unit);
  newObj.width = getLengthConverted(newObj.width, unit);
  newObj.height = getLengthConverted(newObj.height, unit);
  newObj.deltaLength = getLengthConverted(newObj.deltaLength, unit);
  newObj.deltaWidth = getLengthConverted(newObj.deltaWidth, unit);

  return newObj;
}

export const convertLengthsToStore = (obj, unit) => {
  if(unit == 'mm'){
    return obj;
  }

  let newObj = {...obj};

  newObj.length = getLengthToStore(newObj.length, unit);
  newObj.width = getLengthToStore(newObj.width, unit);
  newObj.height = getLengthToStore(newObj.height, unit);
  newObj.deltaLength = getLengthToStore(newObj.deltaLength, unit);
  newObj.deltaWidth = getLengthToStore(newObj.deltaWidth, unit);

  return newObj;
}

const rules = [
  {
    field: 'name',
    method: validator.isEmpty,
    validWhen: false,
    message: 'Name is required'
  },
  {
    field: 'name',
    method: async (value) => {
      let count = await Profile.count({name: value});
      return count == 0;
    },
    validWhen: true,
    ignoreEqualsPrev: true,
    message: 'A Layout with this name already exists'
  },
  {
    field: 'name',
    method: validator.isLength,
    args: [{min: 0, max: 30}],
    validWhen: true,
    message: 'Name exceeds the maximum length of 30'
  },
  {
    field: 'name',
    method: async (value) => {
      return /^[A-Za-z\d\s]+$/.test(value)
    },
    validWhen: true,
    message: 'Name contains invalid characters'
  },
  {
    field: 'length',
    method: validator.isNumeric,
    args: [{min: 0}],  // an array of additional arguments
    validWhen: true,
    message: 'Length must be a positive integer'
  },
  {
    field: 'length',
    method: validator.isLength,
    args: [{min: 0, max: 6}],
    validWhen: true,
    message: 'Length exceeds the maximum length of 6 numbers'
  },
  {
    field: 'width',
    method: validator.isNumeric,
    args: [{min: 0}],  // an array of additional arguments
    validWhen: true,
    message: 'Width must be a positive integer'
  },
  {
    field: 'width',
    method: validator.isLength,
    args: [{min: 0, max: 6}],
    validWhen: true,
    message: 'Width exceeds the maximum length of 6 numbers'
  },
  {
    field: 'height',
    method: validator.isNumeric,
    args: [{min: 0}],  // an array of additional arguments
    validWhen: true,
    message: 'Height must be a positive integer'
  },
  {
    field: 'height',
    method: validator.isLength,
    args: [{min: 0, max: 6}],
    validWhen: true,
    message: 'Height exceeds the maximum length of 6 numbers'
  },
];

export const getValidationRules = (obj) => {
  let valRules = [...rules];
  if(configsWithDeltaL.find((el) => { return el == obj.config; })){
    valRules.push({
      field: 'deltaLength',
      method: validator.isNumeric,
      args: [{min: 0}],  // an array of additional arguments
      validWhen: true,
      message: '△ Length must be a positive integer'
    });
    valRules.push({
      field: 'deltaLength',
      method: validator.isLength,
      args: [{min: 0, max: 6}],
      validWhen: true,
      message: '△ Length exceeds the maximum length of 6 numbers'
    });
  }

  if(configsWithDeltaW.find((el) => { return el == obj.config; })){
    valRules.push({
      field: 'deltaWidth',
      method: validator.isNumeric,
      args: [{min: 0}],  // an array of additional arguments
      validWhen: true,
      message: '△ Width must be a positive integer'
    });
    valRules.push({
      field: 'deltaWidth',
      method: validator.isLength,
      args: [{min: 0, max: 6}],
      validWhen: true,
      message: '△ Width exceeds the maximum length of 6 numbers'
    });
  }

  return valRules;
}

export const getProfileAttrFromId = (_id, attr, profiles) => {
  let filtered = profiles.filter((profile) => {
    return profile._id == _id;
  })

  if(filtered.length > 0){
    return filtered[0][attr];
  }

  return "";
}

export const exportData = async () => {
  const Json2csvParser = require('json2csv').Parser;

  let objects = await Profile.cfind({}).exec();
  let fields = ['_id', 'name', 'config', 'length', 'width', 'height', 'deltaWidth', 'deltaLength'];

  const parser = new Json2csvParser({fields});
  const csv = parser.parse(objects);

  return csv;
}

export const importData = async (data) => {
  let ids = data.map((obj) => {
    return obj._id;
  })

  let existingObjects = await Profile.cfind({_id: {$in: ids}}).exec();

  let existingIds = existingObjects.map((obj) => {
    return obj._id;
  })

  let objectsToCreate = [];
  
  data.forEach(async (obj) => {
    if(existingIds.indexOf(obj._id) > -1) {
      let id = obj._id;
      delete obj._id;
      await Profile.update({_id: id}, obj);
      return;
    }

    objectsToCreate.push(obj);
  });

  await Profile.insert(objectsToCreate);

  return;
}