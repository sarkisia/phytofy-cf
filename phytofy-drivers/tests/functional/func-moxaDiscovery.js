const discovery = require('../../lib/moxa-discovery.js');
const utils = require('../../src/moxa-discovery_utils.js');
const defines = require('../../src/defines.js');
const logger = require('../../src/logger.js');

/*
  Run with:
  ncat -l 4800 --hex-dump  moxa-disc.log --udp

  or use with real moxa devices

*/

async function test(){
  const moxaList = await discovery.discoverMoxas();

  console.log(moxaList);
};

test();
