const electron = require('electron');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';
import moment from 'moment';

import FormValidator from '../utils/FormValidator';
import { runCommandOnFixtures} from '../actions/helpers';

let Schedule;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Schedule = new Datastore({ filename: `${userDataPath}/schedules.db`, autoload: true });

  return Schedule;
}

const rules = [
  {
    field: 'start_date',
    method: validator.isEmpty,
    validWhen: false,
    message: 'Start date is required'
  },
  {
    field: 'end_date',
    method: validator.isEmpty,
    validWhen: false,
    message: 'End date is required'
  },
  {
    field: 'end_date',
    method: async (value, state) => {
      if(state.end_date) return value > state.start_date
      return true;
    },
    validWhen: true,
    message: 'Invalid date'
  },
]  

export default Schedule ? Schedule : createDatastore();

export const ScheduleFormValidator = new FormValidator(rules);

export const addScheduleToFixtures = async (schedule, fixtures, comm, dispatch) => {
  let curr = moment(schedule.start_date);
  let end = moment(schedule.end_date).add(1, 'd');

  let scheduleFixtures = schedule.fixtures ? {...schedule.fixtures} : {};

  while(curr < end) {
    for(var i=0; i<schedule._recipe.periods.length; i++){
        let period = schedule._recipe.periods[i];
        let periodStartHours = parseInt(period.start.split(":")[0]);
        let periodStartMinutes = parseInt(period.start.split(":")[1]);

        let recipeStartDate = curr.hours(periodStartHours).minutes(periodStartMinutes).unix();

        let periodEndHours = parseInt(period.end.split(":")[0]);
        let periodEndMinutes = parseInt(period.end.split(":")[1]);
        
        let recipeEndDate = curr.hours(periodEndHours).minutes(periodEndMinutes).unix();  
        await runCommandOnFixtures(fixtures, comm, dispatch, async (fixture) => {
          let scheduleId = Math.floor(Math.random()*4294967295);

          // let scheduleId = `${currentTimestampStr.substring(currentTimestampStrLength-4,currentTimestampStrLength)}${fixture.serial_num}${i}`;
          if(!Object.keys(scheduleFixtures).includes(fixture.name)) {
            scheduleFixtures[fixture.serial_num] = [scheduleId];
          } else {
            scheduleFixtures[fixture.serial_num].push(scheduleId);
          }
          await comm.setTimeReference(fixture.serial_num, moment().unix());
          await comm.setSchedule(fixture.serial_num, scheduleId, recipeStartDate, recipeEndDate,
              [
                  period.uv,
                  period.blue,
                  period.green,
                  period.hyperRed,
                  period.farRed,
                  period.white
              ])
        })
    }

    curr = curr.add(1, 'd');
  }

  await Schedule.update({_id: schedule._id}, {$set: {fixtures: scheduleFixtures}});
}

export const removeScheduleFromFixtures = async (schedule, fixtures, comm, dispatch) => {
  let scheduleFixtures = schedule.fixtures ? {...schedule.fixtures} : {}; 
  await runCommandOnFixtures(fixtures, comm, dispatch, async (fixture) => {
    if(!schedule.fixtures[fixture.serial_num]) return;

    for(let i=0; i<schedule.fixtures[fixture.serial_num].length; i++) {
      let scheduleId = schedule.fixtures[fixture.serial_num][i];
      await comm.deleteSchedule(fixture.serial_num, scheduleId);
    }

    let {[fixture.serial_num]: omit, ...newScheduleFixtures} = scheduleFixtures;
    scheduleFixtures = newScheduleFixtures;
  });

  await Schedule.update({_id: schedule._id}, {$set: {fixtures: scheduleFixtures}});
}