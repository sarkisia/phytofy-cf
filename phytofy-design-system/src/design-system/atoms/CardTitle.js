import React, { Component } from 'react';
import styled from 'styled-components';

const CardTitle = styled.div`
	h1 {
		color: var(--primaryColor, #FF6600);
	}
`

export default class extends Component {
	render() {
		return (
			<CardTitle>
				<h1>{this.props.title}</h1>
			</CardTitle>
		)
	}
}
