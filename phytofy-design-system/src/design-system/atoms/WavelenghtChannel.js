import React, { Component } from 'react';
import styled, { css } from 'styled-components';

const WavelenghtChannel = styled.li`
	display: flex;
	justify-content: center;
	align-items: center;
	list-style: none;
	border-width: 2px;
	border-style: solid;
	border-radius: 50%;
	width: 30px;
	height: 30px;
	font-size: .75rem;
	font-weight: var(--bold, 700);
	color: var(--white, #FFFFFF);

	${props => props.name === "UV" && css`
    background-color: var(--channelUV, #6A1B9A);
		border-color: var(--channelUV, #6A1B9A);
  `}

	${props => props.name === "B" && css`
    background-color: var(--channelBlue, #61AFF1);
		border-color: var(--channelBlue, #61AFF1);
  `}

	${props => props.name === "G" && css`
    background-color: var(--channelGreen, #09E425);
		border-color: var(--channelGreen, #09E425);
  `}

	${props => props.name === "HR" && css`
    background-color: var(--channelHRed, #C62828);
		border-color: var(--channelHRed, #C62828);
  `}

	${props => props.name === "FR" && css`
    background-color: var(--channelFRed, #AC2A18);
		border-color: var(--channelFRed, #AC2A18);
  `}

	${props => props.name === "W" && css`
    background-color: var(--channelWhite, #A7A8AA);
		border-color: var(--channelWhite, #A7A8AA);
  `}

	${props => props.state === false && css`
    background: transparent;
		border-color: var(--topbarBorder, #ECEDED);
		color: var(--topbarBorder, #ECEDED);
  `}
`

function Channel(props) {
  return (
		<WavelenghtChannel name={props.name} state={props.state}>
			{props.name}
		</WavelenghtChannel>
  );
}

export default Channel;
