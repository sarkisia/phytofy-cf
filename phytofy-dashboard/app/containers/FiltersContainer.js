// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import TopbarFilter from 'phytofy-design-system/src/design-system/molecules/TopbarFilter';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import SearchFilter from 'phytofy-design-system/src/design-system/atoms/SearchFilter'


const FiltersContainer = class extends Component {
    render() {
        const setFilter = (filter) => {
            return (event) => {
              if(event.target.value == -1){
                this.props.removeFilter(filter, this.props.queryAction)
              } else {
                this.props.addFilter(filter, event.target.value, this.props.queryAction)
              }
            }
        }

        return (
            <Topbar>
                {this.props.extraFilters && this.props.extraFilters.map(({label, options, onChange}) => {
                    return <TopbarFilter>
                        <FormGroup type="select" label={ label } options={ options } onChange={ onChange }/>
                    </TopbarFilter>
                })}
                {this.props.filters.map(({label, options, attribute}) => {
                    return <TopbarFilter>
                        <FormGroup type="select" label={ label } options={ options } onChange={ setFilter(attribute) }/>
                    </TopbarFilter>
                })}
                {this.props.search && 
                    <TopbarFilter search>
                        <SearchFilter placeholder="Search" onChange={this.props.search} />
                    </TopbarFilter>
                }

            </Topbar>
        )
    }
}

export default FiltersContainer;