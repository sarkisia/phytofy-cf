// @flow
import validator from 'validator';
import Notifications from 'react-notification-system-redux';

import Profile, {getValidationRules, convertLengthsToRead, convertLengthsToStore} from '../models/Profile';
import { getPageSize } from '../models/Settings';
import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
         makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext, makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo } from './helpers';
import { openSidebar, closeSidebar } from './sidebar';
import Canopy from '../models/Canopy';
import FormValidator from '../utils/FormValidator';

const defaultProfile = {name: "", length: "", width: "", height: "", deltaWidth:"", deltaLength: "", config:"1v", nCanopies: 0};

const queryProfiles = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let profiles;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();
  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    profiles = await Profile.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return profiles;
}

export const getObjects = makeGetObjectsAction('PROFILES_GET', 'profiles', Profile, queryProfiles);
export const getObjectsAll = makeGetObjectsAllAction('PROFILES_GET_ALL', 'profiles', Profile, queryProfiles);
export const sort = makeSortAction('PROFILES_SORT', 'profiles', getObjects);
export const addFilter = makeAddFilterAction('PROFILES_ADD_FILTER', 'profiles', getObjects);
export const removeFilter = makeRemoveFilterAction('PROFILES_REMOVE_FILTER', 'profiles', getObjects);
export const pageBack = makePageBack('PROFILES_PAGE_BACK', 'profiles', getObjects, Profile);
export const pageNext = makePageNext('PROFILES_PAGE_NEXT', 'profiles', getObjects, Profile);
export const select = makeSelectObject('PROFILES_SELECT', 'profiles');
export const deselect = makeDeselectObject('PROFILES_DESELECT', 'profiles');
export const selectAll = makeSelectAll('PROFILES_SELECT_ALL', 'profiles');
export const deselectAll = makeDeselectAll('PROFILES_DESELECT_ALL');
export const goto = makePageGoTo('PROFILES_PAGE_GOTO', 'profiles', getObjects, Profile);


export const create = (obj) => {
  return async (dispatch, getState) => {
    if(obj){
      obj.name = `Copy of ${obj.name}`;
    }

    let profileObj = obj ? obj : {...defaultProfile};
    await dispatch({
      type: 'PROFILES_CREATE',
      profileObj: profileObj,
      validation: {}
    })
    dispatch(openSidebar('CREATE_CANOPY_PROFILE_FORM', 'Create', 'New layout'));
  }
};

export const submitCreate = (obj) => {
  return async (dispatch, getState) => {
    const ProfileFormValidator = new FormValidator(getValidationRules(obj));
    let validations = await ProfileFormValidator.validate(obj);

    if(validations.isValid){
      try {
        await Profile.insert(convertLengthsToStore(obj, getState().settings.lengthUnit));
      } catch(err){
        console.error(err);
      }
      obj = {...defaultProfile};
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Layout created',
        message: `Layout ${obj.name} created with success.`
      }));
    }

    dispatch(getObjects())

    dispatch({
      type: 'PROFILES_SUBMIT_CREATE',
      profileObj: obj,
      validation: validations
    })
  }
}

export const duplicate = (obj) => {
  return async (dispatch, getState) => {
    let newObj = {...obj};
    delete newObj._id;
    newObj.nCanopies = 0;

    dispatch(create(newObj));
  }
} 

export const edit = (obj) => {
  return async (dispatch, getState) => {
    
    await dispatch({
      type: 'PROFILES_EDIT',
      profileObj: convertLengthsToRead(obj, getState().settings.lengthUnit),
      editObjId: obj._id,
      validation: {}
    })

    dispatch(openSidebar('EDIT_CANOPY_PROFILE_FORM', 'Edit', `${obj.name}`));
  }
}

export const submitEdit = (obj) => {
  return async (dispatch, getState) => {

    const ProfileFormValidator = new FormValidator(getValidationRules(obj));
    let validations = await ProfileFormValidator.validate(obj, getState().profiles.profileObj);

    if(validations.isValid){
      try {
        await Profile.update({_id: getState().profiles.editObjId}, convertLengthsToStore(obj, getState().settings.lengthUnit));
        await Canopy.update({'_profile._id': getState().profiles.editObjId}, {'$set': {_profile: convertLengthsToStore(obj, getState().settings.lengthUnit)}}, { multi: true });
      } catch(err){
        console.error(err);
      }
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Layout edited',
        message: `Layout ${obj.name} edited with success.`
      }));
    }

    dispatch(getObjects())
    dispatch({
      type: 'PROFILES_SUBMIT_EDIT',
      profileObj: obj,
      editObjId: validations.isValid ? null : getState().profiles.editObjId,
      validation: validations
    })
  }
}

export const cancelForm = () => {
  return async (dispatch, getState) => {
    dispatch(closeSidebar())
    let obj = {...defaultProfile};
    dispatch({
      type: 'PROFILES_CANCEL_FORM',
      profileObj: obj,
      editObjId: null,
      validation: {}
    })
  }
}

export const confirmDelete = (obj) => {
  return async (dispatch, getState) => {
    try {
      await Profile.remove({_id: obj._id})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteObj = (obj) => {
  return async (dispatch, getState) => {
    let profileId = obj._id;
    let count = await Canopy.count({profileId: profileId});
    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot delete ${obj.name}!`,
        message: `The layout ${obj.name} has one or more canopies associated to it.`,
      }));
    } else {
      dispatch(Notifications.warning({
        title: `Delete ${obj.name}`,
        message: `Are you sure you want to delete ${obj.name}?`,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDelete(obj))
          }
        }
      }));
    }
  };
}

export const confirmDeleteMultiple = () => {
  return async (dispatch, getState) => {
    try {
      obj = await Profile.remove({_id: {$in: getState().profiles.selected}}, {multi: true})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteMultiple = () => {
  return async (dispatch, getState) => {
    let count = await Canopy.count({profileId: {$in: getState().profiles.selected}});
    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot delete layouts!`,
        message: `The selected layouts have one or more canopies associated to them.`
      }));
    } else {
      dispatch(Notifications.warning({
        title: `Delete ${getState().profiles.selected.length} profiles`,
        message: `Are you sure you want to delete ${getState().profiles.selected.length} layouts?`,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDeleteMultiple())
          }
        }
      }));
    }
  }
}
