import React, { Component } from 'react';
import styled, { css } from 'styled-components';

import RightSidebarTopbar from '../organisms/RightSidebarTopbar';
import RightSidebarContent from '../organisms/RightSidebarContent';

const RightSidebar = styled.div`
  --rightSidebarWidth: 300px;

  position: fixed;
  overflow: scroll;
  grid-row: 1 / 2;
  grid-column: 4 / 5;
  right: 0;
  width: var(--rightSidebarWidth, 300px);
  height: 100%;
  background-color: var(--backgroundColor, #FFFFFF);
  box-shadow: -2px 0 6px rgba(0, 0, 0, .05);
  z-index: 5;
  transform: translateX(var(--rightSidebarWidth, 300px));
  transition-property: transform;
  transition-duration: 0.4s;
  color: var(--sidebarPrimaryColor, #C5C6C8);
  position: fixed;
  overflow: scroll;

  ${props => props.isOpen && css`
    transform: translateX(0);
  `}

  ${props => props.type === "xl" && css`
    --rightSidebarWidth: 800px;
    width: 800px;
  `}

  img {
    margin-bottom: 30px;
  }
`

export default class extends Component {
  render() {
    return (
      <RightSidebar type={this.props.type} isOpen={this.props.isOpen}>
        <RightSidebarTopbar goBack={this.props.goBack} label={this.props.label} title={this.props.title} closeRightSidebar={this.props.closeRightSidebar} />
        <RightSidebarContent>
          {this.props.children}
        </RightSidebarContent>
      </RightSidebar>
    );
  }
}
