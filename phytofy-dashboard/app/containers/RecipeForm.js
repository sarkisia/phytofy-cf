import React,{Component} from 'react';
import Timeline from 'react-visjs-timeline'
import { Accordion, AccordionItem, AccordionItemTitle, AccordionItemBody } from 'react-accessible-accordion'

import CanopyConfig from 'phytofy-design-system/src/design-system/atoms/CanopyConfig';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';

import i18n from '../../i18n';

import FormContainer from './FormContainer';
import RecipeItemContainer from './RecipeItemContainer';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';


const options = {
    width: '100%',
    height: '79px',
    showMajorLabels: false,
    showMinorLabels: true,
    showCurrentTime: false,
    min: 1532473200000,
    max: 1532559599000,
    start: 1532473200000,
    end: 1532559599000,
    zoomMin: 300000,
    stack: false,
    format: {
        minorLabels: {
            minute: 'h:mma',
            hour: 'ha'
        }
    }
}

/* Timeline Items */

const RecipeForm = class extends Component {
    constructor(props){
        super();
        this.props = props;

        let tmpPeriods = this.props.obj.periods ? this.props.obj.periods : [];
        let {periods, timelineItems} = this.sortPeriods(tmpPeriods);

        this.state = {...this.props.obj, timelineItems:timelineItems, activeItems: [], periods}

        this.sortPeriods = this.sortPeriods.bind(this);
        this.addPeriod = this.addPeriod.bind(this);
        this.editPeriod = this.editPeriod.bind(this);
        this.deletePeriod = this.deletePeriod.bind(this);
        this.timelineClickHandler = this.timelineClickHandler.bind(this);

        this.sortPeriods(this.state.periods);
    }

    getData() {
        return {...this._form.getData(), periods: this.state.periods};
    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props.obj});
            // this.sortPeriods(this.props.obj.periods);
        }
    }

    sortPeriods(periods) {
        let newPeriods = [...periods].sort(function(a,b) {return (a.start > b.start) ? 1 : ((b.start > a.start) ? -1 : 0);} ); 
        let timelineItems = newPeriods.map((obj, i) => {
            return {
                id: i+1,
                start: `2018-07-25 ${obj.start}:00`,
                end: `2018-07-25 ${obj.end}:00`
            }
        })

        return {periods: newPeriods, timelineItems, activeItems: []};
    }

    addPeriod(obj) {
        let newPeriods = [...this.state.periods, obj];
        this.setState(this.sortPeriods(newPeriods));
    }

    editPeriod(obj, i) {
        let newPeriods = [...this.state.periods];
        newPeriods.splice(i, 1);
        newPeriods.push(obj);
        this.setState(this.sortPeriods(newPeriods));
    }

    deletePeriod(i) {
        let newPeriods = [...this.state.periods];
        newPeriods.splice(i, 1);
        this.setState(this.sortPeriods(newPeriods));
    }

    timelineClickHandler(obj) {
        if(obj.item) {
            if(!this.state.activeItems.includes(obj.item-1)){
                this.setState({activeItems: [...this.state.activeItems, obj.item-1]})
            }
        }
    }

    render() {
        return (
            <div>
                <FormContainer obj={this.props.obj} validation={this.props.validation} actions={this.props.actions} ref={(ref) => this._form = ref}>
                    <FormGroup type="text" label={i18n.t("name")} name="name" placeholder="" value={this.state.name} onChange={this.handleInputChange} error={this.props.validation.name && this.props.validation.name.message}/>
                    <Timeline options={options} items={this.state.timelineItems} clickHandler={this.timelineClickHandler} />
                    <Accordion onChange={(value) => {this.setState({activeItems: value})}} accordion={false}>
                        {this.state.periods.map((period, i) => {
                            return (<RecipeItemContainer uuid={i} isOpen={this.state.activeItems.includes(i)} key={i} i={i} deletePeriod={this.deletePeriod} action={this.editPeriod} obj={period} />)
                        })}
                        <RecipeItemContainer new={true} uuid={'newitem'} action={this.addPeriod} />
                    </Accordion>
                </FormContainer>
            </div>
        )
    }
}

export default RecipeForm;