// @flow
import * as React from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Notifications from 'react-notification-system-redux';

import PrimaryMenu from 'phytofy-design-system/src/design-system/organisms/PrimaryMenu';
import SecondaryMenu from 'phytofy-design-system/src/design-system/organisms/SecondaryMenu';
import ThirdMenu from 'phytofy-design-system/src/design-system/organisms/ThirdMenu';
import RightSidebar from 'phytofy-design-system/src/design-system/organisms/RightSidebar';
import Wrapper from 'phytofy-design-system/src/design-system/organisms/Wrapper';
import MainContent from 'phytofy-design-system/src/design-system/organisms/MainContent';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import i18n from '../../i18n';
import { getNotificationsStyle } from '../utils/notifications';

import 'phytofy-design-system/src/css/App.css';
import 'phytofy-design-system/src/css/Accordion.css';
// import 'vis/dist/vis-timeline-graph2d.min.css';
import 'vis/dist/vis-timeline-graph2d.min.css'
import 'phytofy-design-system/src/css/Vis.css';

import CanopyProfileFormCreate from './CanopyProfileFormCreate';
import CanopyProfileFormEdit from './CanopyProfileFormEdit';
import CanopyFormCreate from './CanopyFormCreate';
import CanopyFormEdit from './CanopyFormEdit';
import RecipeFormCreate from './RecipeFormCreate';
import RecipeFormEdit from './RecipeFormEdit';
import ScheduleFormCreate from './ScheduleFormCreate';
import ScheduleFormEdit from './ScheduleFormEdit';
import RealtimeContainer from './RealtimeContainer';
import AddCanopyToSchedule from './AddCanopyToSchedule';
import AddFixtureToCanopy from './AddFixtureToCanopy';

import * as SidebarActions from '../actions/sidebar'; 
import * as SettingsActions from '../actions/settings'; 
import * as DevicesActions from '../actions/devices'; 


type Props = {
  children: React.Node
};

const primaryMenu = [
  { icon: 'fixture', name: `${i18n.t('overview')}`, key: 1, location: '/'},
  { icon: 'recipes', name: `${i18n.t('recipes')}`, key: 4, location: '/recipes' },
  { icon: 'schedule', name: `${i18n.t('schedules')}`, key: 5, location: '/schedules' },
  { icon: 'setup', name: `${i18n.t('setup')}`, key: 7, location: '/setup/canopies' },
];

const secondaryMenu = [
  {name: `${i18n.t('canopies')}`, key: 1, location: '/setup/canopies'},
  {name: `${i18n.t('devices')}`, key:2, location: '/setup/devices'},
  {name: `${i18n.t('layouts')}`, key:3, location: '/setup/profiles'},
  {name: `${i18n.t('diagnostic')}`, key: 6, location: '/setup/diagnostic' },
  {name: `${i18n.t('settings')}`, key: 7, location: '/setup/settings' },
];

const App = class extends React.Component<Props> {
  props: Props;

  constructor(props) {
    super(props);
    this.state = {isSecondaryMenuOpen: false};
    this.openRightSidebar = this.openRightSidebar.bind(this);
    this.closeRightSidebar = this.closeRightSidebar.bind(this);
    this.openSecondaryMenu = this.openSecondaryMenu.bind(this);
    this.closeSecondaryMenu = this.closeSecondaryMenu.bind(this);
    this.renderRoute = this.renderRoute.bind(this);
    this.renderRouteDevices = this.renderRouteDevices.bind(this);
    this.checkIsActivePrimary = this.checkIsActivePrimary.bind(this);
    this.checkIsActiveSecondary = this.checkIsActiveSecondary.bind(this);
    this.checkIsActiveThird = this.checkIsActiveThird.bind(this);
    this.hasSecondaryMenu = this.hasSecondaryMenu.bind(this);
    this.hasThirdMenu = this.hasThirdMenu.bind(this);
  }

  componentDidMount(){
    this.props.settingsActions.getSettings();
    this.props.devicesActions.getMoxas(this.props.history);
    this.props.devicesActions.getAllFixtures();
  }

  openRightSidebar() {
    // this.setState({
    //   isRightSidebarOpen: true
    // });
    this.props.sidebarActions.openSidebar()
  }

  closeRightSidebar() {
    // this.setState({
    //   isRightSidebarOpen: false
    // });
    this.props.sidebarActions.closeSidebar()
  }

  openSecondaryMenu() {
    this.setState({
      isSecondaryMenuOpen: true
    });
  }

  closeSecondaryMenu() {
    this.setState({
      isSecondaryMenuOpen: false
    });
  }

  renderRoute(newRoute) {
    this.props.history.replace(newRoute)
  }

  renderRouteDevices(newRoute) {
    this.props.history.replace(newRoute)
    let macAddress = this.props.history.location.pathname.split('/')[3]
    this.props.devicesActions.getFixtures(macAddress)
  }

  checkIsActivePrimary(location) {
    return this.props.history.location.pathname.split('/')[1] == location.split('/')[1]
  }

  checkIsActiveSecondary(location) {
    return this.props.history.location.pathname.indexOf(location) > -1;
  }

  checkIsActiveThird(location) {
    return this.props.history.location.pathname == location
  }

  hasSecondaryMenu() {
    return this.props.history.location.pathname.split("/")[1] == 'setup';
  }

  hasThirdMenu() {
    return this.props.history.location.pathname.split("/").length > 2 && this.props.history.location.pathname.split("/")[2] == 'devices';
  }

  render() {
    const sidebarComponents = {
      'CREATE_CANOPY_PROFILE_FORM': CanopyProfileFormCreate,
      'EDIT_CANOPY_PROFILE_FORM': CanopyProfileFormEdit,
      'CREATE_CANOPY_FORM': CanopyFormCreate,
      'EDIT_CANOPY_FORM': CanopyFormEdit,
      'CREATE_RECIPE_FORM': RecipeFormCreate,
      'EDIT_RECIPE_FORM': RecipeFormEdit,
      'CREATE_SCHEDULE_FORM': ScheduleFormCreate,
      'EDIT_SCHEDULE_FORM': ScheduleFormEdit,
      'REALTIME_CONTROL_FORM': RealtimeContainer,
      'ADD_CANOPY_TO_SCHEDULE_FORM': AddCanopyToSchedule,
      'ADD_FIXTURE_TO_CANOPY_FORM': AddFixtureToCanopy,
    }

    let SidebarComponent;
    try {
      SidebarComponent = sidebarComponents[this.props.rightSidebarComponent]
    } catch(err){
      console.error("ERR");
    }

    const notificationsStyle = getNotificationsStyle();

    return (
      <Wrapper isOpen={this.hasSecondaryMenu()}>
        <Notifications style={notificationsStyle} notifications={this.props.notifications} />
        <PrimaryMenu values={primaryMenu} openSecondaryMenu={this.openSecondaryMenu} closeSecondaryMenu={this.closeSecondaryMenu} renderRoute={this.renderRoute} checkIsActive={this.checkIsActivePrimary} isOpen={this.hasSecondaryMenu()} />
        <SecondaryMenu title="Setup" values={secondaryMenu} isOpen={this.hasSecondaryMenu()} checkIsActive={this.checkIsActiveSecondary} renderRoute={this.renderRoute} />
        <ThirdMenu title="Devices" refresh={() => this.props.devicesActions.getMoxas(this.props.history)} values={this.props.devices.moxaDisplay} isOpen={this.hasThirdMenu()} renderRoute={this.renderRoute} checkIsActive={this.checkIsActiveThird} renderRoute={this.renderRouteDevices} />

        <MainContent isSecondaryMenuOpen={this.hasSecondaryMenu()} isThirdMenuOpen={this.hasThirdMenu()} isRightSidebarOpen={this.props.isRightSidebarOpen}>
          { this.props.children }
        </MainContent>
        <RightSidebar goBack={() => this.props.sidebarActions.openSidebar(
            this.props.rightSidebarPrevState.component,
            this.props.rightSidebarPrevState.label,
            this.props.rightSidebarPrevState.title,
            this.props.rightSidebarPrevState.type
          )} type={this.props.rightSidebarType} label={this.props.rightSidebarLabel} title={this.props.rightSidebarTitle} isOpen={this.props.isRightSidebarOpen} closeRightSidebar={this.closeRightSidebar} >
          {SidebarComponent && <SidebarComponent />}
        </RightSidebar>
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    rightSidebarComponent: state.sidebar.component,
    rightSidebarLabel: state.sidebar.label,
    rightSidebarTitle: state.sidebar.title,
    rightSidebarType: state.sidebar.type,
    rightSidebarPrevState: state.sidebar.prevState,
    notifications: state.notifications,
    devices: state.devices
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: bindActionCreators(SidebarActions, dispatch),
    settingsActions: bindActionCreators(SettingsActions, dispatch),
    devicesActions: bindActionCreators(DevicesActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));