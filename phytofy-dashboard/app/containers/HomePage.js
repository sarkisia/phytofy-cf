// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';


import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
import FiltersContainer from './FiltersContainer';
import GridContainer from './GridContainer';

import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as CanopiesActions from '../actions/canopies';
import * as ProfilesActions from '../actions/profiles'; 


const CanopyContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [], view: 'grid' };
  }

  componentDidMount() {
    this.props.canopiesActions.getObjects();
    this.props.profilesActions.getObjectsAll();
  }

  render() {
    const tableAttributes = {
      name: {name: "Name", configs: []},
      '_profile.name': {name: "Layout", configs: [], transform: (row) => `${row._profile.name} (${canopyConfigs[row._profile.config]})` }
    }

    const getProfilesForSelect = (profiles) => {
      return profiles.map((profile) => {
          return {name: `${profile.name} (${canopyConfigs[profile.config]})`, value: profile._id}
      })
    };

    let profileFilter = [
      {value: -1, name: 'All'},
      ...getProfilesForSelect(this.props.profiles)
    ];

    const filters = [
      {label: 'Layout', options: profileFilter, attribute: 'profileId'}
    ]

    const rowOptions = [
      {optionMethod: this.props.canopiesActions.edit, iconName: 'edit'},
      {optionMethod: this.props.canopiesActions.duplicate, iconName: 'duplicate'},
      {optionMethod: this.props.canopiesActions.deleteObj, iconName: 'delete'}
    ];

    return (
      <div>
        <FiltersContainer search={(value) => {this.props.canopiesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.canopiesActions.addFilter} queryAction={this.props.canopiesActions.getObjectsAll} removeFilter={this.props.canopiesActions.removeFilter} />
        <GridContainer objects={this.props.objects} />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    objects: state.canopies.objects,
    count: state.canopies.count,
    profiles: state.profiles.objectsAll,
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    canopiesActions : {...bindActionCreators(CanopiesActions, dispatch)},
    profilesActions: {...bindActionCreators(ProfilesActions, dispatch)}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CanopyContainer);