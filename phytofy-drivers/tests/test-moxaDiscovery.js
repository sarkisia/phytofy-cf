const assert = require('assert');
const moxaUtils = require('../src/moxa-discovery_utils.js')

describe('Discover Moxas', function() {
  describe('utils', function() {
    describe('#parseCheck()', function() {
      let correctMsg = Buffer.from([0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x90, 0xE8,
       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
      it('Should return true (pass all checks)', function() {
        assert.equal(moxaUtils.parseCheck(correctMsg), true);
      });
      it('Should return false for wrong length', function() {
        assert.equal(moxaUtils.parseCheck(Buffer.from([0x01, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00 ])), false);
      });
      it('Should return false for wrong function code', function() {
        let incorrMsg = correctMsg;
        incorrMsg[0] = 0x91;
        assert.equal(moxaUtils.parseCheck(incorrMsg), false);
      });
      it('Should return false for wrong mac address', function() {
        let incorrMsg = correctMsg;
        incorrMsg[15] = 0x91;
        incorrMsg[16] = 0xA1;
        assert.equal(moxaUtils.parseCheck(Buffer.from([0x01, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00 ])), false);
      });
    });
    describe('#isRepeated()', function(){
      let moxaList = [
	       {ip: '192.168.1.20', ports: [4001, 4002]},
	       {ip: '192.168.1.23', ports: [4001, 4002, 4003, 4004]}
      ]
      it('Should return true, repeated address', function() {
        assert.equal(moxaUtils.isRepeated('192.168.1.20', moxaList), true);
      });
      it('Should return false, new address', function() {
        assert.equal(moxaUtils.isRepeated('192.168.1.30', moxaList), false);
      });
    });
  });
});
