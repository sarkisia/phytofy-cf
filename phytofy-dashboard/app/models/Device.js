const electron = require('electron');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';

let Device;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Device = new Datastore({ filename: `${userDataPath}/devices.db`, autoload: true });
  try {
    Device.ensureIndex({ fieldName: 'name', unique: true });
  } catch (err) {
    console.error(err);
  }

  return Device;
}

export default Device ? Device : createDatastore();

