// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import {reducer as notifications} from 'react-notification-system-redux'; 
import sidebar from './sidebar';
import profiles from './profiles';
import canopies from './canopies';
import canopiesSetup from './canopiesSetup';
import devices from './devices';
import recipes from './recipes';
import schedules from './schedules';
import settings from './settings';

const rootReducer = combineReducers({
  sidebar,
  profiles,
  canopies,
  canopiesSetup,
  recipes,
  notifications,
  devices,
  router,
  settings,
  schedules
});

export default rootReducer;
