import React, { Component } from 'react'
import styled, { css } from 'styled-components'

import Label from '../atoms/Label'
import Text from '../atoms/Text'
import Input from '../atoms/Input'
import Select from '../atoms/Select'
import Checkbox from '../atoms/Checkbox'
import WavelenghtChannel from '../atoms/WavelenghtChannel'

const FormGroup = styled.div`
    &:not(:last-child) {
        margin-bottom: 30px;
    }

    &.error {
        label {
            color: var(--red, #E53012);
        }

        input {
            border-bottom-color: var(--red, #E53012);
        }

        select {
            border-bottom-color: var(--red, #E53012);
        }
    }

    label {
        margin-bottom: 8px;
    }
`;

const Wavelenght = styled.ul `
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
`

const Error = styled.span`
    color: var(--red, #E53012);
    font-size: .75rem;
`

export default class extends Component {
    render() {
        let inputElement;
        let values = this.props.values ? this.props.values : [];

        const channels = values.map((value) =>
            <WavelenghtChannel name={value.name} state={value.state} />
        );

        switch(this.props.type){
            case 'detail':
                inputElement = <Text>{this.props.value}</Text>;
                break;
            case 'wavelenght':
                inputElement = <Wavelenght>{ channels }</Wavelenght>;
                break;
            case 'text':
                inputElement = <Input type={this.props.type} name={this.props.name} placeholder={this.props.placeholder} value={this.props.value} onChange={this.props.onChange} />;
                break;
            case 'time':
                inputElement = <Input type={this.props.type} name={this.props.name} placeholder={this.props.placeholder} value={this.props.value} min={this.props.min} max={this.props.max} onChange={this.props.onChange} />;
                break;
            case 'select':
                inputElement = <Select values={this.props.options} name={this.props.name} value={this.props.value} defaultValue={this.props.defaultValue} onChange={this.props.onChange} />;
                break;
            case 'checkbox':
                inputElement = <Checkbox name={this.props.name} checkboxLabel={this.props.checkboxLabel} defaultValue={this.props.defaultValue} onChange={this.props.onChange} />;
                break;
            case 'file':
                inputElement = <Input type={this.props.type} name={this.props.name} onChange={this.props.onChange} />;
                break;
            case 'search':
                inputElement = <Input type={this.props.type} name={this.props.name} placeholder={this.props.placeholder} value={this.props.value} onChange={this.props.onChange} />;
                break;
            default:
                inputElement = <Input type={this.props.type} name={this.props.name} placeholder={this.props.placeholder} value={this.props.value} onChange={this.props.onChange} />;
        }

        const error = this.props.error;

        if(this.props.hidden){
            return <span />;
        }

        return (
            <FormGroup className={error ? 'error' : ''}>
                <Label label={this.props.label} />
                { inputElement }
                <Error>{this.props.error}</Error>
            </FormGroup>
        );
    }
}
