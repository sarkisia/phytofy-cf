import React,{Component} from 'react';

import CanopyConfig from 'phytofy-design-system/src/design-system/atoms/CanopyConfig';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import FormGroupList from 'phytofy-design-system/src/design-system/molecules/FormGroupList';
import ListItem from 'phytofy-design-system/src/design-system/atoms/ListItem';

import i18n from '../../i18n';

import FormContainer from './FormContainer';
import { getCanopyConfigsForSelect, getImageForConfig, canopyConfigsNFixtures } from '../models/Profile';

const CanopyForm = class extends Component {
    constructor(props){
        super();
        this.props = props;
        if(this.props.obj.fixtures) this.state = {...this.props.obj};
        else this.state = {...this.props.obj, fixtures: []};
        this.configChangeHandler = this.configChangeHandler.bind(this);
    }
    
    componentDidUpdate(prevProps){
        if(this.props.obj != prevProps.obj){
            this.setState({...this.props.obj})
        }
    }

    getData() {
        return {...this._form.getData(), fixtures: this.state.fixtures};
    }

    configChangeHandler(event) {
        this.setState({profileId: event.target.value});
    }

    render() {
        const getProfilesForSelect = (profiles) => {
            return profiles.map((profile) => {
                return {name: profile.name, value: profile._id}
            })
        }

        const getConfigForProfile = (profiles) => {
            for(let pIndex in profiles){
                let profile = profiles[pIndex];
                if(profile._id == this.state.profileId){
                    return profile.config;
                }
            }
        }

        const removeFixture = (fixture) => {
            let fixtures = this.state.fixtures;
            if(fixtures.indexOf(fixture) != -1) {
                fixtures.splice(fixtures.indexOf(fixture), 1);
            }

            this.setState({fixtures});
        }

        return (
            <FormContainer obj={this.props.obj} validation={this.props.validation} actions={this.props.actions} ref={(ref) => this._form = ref}>
                <CanopyConfig config={getImageForConfig(getConfigForProfile(this.props.profiles))} />
                <FormGroup type="select" label={i18n.t("layout")} name="profileId" options={getProfilesForSelect(this.props.profiles)} value={this.state.profileId} onChange={this.configChangeHandler} />
                <FormGroup type="text" label={i18n.t("name")} name="name" placeholder="" value={this.state.name} onChange={this.handleInputChange} error={this.props.validation.name && this.props.validation.name.message}/>
                <FormGroupList label={i18n.t("fixtures")}>
                    <ListItem type="add" value="Add new" onClick={() => this.props.addFixture(this.getData())} />
                    {this.state.fixtures.map((fixture) => {
                        return <ListItem value={fixture} onClick={() => removeFixture(fixture)}/>
                    })}
                </FormGroupList>
            </FormContainer>
        )
    }
}

export default CanopyForm;