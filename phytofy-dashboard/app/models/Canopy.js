const electron = require('electron');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

import FormValidator from '../utils/FormValidator';
import Fixture from './Fixture';

let Canopy;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Canopy = new Datastore({ filename: `${userDataPath}/canopies.db`, autoload: true });
  try {
    Canopy.ensureIndex({ fieldName: 'name', unique: true });
  } catch (err) {
    console.error(err);
  }

  return Canopy;
}

export default Canopy ? Canopy : createDatastore();

const rules = [
  {
    field: 'name',
    method: validator.isEmpty,
    validWhen: false,
    message: 'Name is required'
  },
  {
    field: 'name',
    method: async (value) => {
      let count = await Canopy.count({name: value});
      return count == 0;
    },
    validWhen: true,
    ignoreEqualsPrev: true,
    message: 'A canopy with this name already exists'
  },
  {
    field: 'name',
    method: validator.isLength,
    args: [{min: 0, max: 30}],
    validWhen: true,
    message: 'Name exceeds the maximum length of 30'
  },
  {
    field: 'name',
    method: async (value) => {
      return /^[A-Za-z\d\s]+$/.test(value)
    },
    validWhen: true,
    message: 'Name contains invalid characters'
  },
];

export const CanopyFormValidator = new FormValidator(rules);

export const getFixturesFromCanopy = async (canopy) => {
  return await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
}