import React, { Component } from 'react';
import styled from 'styled-components';

const Paragraph = styled.p`
  display: block;
  font-size: 1rem;
  color: var(--textColor, #424242);
`;

export default class extends Component {
	render() {
		return (
			<Paragraph>{this.props.children}</Paragraph>
		)
	}
}
