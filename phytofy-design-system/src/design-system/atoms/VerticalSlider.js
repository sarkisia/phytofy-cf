import React, { Component } from "react";
import styled, { css } from "styled-components";
import Slider from "rc-slider";

const VerticalSlider = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    color: var(--textColor, #424242);

    .rc-slider {
        position: relative;
        margin: 10px 0;
        width: 100%;
        height: 14px;
        padding: 5px 0;
        border-radius: 4px;
        -ms-touch-action: none;
        touch-action: none;
        box-sizing: border-box;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);

        * {
            box-sizing: border-box;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        &-rail {
            position: absolute;
            background-color: var(--topbarBorder, #eceded);
            width: 100%;
            height: 4px;
            border-width: 2px;
            border-style: solid;
            border-radius: 4px;
        }

        &-track {
            position: absolute;
            left: 0;
            height: 4px;
            border-width: 2px;
            border-style: solid;
            border-radius: 4px;
            background-color: var(--white, #FFFFFF);
        }

        &-handle {
            position: absolute;
            background-color: var(--backgroundColor, #FFFFFF);
            margin-left: -7px;
            margin-top: -5px;
            width: 15px;
            height: 15px;
            border-width: 2px;
            border-style: solid;
            border-radius: 50%;
            cursor: pointer;
            cursor: -webkit-grab;
            cursor: grab;
            -ms-touch-action: pan-x;
            touch-action: pan-x;

            &:active {
                cursor: -webkit-grabbing;
                cursor: grabbing;
            }
        }

        &-vertical {
            width: 40px;
            height: 100px;
            padding: 0;
        }
    }

    .rc-slider-vertical {
        .rc-slider-rail {
            width: 40px;
            height: 100%;
        }

        .rc-slider-track {
            bottom: 0;
            width: 40px;
        }

        .rc-slider-handle {
            margin-left: calc(50% - (15px / 2));
            margin-bottom: calc(-15px / 2);
            -ms-touch-action: pan-y;
            touch-action: pan-y;
        }
    }

    ${props => props.channel === "uv" && css`
        .rc-slider-rail, .rc-slider-handle {
            border-color: var(--channelUV, rgb(106, 27, 154));
        }

        .rc-slider-track {
            background-color: rgba(106, 27, 154, .5);
            border-color: var(--channelUV, rgb(106, 27, 154));
        }
    `}

    ${props => props.channel === "blue" && css`
        .rc-slider-rail, .rc-slider-handle {
            border-color: var(--channelBlue, rgb(97, 175, 241));
        }

        .rc-slider-track {
            background-color: rgba(97, 175, 241, .5);
            border-color: var(--channelBlue, rgb(97, 175, 241));
        }
    `}

    ${props => props.channel === "green" && css`
        .rc-slider-rail, .rc-slider-handle {
            border-color: var(--channelGreen, rgb(9, 228, 37));
        }

        .rc-slider-track {
            background-color: rgba(9, 228, 37, .5);
            border-color: var(--channelGreen, rgb(9, 228, 37));
        }
    `}

    ${props => props.channel === "hyperRed" && css`
        .rc-slider-rail, .rc-slider-handle {
            border-color: var(--channelHyperRed, rgb(198, 40, 40));
        }

        .rc-slider-track {
            background-color: rgba(198, 40, 40, .5);
            border-color: var(--channelHyperRed, rgb(198, 40, 40));
        }
    `}

    ${props => props.channel === "farRed" && css`
        .rc-slider-rail, .rc-slider-handle {
            border-color: var(--channelFarRed, rgb(172, 42, 24));
        }

        .rc-slider-track {
            background-color: rgba(172, 42, 24, .5);
            border-color: var(--channelFarRed, rgb(172, 42, 24));
        }
    `}

    ${props => props.channel === "warmWhite" && css`
        .rc-slider-rail, .rc-slider-handle {
            border-color: var(--channelWarmRed, rgb(167, 168, 170));
        }

        .rc-slider-track {
            background-color: rgba(167, 168, 170, .5);
            border-color: var(--channelWarmRed, rgb(167, 168, 170));
        }
    `}

    ${props => props.channel === "total" && css`
        .rc-slider-handle {
            display: none;
        }
    `}

    input {
        border: 0;
        border-bottom: 1px solid rgba(0, 0, 0, 0);
        max-width: 40px;
        padding-bottom: 5px;
        font-size: 1rem;
        color: var(--textColor, #424242);
        text-align: center;

        &:focus {
            border-bottom: 1px solid var(--textColor, #424242);
        }
    }
`;

/* Log slider value */

function log(value) {
    document.getElementById("sliderValue").innerHTML = value;
}

export default class extends Component {
    constructor(props) {
        super(props);

        // this.state = {
        //     value: this.props.value,
        // };

        // this.handleChange = this.handleChange.bind(this);
    }

    // handleChange(event) {
    //     this.setState({value: event.target.value});
    // }

    render() {
        return (
            <VerticalSlider channel={this.props.channel} >
                <input id="sliderPercentage" value={this.props.value} maxlength="3" onChange={(event) => this.props.onPercentageChange(event.target.value)} disabled={this.props.disabled} />

                <Slider
                    disabled={this.props.disabled}
                    vertical
                    min={this.props.min}
                    max={this.props.max}
                    defaultValue={this.props.defaultValue}
                    onChange={this.props.onChange}
                    onAfterChange={this.props.onAfterChange}
                    value={this.props.value}
                />

                <input id="sliderValue" value={this.props.irradiance} maxlength="3" onChange={(event) => this.props.onIrradianceChange(event.target.value)} disabled={this.props.disabled} />
            </VerticalSlider>
        );
    }
}
