import styled, { css } from 'styled-components';

const Content = styled.div`
    padding: calc(60px + var(--spacing, 30px)) var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);

    ${props => props.cardList && css `
        display: grid;
        grid-gap: var(--spacing, 30px);
        grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
    `}
`

export default Content;
