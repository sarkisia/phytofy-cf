import React, { Component } from 'react';
import styled from 'styled-components';

const Toggle = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;

    label {
        margin: 0 .5rem 0;
        color: var(--textColor, #424242);
        cursor: pointer;
    }

    input[type="radio"] {
        position: relative;
        display: inline-block;
        margin: 0;
        width: 24px;
        height: 100%;
        opacity: 0;
        z-index: 1;
        cursor: pointer;
    }

    .switcher {
        display: block;
        position: absolute;
        top: -3px;
        left: 0;
        right: 100%;
        width: 29px;
        height: 29px;
        border-radius: 50%;
        background-color: var(--primaryColor, #FF6600);
        transition: all 0.1s ease-out;
        z-index: 2;
    }

    .background {
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 0;
        border-radius: 25px;
        background-color: var(--backgroundPrimaryMenu, #F5F6F8);
        transition: all 0.1s ease-out;
    }

    #right:checked ~ .switcher {
        right: 0;
        left: calc(50% - 2px);
    }

    #right:checked ~ .background {
        background-color: var(--textColor, #424242);
    }
`

const Wrapper = styled.span `
    position: relative;
    display: inline-block;
    border-radius: 25px;
    border: 1px solid var(--topbarBorder, #ECEDED);
    width: 50px;
    height: 25px;
    vertical-align: middle;
`

export default class extends Component {
    render() {
        return (
            <Toggle>
                <label for="left">{this.props.optionLeft}</label>
	            <Wrapper>
		            <input type="radio" name="theme" id="left" checked />
		            <input type="radio" name="theme" id="right" />
		            <span aria-hidden="true" class="background"></span>
		            <span aria-hidden="true" class="switcher"></span>
	            </Wrapper>
	            <label for="right">{this.props.optionRight}</label>
            </Toggle>
        )
    }
}
