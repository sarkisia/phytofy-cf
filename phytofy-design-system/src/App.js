import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import PrimaryMenu from './design-system/organisms/PrimaryMenu'
import RightSidebar from './design-system/organisms/RightSidebar'
import Wrapper from './design-system/organisms/Wrapper'
import MainContent from './design-system/organisms/MainContent'
// import Commissioning from './design-system/organisms/Commissioning'
import './css/App.css';

/* Templates */

import CanopiesGridList from './design-system/templates/CanopiesGridList'
import CanopiesList from './design-system/templates/CanopiesList'
import CanopyProfiles from './design-system/templates/CanopyProfiles'
import RecipesTemplate from './design-system/templates/RecipesTemplate'
import EditProfile from './design-system/templates/EditProfile'
import ConnectedFixture from './design-system/templates/ConnectedFixture'
import FixtureDetails from './design-system/templates/FixtureDetails'
import ViewRecipe from './design-system/templates/ViewRecipe'
import RealTimeControl from './design-system/templates/RealTimeControl'
import Settings from './design-system/templates/Settings'

/* Navigation */

const activeLocation = '/';

const primaryMenu = [
  { icon: 'fixture', location: '/' },
  { icon: 'recipes', location: '/recipes' },
  { icon: 'schedule', location: '/schedule' },
  { icon: 'setup', location: '/setup' },
];

const Canopies = () => (<CanopiesGridList />);
const Recipes = () => (<RecipesTemplate />);
const Schedule = () => (<div>schedule</div>);

const CanopiesSetupList = () => (<CanopiesList />);
const Devices = () =>  (<div>devices</div>);
const Profiles = () =>  (<CanopyProfiles />);
const Diagnostic = () =>  (<div>diagnostic</div>);
const SetupSettings = () =>  (<Settings />);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRightSidebarClosed: true
    };

    this.openRightSidebar = this.openRightSidebar.bind(this);
    this.closeRightSidebar = this.closeRightSidebar.bind(this);
    this.renderRoute = this.renderRoute.bind(this);
    this.checkIsActive = this.checkIsActive.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  openRightSidebar(){ this.setState({isRightSidebarClosed: false}) }
  closeRightSidebar(){ this.setState({isRightSidebarClosed: true}) }

  renderRoute(newRoute) {
    this.props.history.replace(newRoute)
  }

  checkIsActive(path) {
    return path.split('/')[1] === activeLocation.split('/')[1]
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <Router>
        <Wrapper>
          {/* <Commissioning /> */}

          <PrimaryMenu values={primaryMenu} checkIsActive={this.checkIsActive} />

          <MainContent isRightSidebarOpen={!this.state.isRightSidebarClosed}>
            <Route exact path="/" component={Canopies} />
            <Route path="/recipes" component={Recipes} />
            <Route path="/schedule" component={Schedule} />
            <Route path="/setup" component={Settings} />

            <Route path="/canopies-list" component={CanopiesSetupList} />
            <Route path="/devices" component={Devices} />
            <Route path="/profiles" component={Profiles} />
            <Route path="/diagnostic" component={Diagnostic} />
            <Route path="/setup/settings" component={SetupSettings} />

            <button onClick={this.openRightSidebar}>Sidebar</button>
          </MainContent>

          {/* <RightSidebar type="xl" label="Real-time" title="Canopy Control" isOpen={true} closeRightSidebar={this.closeRightSidebar}> */}
          <RightSidebar type="" label="" title="" isOpen={!this.state.isRightSidebarClosed} closeRightSidebar={this.closeRightSidebar}>
          {/* <RightSidebar label="Fixture" title="00000000000" isOpen={!this.state.isRightSidebarClosed} closeRightSidebar={this.closeRightSidebar}> */}
            {/* <EditProfile /> */}
            <ConnectedFixture />
            {/* <FixtureDetails /> */}
            {/* <ViewRecipe /> */}
            {/* <RealTimeControl /> */}
          </RightSidebar>
        </Wrapper>
      </Router>
    );
  }
}

export default App;
