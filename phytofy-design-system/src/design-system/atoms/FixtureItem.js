import React, { Component } from 'react';
import styled from 'styled-components';

const FixtureItem = styled.dt `
    display: flex;
    flex-direction: row;
    align-items: center;

    &:not(:last-child) {
        margin-bottom: 6px;
    }

    &:before {
        display: inline-block;
        margin-right: 10px;
        background-color: var(--primaryColor, #FF6600);
        border-radius: 10px;
        width: 20px;
        height: 20px;
        text-align: center;
        font-size: .75rem;
        line-height: 20px;
        vertical-align: middle;
        color: var(--backgroundColor, #FFFFFF);
        content: counter(list-counter);
	    counter-increment: list-counter;
        z-index: 1;
    }
`

const AddFixture = styled.a `
    margin-left: -10px;
    border-width: 1px 1px 1px 0;
    border-style: solid;
    border-color: var(--primaryColor, #FF6600);
    border-radius: 0 4px 4px 0;
    background-color: #FFE0CC;
    padding: 0 12px;
    color: var(--primaryColor, #FF6600);
    cursor: pointer;
`

const SemiCircle = styled.div `
    position: absolute;
    margin: -1px 0 0 -26px;
    width: 20px;
    height: 21px;
    border-right-width: 1px;
    border-style: solid;
    border-color: var(--primaryColor, #FF6600);
    border-radius: 0 10px 10px 0;
    background-color: var(--backgroundColor, #FFFFFF);
`

export default class extends Component {
    render() {
        const value = this.props.value;

        const add =
            <AddFixture>
                <SemiCircle />
                {this.props.addText}
            </AddFixture>;

        return (
            <FixtureItem>
                {value ? value : add}
            </FixtureItem>
        )
    }
}
