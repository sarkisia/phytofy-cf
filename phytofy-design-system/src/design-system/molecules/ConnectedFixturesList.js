import React, { Component } from 'react'
import styled from 'styled-components'

import Label from '../atoms/Label'

const ConnectedFixturesList = styled.div `
    color: var(--textColor, #424242);
`

const ConnectedFixturesOrderList = styled.dl `
    margin-top: 10px;
    counter-reset: list-counter;
`

export default class extends Component {
    render() {
        return (
            <ConnectedFixturesList>
                <Label label={this.props.label} />

                <ConnectedFixturesOrderList>
                    {this.props.children}
                </ConnectedFixturesOrderList>
            </ConnectedFixturesList>
        );
    }
}
