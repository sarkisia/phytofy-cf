import React, { Component } from "react";

import Button from "../atoms/Button";
import CanopyConfig from "../atoms/CanopyConfig";
import Form from "../organisms/Form";
import FormGroup from "../molecules/FormGroup";
import FormActions from "../molecules/FormActions";
import ConnectedFixturesList from "../molecules/ConnectedFixturesList";
import FixtureItem from "../atoms/FixtureItem";

// import Config1H from '../../images/1h.svg'
// import Config1V from '../../images/1v.svg'
// import Config2H from '../../images/2h.svg'
import Config2V from "../../images/2v.svg";
// import Config2X2H from '../../images/2x2h.svg'
// import Config3V from '../../images/3v.svg'
// import Config4V from '../../images/4v.svg'

/* Canopy Configuration on Sidebar */

const canopyConfig = [
    { value: "1v", name: "1V" },
    { value: "1h", name: "1H" },
    { value: "2v", name: "2V" },
    { value: "2h", name: "2H" },
    { value: "3v", name: "3V" },
    { value: "2x2h", name: "2x2H" },
    { value: "3v", name: "4V" }
];

class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = { isRightSidebarClosed: true, isSecondaryMenuClosed: true };
    }

    render() {
        return (
            <div>
                <CanopyConfig config={Config2V} />

                <Form>
                    <FormGroup
                        type="text"
                        label="Input Name"
                        placeholder="example text"
                        name="text"
                        value={this.state.text}
                        error=""
                        onChange={this.handleInputChange}
                    />

                    <FormGroup
                        type="select"
                        label="Select Name"
                        options={canopyConfig}
                        error=""
                    />

                    <ConnectedFixturesList label="Connected Fixtures">
                        <FixtureItem value="OSR074F2B29-H9" />
                        <FixtureItem value="OSR074F2B29-H6" />
                        <FixtureItem value="" addText="Connected fixture" />
                    </ConnectedFixturesList>

                    <FormActions>
                        <Button state="cancel" name="Cancel" onClick={this.props.closeRightSidebar} />
                        <Button state="success" name="Submit" onClick="console.log('click')" />
                    </FormActions>
                </Form>
            </div>
        );
    }
}

export default EditProfile;
