import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import * as RecipesActions from '../actions/recipes';  
import RecipeForm from './RecipeForm';

const RecipeFormEdit = class extends Component {
    render() {
        const actions = [
            <Button type="cancel" name="Cancel" onClick={() => this.props.cancelForm() }/>,
            <Button type="success" name="Submit" onClick={() => this.props.submitEdit(this._form.getData())} />
        ]

        return (
            <RecipeForm actions={actions} validation={this.props.validation} obj={this.props.obj} ref={(ref) => this._form = ref}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        obj: state.recipes.recipeObj,
        validation: state.recipes.validation,
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(RecipesActions, dispatch);
  }
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RecipeFormEdit));