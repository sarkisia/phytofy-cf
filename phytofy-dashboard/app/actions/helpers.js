import Notifications from 'react-notification-system-redux';

import { getPageSize } from '../models/Settings';
import Fixture from '../models/Fixture';
import Moxa from '../models/Moxa';

export const getPaginationInfo = async (from, count) => {
    let pageLimit = await getPageSize();
    if (from > count) {
        from = count;
    }
    let to = from + parseInt(pageLimit) - 1;
    if (to > count) {
        to = count;
    }
    let hasPrev = from > 1;
    let hasNext = to < count;

    return {
        pageLimit,
        from,
        to,
        hasPrev,
        hasNext
    }
}

export const getNewSortType = (newSortAttribute, prevSortAttribute, prevSortType) => {
    if (newSortAttribute == prevSortAttribute) {
        if (prevSortType == 'orderAsc') {
            return 'orderDesc';
        } else {
            return 'orderAsc';
        }
    } else {
        return 'orderAsc';
    }
}

export const countQuery = async (filters = {}, ObjType) => {
    try {
        return ObjType.ccount(filters).exec();
    } catch (err) {
        console.error(err);
        return 0;
    }
}

export const makeGetObjectsAction = (type, stateKey, ObjType, queryFunc) => {
    return () => {
        return async (dispatch, getState) => {
            let { sortAttribute, sortType, from, filters } = getState()[stateKey];
            if (from == 0) from = 1;
            let objects = await queryFunc(sortAttribute, sortType, from, filters);
            let count = await countQuery(filters, ObjType);
            dispatch({
                type: type,
                objects,
                count,
                ...await getPaginationInfo(from, count)
            })
        }
    }
}

export const makeGetObjectsAllAction = (type, stateKey, ObjType, queryFunc) => {
    return (withFilters) => {
        return async (dispatch, getState) => {
            let filters = null;
            if (withFilters) filters = getState()[stateKey].filters;
            let objects = await queryFunc({}, null, 1, filters, true);
            dispatch({
                type: type,
                objects
            })
        }
    }
}

export const makeSortAction = (type, stateKey, queryAction) => {
    return (newSortAttribute) => {
        return async (dispatch, getState) => {
            const { sortAttribute, sortType, from } = getState()[stateKey];
            let newSortType = getNewSortType(newSortAttribute, sortAttribute, sortType);
            await dispatch({
                type: type,
                sortAttribute: newSortAttribute,
                sortType: newSortType,
            })

            dispatch(queryAction());
        }
    }
}

export const makeAddFilterAction = (type, stateKey, queryAction) => {
    return (filter, value) => {
        return async (dispatch, getState) => {
            let filters = { ...getState()[stateKey].filters, [filter]: value };
            await dispatch({
                type: type,
                from: 1,
                filters
            })

            dispatch(queryAction());
        }
    }
}

export const makeRemoveFilterAction = (type, stateKey, queryAction) => {
    return (filter, ) => {
        return async (dispatch, getState) => {
            let filters = { ...getState()[stateKey].filters };
            delete filters[filter];

            await dispatch({
                type: type,
                from: 1,
                filters
            })

            dispatch(queryAction());
        }
    }
}

export const makePageBack = (type, stateKey, queryAction, ObjType) => {
    return () => {
        return async (dispatch, getState) => {
            const { sortAttribute, sortType, from, hasPrev, filters } = getState()[stateKey];
            if (!hasPrev) return
            let count = await countQuery(filters, ObjType);
            let pageSize = await getPageSize();
            let newFrom = from - pageSize;
            if (newFrom < 1) newFrom = 1;
            await dispatch({
                type: type,
                count,
                from: newFrom,
                ...await getPaginationInfo(newFrom, count)
            })

            dispatch(queryAction());
        }
    }
}

export const makePageNext = (type, stateKey, queryAction, ObjType) => {
    return () => {
        return async (dispatch, getState) => {
            const { sortAttribute, sortType, from, hasNext, filters } = getState()[stateKey];
            if (!hasNext) return
            let count = await countQuery(filters, ObjType);
            let pageSize = await getPageSize();
            await dispatch({
                type: type,
                count,
                from: from + pageSize,
                ...await getPaginationInfo(from + pageSize, count)
            })

            dispatch(queryAction());
        }
    }
}

export const makePageGoTo = (type, stateKey, queryAction, ObjType) => {
    return (goto) => {
        return async (dispatch, getState) => {
            const { sortAttribute, sortType, from, filters } = getState()[stateKey];
            let count = await countQuery(filters, ObjType);
            if (goto <= 0 || goto > count) return;
            let pageSize = await getPageSize();

            await dispatch({
                type: type,
                count,
                from: goto,
                ...await getPaginationInfo(goto, count)
            })

            dispatch(queryAction());
        }
    }
}

export const makeSelectObject = (type, stateKey) => {
    return (objId) => {
        return async (dispatch, getState) => {
            let selected = [...getState()[stateKey].selected, objId];
            dispatch({
                type: type,
                selected
            })
        }
    }
}

export const makeDeselectObject = (type, stateKey) => {
    return (objId) => {
        return async (dispatch, getState) => {
            let selected = [];
            getState()[stateKey].selected.map((tmpId) => {
                if (tmpId != objId) selected.push(tmpId);
            })
            dispatch({
                type: type,
                selected
            })
        }
    }
}

export const makeSelectAll = (type, stateKey) => {
    return () => {
        return async (dispatch, getState) => {
            let selected = getState()[stateKey].objects.map((obj) =>
                obj._id
            );

            dispatch({
                type: type,
                selected
            })
        }
    }
}

export const makeDeselectAll = (type) => {
    return () => {
        return async (dispatch, getState) => {
            dispatch({
                type: type,
                selected: []
            })
        }
    }
}

export const runCommandOnFixtures = async (fixtures, comm, dispatch, callback) => {
    let currPort = null;
    let currMac = null;
    let connected = false;
    let errors = {};

    for (let i = 0; i < fixtures.length; i++) {
        let fixture = fixtures[i];
        if (fixture.port != currPort || fixture.moxa_mac != currMac) {
            await comm.closeConnection();
            connected = false;
            let openConnectionResult = await comm.openConnection(fixture.port, fixture.moxa_mac);
            if (openConnectionResult < 0) {
                if (Object.keys(errors).indexOf(fixture.moxa_mac) == -1) errors[fixture.moxa_mac] = {};
                errors[fixture.moxa_mac][fixture.port] = true;
                continue;
            } else {
                connected = true;
            }
            currPort = fixture.port;
            currMac = fixture.moxa_mac;
        }

        await callback(fixture)
    }

    Object.keys(errors).forEach((mac) => {
        let moxaErrors = errors[mac];
        Object.keys(moxaErrors).forEach(async (port) => {
            let hasPortErrors = moxaErrors[port];
            if (hasPortErrors) {
                try {
                    let moxa = (await Moxa.cfind({ mac: mac }).exec())[0];
                    dispatch(Notifications.error({
                        title: "Cannot reach Moxa server",
                        message: `Impossible to connect to Moxa server on IP ${moxa.ip} [Port ${port}]`
                    }));
                } catch (err) {
                    console.log(err);
                    dispatch(Notifications.error({
                        title: "Cannot reach Moxa server",
                        message: `Can't find Moxa server`
                    }));
                }
            }
        });
    });

    if (connected) await comm.closeConnection();
}