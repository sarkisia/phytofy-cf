import React, { Component } from 'react';
import styled from 'styled-components';

const Input = styled.input`
  outline: none;
  border: 0;
  font-size: 1rem;

  &::-webkit-search-decoration {
    display: none;
  }

  &::-webkit-search-cancel-button {
    display: none;
  }

  &[type=search] {
    background: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'><path fill='rgb(66,66,66)' d='M20.667 18.667h-1.053l-0.373-0.36c1.6-1.867 2.427-4.413 1.973-7.12-0.627-3.707-3.72-6.667-7.453-7.12-5.64-0.693-10.387 4.053-9.693 9.693 0.453 3.733 3.413 6.827 7.12 7.453 2.707 0.453 5.253-0.373 7.12-1.973l0.36 0.373v1.053l5.667 5.667c0.547 0.547 1.44 0.547 1.987 0v0c0.547-0.547 0.547-1.44 0-1.987l-5.653-5.68zM12.667 18.667c-3.32 0-6-2.68-6-6s2.68-6 6-6 6 2.68 6 6-2.68 6-6 6z'></path></svg>") no-repeat 19px center;
    background-size: 24px 24px;
    width: var(--topBarHeight, 60px);
  	color: transparent;
  	cursor: pointer;
  	transition: all .4s;

    &:focus {
    	width: 209px;
      padding: 0 20px 0 50px;
    	color: var(--textColor, #424242);
    	cursor: auto;

      &::placeholder {
        color: #BDBDBD;
      }
    }
  }

  &[type=search]:focus {
  	width: 209px;
  }

  &:-moz-placeholder {
  	color: transparent;
  }

  &::-webkit-input-placeholder {
  	color: transparent;
  }
`;

export default class extends Component {
	constructor(props) {
    super(props);
    this.state = {value: this.props.value};
    this.handleChange = this.handleChange.bind(this);
  }

	handleChange(event) {
    this.setState({value: event.target.value});
    this.props.onChange(event.target.value)
  }

	render() {
		return (
      <Input type="search" placeholder={this.props.placeholder} onChange={this.handleChange} />
		)
	}
}
