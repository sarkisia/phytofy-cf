import styled from 'styled-components';

export default styled.tr `
  position: relative;
  height: var(--rowHeight, 50px);
  text-align: left;
`
