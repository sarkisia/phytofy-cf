import React, { Component } from 'react';
import styled, { css } from 'styled-components';

const LoadingBarContainer = styled.div `
	width: 100%;
	max-width: 800px;
`

const LoadingBar = styled.div `
	display: flex;
	flex-direction: column;
	align-items: flex-end;
	width: ${(props) => props.progress + '%'};
`

const Percentage = styled.div `
	margin: 0 -25px 10px 0;
	background-color: var(--primaryColor, #FF6600);
	display: flex;
	justify-content: center;
	align-items: center;
	border-radius: 15px;
	width: 60px;
	height: 30px;
	font-weight: var(--bold, 700);
	color: var(--white, #FFFFFF);

	&:before {
		position: absolute;
		align-self: flex-end;
		margin-bottom: -5px;
	  display: inline-block;
	  border: 5px solid transparent;
	  border-bottom-width: 0;
	  border-top-color: var(--primaryColor, #FF6600);
	  content: "";
	}
`

const Bar = styled.div `
	width: 100%;
	height: 14px;
	border: 2px solid #FF6600;
	border-radius: 7px;
	background: repeating-linear-gradient(-45deg, #FF6600, #FF6600 11px, #FF7C26 10px, #FF7C26 20px);
	background-size: 28px 28px;
	animation: animation 1s linear infinite;

	@keyframes animation {
		0% {
			background-position: 0 0;
		}
		100% {
			background-position: 28px 0;
		}
	}
`

export default class extends Component {
  render() {
    return (
			<LoadingBarContainer>
				<LoadingBar progress={this.props.progress}>
					<Percentage>{this.props.progress}%</Percentage>
        	<Bar />
				</LoadingBar>
      </LoadingBarContainer>
    );
  }
}
