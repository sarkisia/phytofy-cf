import React, { Component } from 'react'

import Button from '../atoms/Button'
import CanopyConfig from '../atoms/CanopyConfig'
import FormGroup from '../molecules/FormGroup'
import FormActions from '../molecules/FormActions'

import Config1H from '../../images/1h.svg'
import Config1V from '../../images/1v.svg'
import Config2H from '../../images/2h.svg'
import Config2V from '../../images/2v.svg'
import Config2X2H from '../../images/2x2h.svg'
import Config3V from '../../images/3v.svg'
import Config4V from '../../images/4v.svg'

const wavelenght = [
  { name: "UV", state: false },
  { name: "B", state: true },
  { name: "G", state: true },
  { name: "HR", state: false },
  { name: "FR", state: false },
  { name: "W", state: true }
];

class FixtureDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {isRightSidebarClosed: true, isSecondaryMenuClosed: true};
  }

	render() {
		return (
      <div>
        <FormGroup type="detail" label="Canopy" value="Canopy #01 (2V)" />
        <FormGroup type="detail" label="Software" value="2.03" />
        <FormGroup type="detail" label="Firmware" value="00102029" />
        <FormGroup type="detail" label="Moxa Device" value="192.168.1.227" />
        <FormGroup type="detail" label="Port" value="1" />
        <FormGroup type="detail" label="Schedule" value="14 minutes left" />
        <FormGroup type="detail" label="Recipe" value="Recipe #09" />
        <FormGroup type="wavelenght" label="Wavelenght" values={wavelenght} />
      </div>
    )
  }
}

export default FixtureDetails;
