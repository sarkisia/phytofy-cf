// @flow
import validator from 'validator';
import Notifications from 'react-notification-system-redux';
import moment from 'moment';

import Profile from '../models/Profile';
import Canopy, { CanopyFormValidator, getFixturesFromCanopy} from '../models/Canopy';
import Fixture from '../models/Fixture';
import Schedule, { addScheduleToFixtures, removeScheduleFromFixtures } from '../models/Schedule';

import { getPageSize } from '../models/Settings';
import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
  makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext,
  makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo, runCommandOnFixtures } from './helpers';
import { openSidebar, closeSidebar } from './sidebar';

const defaultCanopy = {name: ""};

const queryCanopies = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let canopies;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();

  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1;
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    canopies = await Canopy.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return canopies;
}

const queryCountCanopies = async(filters = {}) => {
  try {
    return Canopy.ccount(filters).exec();
  } catch(err){
    console.error(err);
    return 0;
  }
  
}

export const getObjects = () => {
  return async (dispatch, getState) => {
    let { sortAttribute, sortType, from, filters } = getState().canopiesSetup;
    if (from == 0) from = 1;
    let objects = await queryCanopies(sortAttribute, sortType, from, filters);
    for(let i=0; i<objects.length; i++){
        let canopy = objects[i];
        let fixtures = await getFixturesFromCanopy(canopy);
        
        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          objects[i].scheduleState = (await getState().devices.comm.getScheduleState(fixture.serial_num)).state;
        });
    }

    console.log(objects)

    let count = await countQuery(filters, Canopy);
    dispatch({
        type: "CANOPIES_SETUP_GET",
        objects,
        count,
        ...await getPaginationInfo(from, count)
    })
  }
}


export const getObjectsAll = makeGetObjectsAllAction('CANOPIES_SETUP_GET_ALL', 'canopiesSetup', Canopy, queryCanopies);
export const sort = makeSortAction('CANOPIES_SETUP_SORT', 'canopiesSetup', getObjects);
export const addFilter = makeAddFilterAction('CANOPIES_SETUP_ADD_FILTER', 'canopiesSetup', getObjects);
export const removeFilter = makeRemoveFilterAction('CANOPIES_SETUP_REMOVE_FILTER', 'canopiesSetup', getObjects);
export const pageBack = makePageBack('CANOPIES_SETUP_PAGE_BACK', 'canopiesSetup', getObjects, Canopy);
export const pageNext = makePageNext('CANOPIES_SETUP_PAGE_NEXT', 'canopiesSetup', getObjects, Canopy);
export const goto = makePageGoTo('CANOPIES_SETUP_PAGE_GOTO', 'canopiesSetup', getObjects, Canopy);
export const select = makeSelectObject('CANOPIES_SETUP_SELECT', 'canopiesSetup');
export const deselect = makeDeselectObject('CANOPIES_SETUP_DESELECT', 'canopiesSetup');
export const selectAll = makeSelectAll('CANOPIES_SETUP_SELECT_ALL', 'canopiesSetup');
export const deselectAll = makeDeselectAll('CANOPIES_SETUP_DESELECT_ALL');

export const addFixture = (obj) => {
  return async (dispatch, getState) => {
      await dispatch({
          type: 'CANOPIES_ADD_FIXTURE',
          fixturesSearch: [],
          canopyObj: obj,
      })

      dispatch(openSidebar('ADD_FIXTURE_TO_CANOPY_FORM'));
  }
}

export const getFixtures = (search) => {
  return async (dispatch, getState) => {
      let canopies = await Canopy.cfind({}).exec();
      let usedFixtures = [];
      canopies.forEach((canopy) => {
        usedFixtures = usedFixtures.concat(canopy.fixtures);
      });

      let fixtures = await Fixture.cfind({serial_num: {$nin: usedFixtures}, name: new RegExp(search, 'i')}).sort({'serial_num': 1}).exec();

      await dispatch({
          type: 'CANOPIES_GET_FIXTURES',
          fixturesSearch: fixtures
      })
  }
}

export const submitAddFixture = (fixture) => {
  return async (dispatch, getState) => {
      let canopyObj = getState().canopiesSetup.canopyObj;
      canopyObj.fixtures.push(fixture);


      await dispatch({
          type: 'CANOPIES_EDIT_FIXTURE',
          canopyObj
      })

      let prevState = getState().sidebar.prevState;

      dispatch(openSidebar(prevState.component, prevState.label, prevState.title));
  }
}

export const create = (obj) => {
  return async (dispatch, getState) => {
    if(obj){
      obj.name = `Copy of ${obj.name}`;
    }

    let canopyObj = obj ? obj : {...defaultCanopy};
    let profiles = []
    try {
        profiles = await Profile.cfind({}).sort({'name': 1}).exec();
    } catch(err){
        console.error(err);
    }

    if(profiles.length == 0){
      dispatch(Notifications.error({
        title: `No layouts found!`,
        message: `Please create a layout before trying to add a new canopy.`
      }));
      return;
    }

    if(!canopyObj.profileId){
      canopyObj.profileId = profiles[0]._id
    }

    await dispatch({
      type: 'CANOPIES_SETUP_CREATE',
      canopyObj: canopyObj,
      validation: {}
    })
    dispatch(openSidebar('CREATE_CANOPY_FORM', 'Create', 'New canopy'));
  }
};

export const submitCreate = (obj) => {
  return async (dispatch, getState) => {
    let validations = await CanopyFormValidator.validate(obj);

    if(validations.isValid){
      try {
        await Profile.update({_id: obj.profileId}, {'$inc': {nCanopies: 1}});
        obj._profile = await Profile.findOne({_id: obj.profileId})
        await Canopy.insert(obj);
      } catch(err){
        console.error(err);
      }
      obj = {...defaultCanopy};
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Canopy created',
        message: `Canopy ${obj.name} created with success.`
      }));
    }

    dispatch(getObjects())

    dispatch({
      type: 'CANOPIES_SETUP_SUBMIT_CREATE',
      canopyObj: obj,
      validation: validations
    })
  }
}

export const duplicate = (obj) => {
  return async (dispatch, getState) => {
    let newObj = {...obj};
    delete newObj._id;

    dispatch(create(newObj));
  }
} 

export const edit = (obj) => {
  return async (dispatch, getState) => {
    let canopyId = obj._id;

    await dispatch({
      type: 'CANOPIES_SETUP_EDIT',
      canopyObj: obj,
      editObjId: canopyId,
      validation: {}
    })

    dispatch(openSidebar('EDIT_CANOPY_FORM', 'Edit', `${obj.name}`));
  }
}

export const submitEdit = (obj) => {
  return async (dispatch, getState) => {
    let original = (await Canopy.cfind({_id: getState().canopiesSetup.editObjId}).exec())[0];
    console.log(getState().canopiesSetup.editObjId, obj, original);
    let validations = await CanopyFormValidator.validate(obj, original);
    if(validations.isValid){
      try {
        await Profile.update({_id: getState().canopiesSetup.canopyObj.profileId}, {'$inc': {nCanopies: -1}});
        await Profile.update({_id: obj.profileId}, {'$inc': {nCanopies: 1}});
        obj._profile = await Profile.findOne({_id: obj.profileId})
        await Canopy.update({_id: getState().canopiesSetup.editObjId}, obj);
      } catch(err){
        console.error(err);
      }
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Canopy edited',
        message: `Canopy ${obj.name} edited with success.`
      }));

      let schedules = await Schedule.cfind({canopies: obj._id}).exec();

      for(let i=0; i<obj.fixtures.length; i++) {
        let fixtureId = obj.fixtures[i];
        if(original.fixtures.includes(fixtureId)) {
          continue;
        }

        let fixture = await Fixture.findOne({serial_num: fixtureId});

        if(!fixture) continue;

        for(let j=0; j<schedules.length; j++){
          let schedule = schedules[j];
          await addScheduleToFixtures(schedule, [fixture], getState().devices.comm, dispatch); 
        }  
        
      }
      console.log(original.fixtures);
      console.log(obj.fixtures);
      for(let i=0; i<original.fixtures.length; i++) {
        let fixtureId = original.fixtures[i];

        if(obj.fixtures.includes(fixtureId)) {
          continue;
        }

        let fixture = await Fixture.findOne({serial_num: fixtureId});
        console.log(fixture);
        if(!fixture) continue;

        for(let j=0; j<schedules.length; j++){
          let schedule = schedules[j];
          await removeScheduleFromFixtures(schedule, [fixture], getState().devices.comm, dispatch); 
        }  
        
      }
    }

    dispatch(getObjects())

    dispatch({
      type: 'CANOPIES_SETUP_SUBMIT_EDIT',
      canopyObj: obj,
      editObjId: validations.isValid ? null : getState().canopiesSetup.editObjId,
      validation: validations
    })
  }
}

export const cancelForm = () => {
  return async (dispatch, getState) => {
    dispatch(closeSidebar())
    let obj = {...defaultCanopy};
    dispatch({
      type: 'CANCEL_CANOPY_FORM',
      canopyObj: obj,
      editObjId: null,
      validation: {}
    })
  }
}

export const confirmDelete = (obj) => {
  return async (dispatch, getState) => {
    try {
      let schedules = await Schedule.cfind({canopies: obj._id}).exec();
      let fixtures = await getFixturesFromCanopy(obj);

      for(let j=0; j<schedules.length; j++){
        let schedule = schedules[j];
        await removeScheduleFromFixtures(schedule, fixtures, getState().devices.comm, dispatch); 
      }

      await Profile.update({_id: obj.profileId}, {'$inc': {nCanopies: -1}});
      await Canopy.remove({_id: obj._id})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteObj = (obj) => {
  return async (dispatch, getState) => {
    dispatch(Notifications.warning({
      title: `Delete ${obj.name}`,
      message: `Are you sure you want to delete ${obj.name}?`,
      action: {
        label: 'Yes',
        callback: () => {
          dispatch(confirmDelete(obj))
        }
      }
    }));
  };
}

export const confirmDeleteMultiple = () => {
  return async (dispatch, getState) => {
    try {
      let canopies = await Canopy.cfind({_id: {$in: getState().canopiesSetup.selected}}).exec()
      let profileIds = {};
      for(var i=0; i<canopies.length; i++){
        let canopy = canopies[i];
        let schedules = await Schedule.cfind({canopies: canopy._id}).exec();
        let fixtures = await getFixturesFromCanopy(canopy);

        for(let j=0; j<schedules.length; j++){
          let schedule = schedules[j];
          await removeScheduleFromFixtures(schedule, fixtures, getState().devices.comm, dispatch); 
        }

        if(Object.keys(profileIds).indexOf(canopy.profileId) == -1){
          profileIds[canopy.profileId] = 0;
        }
        profileIds[canopy.profileId] += 1;
      }
      
      Object.keys(profileIds).forEach((key) => {
        Profile.update({_id: key}, {$inc: {nCanopies: -profileIds[key]}});
      })

      await Canopy.remove({_id: {$in: getState().canopiesSetup.selected}}, {multi: true})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteMultiple = () => {
  return async (dispatch, getState) => {
    dispatch(Notifications.warning({
      title: `Delete ${getState().canopiesSetup.selected.length} canopies`,
      message: `Are you sure you want to delete ${getState().canopiesSetup.selected.length} canopies?`,
      action: {
        label: 'Yes',
        callback: () => {
          dispatch(confirmDeleteMultiple())
        }
      }
    }));
  }
}

export const realtimeControl = (obj) => {
  return async (dispatch, getState) => {
    let canopyId = obj._id;
    let fixtures = await Fixture.cfind({serial_num: {$in: obj.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
    
    let comm = getState().devices.comm;
    let fixturesLedState = null;
    if(fixtures.length > 0) {
      await runCommandOnFixtures(fixtures, comm, dispatch, async ({serial_num}) => {
        fixturesLedState = await comm.getLEDstate(serial_num, 'percentage');
      })
    }
    
    await dispatch({
      type: 'CANOPIES_SETUP_REALTIME',
      canopyObj: obj,
      fixturesLedState
    })

    dispatch(openSidebar('REALTIME_CONTROL_FORM', 'Realtime control', `${obj.name}`, 'xl'));
  }
}

export const realtimeControlChange = ({uv, blue, green, hyperRed, farRed, warmWhite}) => {
  return async (dispatch, getState) => {
    let serials = getState().canopiesSetup.canopyObj.fixtures;
    let fixtures = await Fixture.cfind({serial_num: {$in: serials}}).sort({moxa_mac: 1, port: 1}).exec();
    let comm = getState().devices.comm;
    await runCommandOnFixtures(fixtures, comm, dispatch, async ({serial_num}) => {
      await comm.setLEDrt(serial_num, [uv, blue, green, hyperRed, farRed, warmWhite]);
    })
  }
}

export const resumeScheduleOnCanopy = (obj) => {
  return async (dispatch, getState) => {
    try {
      let fixtures = await getFixturesFromCanopy(obj);
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
        await getState().devices.comm.resumeScheduling(fixture.serial_num);
      })
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const stopScheduleOnCanopy = (obj) => {
  return async (dispatch, getState) => {
    try {
      let fixtures = await getFixturesFromCanopy(obj);
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        // await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
        await getState().devices.comm.stopScheduling(fixture.serial_num);
      })
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}


export const resumeScheduleOnMultiple = () => {
  return async (dispatch, getState) => {
    try {
      let canopies = await Canopy.cfind({_id: {$in: getState().canopiesSetup.selected}}).exec();
      for(let i=0; i<canopies.length; i++) {
        let canopy = canopies[i];
        let fixtures = await getFixturesFromCanopy(canopy);
        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
          await getState().devices.comm.resumeScheduling(fixture.serial_num);
        })
      }
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const stopScheduleOnMultiple = () => {
  return async (dispatch, getState) => {
    try {
      let canopies = await Canopy.cfind({_id: {$in: getState().canopiesSetup.selected}}).exec();
      for(let i=0; i<canopies.length; i++) {
        let canopy = canopies[i];
        let fixtures = await getFixturesFromCanopy(canopy);
        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          await getState().devices.comm.stopScheduling(fixture.serial_num);
        })
      }
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}