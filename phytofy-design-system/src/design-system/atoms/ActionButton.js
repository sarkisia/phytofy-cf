import React from 'react';
import styled from 'styled-components';

import Icon from '../atoms/Icons';

const Button = styled.button`
  background: transparent;
  border: 0;
  width: 30px;
  height: 30px;
  padding: 0;
  appearance: none;
  cursor: pointer;

  &:not(:last-child) {
    margin-right: 10px;
  }

  &:hover {
    > svg {
      fill: var(--darkGrey, #A7A8AA);
    }
  }

  > svg {
    width: auto;
  	height: 30px;
    fill: var(--secondaryColor, #87888A);
    transition-duration: .2s;
  }
`

function ActionButton(props) {
  return (
    <Button onClick={props.onClick}>
      <Icon icon={props.icon} />
    </Button>
  );
}

export default ActionButton;
