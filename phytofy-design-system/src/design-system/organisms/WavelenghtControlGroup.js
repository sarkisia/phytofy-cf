import styled from 'styled-components';

const WavelenghtControlGroup = styled.div `
    display: flex;
    flex-direction: row;

    > div:first-child {
        margin-right: var(--spacing, 30px);
    }
`

export default WavelenghtControlGroup;
