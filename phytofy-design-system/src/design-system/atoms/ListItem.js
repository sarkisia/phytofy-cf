import React, { Component } from 'react';
import styled, { css } from 'styled-components';

const ListItem = styled.li `
    background-color: var(--backgroundPrimaryMenu, #F5F6F8);
    border-radius: 4px;
    list-style: none;
    color: var(--textColor, #424242);

    &:not(:last-child) {
        margin-bottom: 5px;
    }

    ${props => props.type === "add" && css`
        background: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'><path fill='#c5c6c8' d='M24 17.333h-6.667v6.667c0 0.733-0.6 1.333-1.333 1.333v0c-0.733 0-1.333-0.6-1.333-1.333v-6.667h-6.667c-0.733 0-1.333-0.6-1.333-1.333v0c0-0.733 0.6-1.333 1.333-1.333h6.667v-6.667c0-0.733 0.6-1.333 1.333-1.333v0c0.733 0 1.333 0.6 1.333 1.333v6.667h6.667c0.733 0 1.333 0.6 1.333 1.333v0c0 0.733-0.6 1.333-1.333 1.333z'></path></svg>") no-repeat center right 10px;
        background-size: 24px 24px;
        background-color: var(--backgroundPrimaryMenu, #F5F6F8);

        button {
            color: var(--sidebarPrimaryColor, #C5C6C8);
        }
    `}
`

const Button = styled.button `
    appearance: none;
    background: transparent;
    width: 100%;
    height: 30px;
    border: 0;
    padding: 5px;
    font-size: 1rem;
    text-align: left;
    cursor: pointer;
`

export default class extends Component {
    render() {
        return (
            <ListItem type={this.props.type}>
                <Button add onClick={(event) => {event.preventDefault(); this.props.onClick();}}>
                    {this.props.value}
                </Button>
            </ListItem>
        )
    }
}
