import React, { Component } from 'react';
import styled from 'styled-components';

import CanopyConfig from '../atoms/CanopyConfig';
import CardValue from '../molecules/CardValue';

const CardCanopy = styled.div`
	background-color: var(--backgroundColor, #FFFFFF);
	border-width: 4px 1px 1px 1px;
	border: 1px solid var(--topbarBorder, #ECEDED);
	border-top: 4px solid var(--primaryColor, #FF6600);
	border-radius: 4px;
`

const Header = styled.div`
	display: flex;
	flex-direction: row;
	flex-wrap: nowrap;
	margin-bottom: var(--spacing, 30px);
	padding: var(--spacing, 30px) var(--spacing, 30px) 0 var(--spacing, 30px);

	h1 {
		color: var(--primaryColor, #FF6600);
	}
`

const Body = styled.div`
	padding: 0 var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);
`

const Configuration = styled.div`
  display: flex;
  flex-direction: row;
	border-bottom: 1px solid var(--backgroundPrimaryMenu, #F5F6F8);
	margin-bottom: 20px;
	padding-bottom: 20px;

	img {
		width: auto;
		height: 80px;
	}
`

const ConfigDetails = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding-left: 16px;
	overflow: hidden;

	span:last-child {
		font-size: 2rem;
		font-weight: var(--bold, 700);
	  white-space: nowrap;
	  overflow: hidden;
	  text-overflow: ellipsis;
	}
`

const Values = styled.div`
	margin-top: 20px;
	display: grid;
	grid-template-columns: 1fr 1fr 1fr;
	grid-column-gap: 1px;
	background-color: var(--backgroundPrimaryMenu, #F5F6F8);
`

export default class extends Component {
	render() {
		const cardValues = this.props.values.map((value) =>
			<CardValue icon={value.icon} value={value.value} unit={value.unit} legend={value.legend} />
		);

		return (
			<CardCanopy>
				<Header>
					<h1>{this.props.title}</h1>
				</Header>
				<Body >
					<Configuration>
						<CanopyConfig config={this.props.configImage} />
						<ConfigDetails>
							<span>Layout</span>
							<span>{this.props.configDesc}</span>
						</ConfigDetails>
					</Configuration>
					<Values>
						{cardValues}
					</Values>
				</Body>
			</CardCanopy>
		)
	}
}
