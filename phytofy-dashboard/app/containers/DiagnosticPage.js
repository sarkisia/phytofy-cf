// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import Table from 'phytofy-design-system/src/design-system/organisms/Table';
import TableData from 'phytofy-design-system/src/design-system/molecules/TableData';
import TableRow from 'phytofy-design-system/src/design-system/molecules/TableRow';
// import Card from 'phytofy-design-system/src/design-system/organisms/Card';
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell';
import TableRowOptions from 'phytofy-design-system/src/design-system/molecules/TableRowOptions';
import Checkbox from 'phytofy-design-system/src/design-system/atoms/Checkbox';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';


import { getTableHeaderCellFromObj } from './helpers';

import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as CanopiesActions from '../actions/canopies'; 


const CanopyContainer = class extends Component {


  render() {

    return (
      <div>
        <Topbar>
        </Topbar>
        <Content>
            {/* <Card title="Diagnostic Coming Soon" /> */}
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(CanopyContainer);