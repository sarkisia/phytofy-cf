import React, { Component } from "react";
import styled from 'styled-components';

const ItemTimeContainer = styled.div `
    display: flex;
    flex-direction: row;
    margin-bottom: var(--spacing, 30px);
    border: 1px solid var(--topbarBorder, #ECEDED);
    border-radius: 4px;
    width: 100%;
    padding: var(--spacing, 30px);

    > span {
        position: absolute;
        margin-top: -42px;
        background-color: var(--backgroundColor, #FFFFFF);
        padding: 0 10px;
        color: var(--topbarBorder, #ECEDED);
    }

    > div {
        margin: 0 15px 0 0 !important;
        width: 100px;
    }
`


export default class extends Component {
    render() {
        return (
            <ItemTimeContainer>
                <span>{this.props.title}</span>
                {this.props.children}
            </ItemTimeContainer>
        );
    }
}
