import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/LightController';
import LightController from '../../lib/phytofy-design-system/src/design-system/organisms/LightController';

import * as CanopiesActions from '../actions/canopiesSetup';


class RealtimeContainer extends Component {
  render() {
    return <LightController {...this.props.fixturesLedState} onAfterChange={this.props.canopiesActions.realtimeControlChange} />
  }
}

function mapStateToProps(state) {
    return {
      fixturesLedState: state.canopiesSetup.fixturesLedState
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return {
      canopiesActions: {...bindActionCreators(CanopiesActions, dispatch)},
    }
  }
  
  export default translate()(connect(mapStateToProps, mapDispatchToProps)(RealtimeContainer));