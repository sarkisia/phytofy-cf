const net = require('net');
const compute = require('../src/compute.js');
const defines = require('../src/defines.js');
const logger = require('../src/logger.js');

class RS485protocol {
  constructor(addr){
    this.myAddr = [];
    addr.split('.').forEach((each) => {
      this.myAddr.push(parseInt(each));
    });
    this.myAddr.reverse();
    logger.debug(`[RS485 comm] Init: my addr .. ${this.myAddr[3]}:${this.myAddr[2]}:${this.myAddr[1]}:${this.myAddr[0]}`)

    this.slaveAddrList = [...Array(247).keys()].map(i => i + 1);
    /*
    responseList = [
      {sequence_num: seqNum, serial_num: serialNum, slave_addr: slaveAddr, function_code: fc, msg: stream}
    ]
    */
    this.responseList = [];
    /*
      fixtureMap = [
        {serial_num: xxxx, slave_addr: zzz, moxa_mac: '81:0:144:232:111:186', port: 4001}
      ]
    */
    this.fixtureMap = [];
    /*
      fixtureMap = [
        {ip: '192.168.1.191',  mac: '81:0:144:232:111:186', ports: [4001]}
      ]
    */
    this.moxaList = [];
    this.takenSlaves = [];
    this.SeqNumIter = 0;
    this.commissioningMoxaMAC = '';
    this.commissioningPort = 4001;
    this.connected = 0;
    this.sendErrors = 0;

    this.storeResponses = (data) => {
      let stream = [... data]; //msg Buffer to array
      if(stream[0] == this.myAddr[0] && stream[1] == this.myAddr[1] && stream[2] == this.myAddr[2] && stream[3] == this.myAddr[3]){
        //filter by sourced IP first
        if(stream.length >= 12){ //minimum msg size
          let slaveAddr = stream[8];
          let fc = stream[9];
          let seqNum = compute.byte2dec([stream[7], stream[6], stream[5], stream[4]]);
          if(stream[9] == 3){ //function code 3, commissioning
            logger.debug(`[storeResponses] Got a FC3 commissioning response! slave address: ${slaveAddr} message: ${stream}`);
            let serialNum = compute.byte2dec([stream[13],stream[12], stream[11], stream[10]]);
            this.takenSlaves.push(slaveAddr);
            this.responseList.push({sequence_num: seqNum, serial_num: serialNum, slave_addr: slaveAddr, function_code: fc, msg: stream});
            //FC4 command to sleep fixture, dont wait for response
            this.setSlaveAddress(serialNum, slaveAddr, 0);
          }else {
            logger.debug(`[storeResponses] Storing message: ${stream}`);
            this.responseList.push({sequence_num: seqNum, serial_num: 0, slave_addr: slaveAddr, function_code: fc, msg: stream});
          }
        }
      }else{
        logger.verbose(`[RS485 Comm] Discarded received message.`);
      }
    };

    this.singleShotCommand = async (serialNum, fc, payload, cmdTag='RS485 CMD') => {
      let slaveAddr = -1, moxaPort = -1, moxaAddr = -1;
      let remotePort = 0, remoteAddress = 0;
      try {
        remotePort = this.conn.remotePort;
        remoteAddress = this.conn.remoteAddress;
      }
      catch(TypeError){
        logger.error(`[${cmdTag}] Open a connection before sending a command!`);
        return -4;
      }
      if( serialNum == 0) slaveAddr = 0;
      else {
        [slaveAddr, moxaAddr, moxaPort] = this.findFixture(serialNum);
        if(slaveAddr == null){
          logger.error(`[${cmdTag}] Fixture ${serialNum} was not found.`);
          return -2
        }
        if(moxaPort != remotePort || moxaAddr != remoteAddress){
          logger.error(`[${cmdTag}] Wrong connection. Fixture ${serialNum} is at ${moxaPort}:${moxaAddr}`);
          return -3
        }
      }
      let seqNum = this.basicSend(slaveAddr, fc, payload);
      let response = undefined, idx = 0;
      [response, idx] = await this.waitForMsg(seqNum); //500 milliseconds timeout
      if( response!= undefined){
          //remove response from list
          this.responseList.splice(idx,1);
          if(compute.compareCRC(response.msg)){
            return response.msg;
          }else{
            logger.error(`[${cmdTag}] Response CRC msg is incorrect!`);
            return -2;
          }
      }else{
        logger.warn(`[${cmdTag}] Reached timeout and no msg arrived!`);
        return -1;
      }
      return 0;
    };

    this.cleanFixtureMap = async (mac, port) => {
      let removed = 0;
      let indexes = []
      this.fixtureMap.forEach((element, index) => {
        if(element.moxa_mac == mac && element.port == port){
          indexes.push(index);
        }
      });
      let length = indexes.length;
      for(let i = 0; i < length; i++){
        this.fixtureMap.splice(indexes[i], 1);
        removed++;
      }
      return removed;
    };

  }
  openConnection(port, mac, timeout=1000){
    let moxa = this.moxaList.find((item) => {
      return item.mac == mac;
    });
    if( moxa == undefined){
      logger.error(`[RS485 Comm] I know moxa with this mac address: ${mac}`);
      this.connected = 0;
      return -1;
    }
    else{
      let address = moxa.ip;
      return new Promise( (resolve, reject) => {

        let openTimeout = setInterval(() => {
          logger.error(`[RS485 Comm] Trying to connect: reach timeout`);
          this.conn.destroy();
          clearInterval(openTimeout);
          return resolve(-1);
        },timeout);

        this.conn = net.createConnection(port, address, () => {
          clearInterval(openTimeout);
          logger.info(`[RS485 Comm]opened connection: ${this.conn.remoteAddress}:${this.conn.remotePort}`);
          this.connected = 1;
          return resolve(1);
        });

        this.conn.on('close', (hadError) => {
          if(hadError){
            logger.error(`[RS485 Comm] closed socket due to a transmission error`);
            return resolve(-1);
          }
          else{
              logger.info('[RS485 Comm] closed communication socket.');
          }
        });

        this.conn.on('data', this.storeResponses);
      });
    }
  }
  async closeConnection(){
    if( this.connected ){
      this.conn.destroy();
      logger.verbose(`[RS485 Comm] Closed connection`);
      this.connected = 0;
    }else{
      logger.warn(`[RS485 Comm] No open connection to be closed`);
    }
  }
  findFixture(serialNum){
    let fixture = this.fixtureMap.find((item) => {
      return item.serial_num == serialNum;
    });
    if(fixture!=undefined){
      let moxa = this.moxaList.find((item) => {
        return item.mac == fixture.moxa_mac;
      });
      if( moxa!=undefined) return [fixture.slave_addr, moxa.ip, fixture.port];
      else{
        logger.error(`[findFixture] Theres a mismatch between fixtures moxa and the active moxas list.`);
      }
    }
    else{
      logger.warn(`[findFixture] Did not find fixture with matching serial number`);
      return [null, 'not found', 0];
    }
  }
  basicSend(slaveAddr, fc, payload=null){
    /*
      payload is an array of n bytes...
      [4B-client IPV4 Addr][4B-sequence num][1B-slave Addr][1B-FC][nB-payload][2B-CRC]
    */
    //socket connect OUTSIDE function
    this.SeqNumIter++;
    let stream = this.myAddr;
    stream = stream.concat(compute.dec2byte(this.SeqNumIter, 4).reverse());
    stream = stream.concat([slaveAddr, fc]);
    if(payload != null) stream  = stream.concat(payload);
    let crc = compute.computeCRC(stream, 0);
    stream = stream.concat(crc);
    let msg = Buffer.from(stream);//byte-order is little endyan
    let ret = this.SeqNumIter;

    logger.debug(`[DEBUG] sending msg: ${stream}`);
    try{
      this.conn.write(msg, (err) => {
        if (err != null){
          logger.error(`[RS485 Comm] send msg failed with error: ${err}. Closing socket`);
          this.sendErrors ++;
        } else {
          logger.verbose(`[RS485 Comm] Message sent with sequence number: ${this.SeqNumIter}`);
        }
      });
    }
    catch(TypeError){
      logger.error(`[RS485 Comm] Open a connection before writing a message!`);
    }

    return ret;
  }
  waitForMsg(seqNum, limit=5){
    /*
      Look for a msg in responseList with sequence number SeqNum for 0.5 seconds
    */
    return new Promise( (resolve, reject) => {
      let counter = 0, idx = -1;
      let response = undefined;
      //Broadcast message is sent every broadcastInterval miliseconds
      let readInterval = setInterval( () => {
        response = this.responseList.find((item, index) => {
          if(item.sequence_num == seqNum){
              idx = index;
              return item;
          }
        });
        counter ++;
        if (counter >= limit || response != undefined) {
          clearInterval(readInterval);
          resolve([response, idx]);
        }
      }, 100);
    });
  }
  async commissionFixtures(moxaMac, ports){
  /*
  */
    this.closeConnection(); //try to close connection before starting commissioning
    this.commissioningMoxaMAC = moxaMac;
    let addedFixtures = 0;
    //for each port
    for(let i = 0; i < ports.length; i++){
      this.commissioningPORT = ports[i];
      await this.cleanFixtureMap(moxaMac, ports[i]);
      await this.openConnection(ports[i], moxaMac);
      await this.getSerialNumbers(defines.BROADCAST_PERIOD, defines.BROADCAST_TIMEOUT);
      addedFixtures += await this.updateFixtureMap();
      await this.closeConnection();
      //go through commission responses and build fixture map (set slave adresses for those necessary)
    }
    return addedFixtures;
  }
  async updateFixtureMap(){
    let commissionedNums = [], storedSlaves = [];
    let addedFixtures = 0;
    let response = 0;
    let length = this.responseList.length;
    //remove duplicates
    /*this.takenSlaves = this.takenSlaves.filter(function(elem, pos) {
        return this.takenSlaves.indexOf(elem) == pos;
    })*/
    let available = compute.arrDiff(this.slaveAddrList, this.takenSlaves).reverse();
    for(let i = 0; i < length; i++){
      if( this.responseList[i].function_code == 3){
        response = this.responseList.splice(i,1); //returns an array
        i--;
        length--;
        let repeated = commissionedNums.find((num) => {
            return num == response[0].serial_num;
        });
        if( !repeated){
          if(response[0].slave_addr == 255){
            //atribute an available slave address;
            let slaveAddr = available.pop();
            logger.debug(`[Update Fixture Map] Using slave addr: ${slaveAddr} for fixture-${response[0].serial_num}`);
            //if not for the firmware bug, wait flag of FC4 should be = 1
            let ack = await this.setSlaveAddress(response[0].serial_num, slaveAddr, 0);
            //if(ack == 1){
            storedSlaves.push(slaveAddr);
            this.fixtureMap.push({serial_num: response[0].serial_num, slave_addr: slaveAddr, moxa_mac: this.commissioningMoxaMAC, port: this.commissioningPORT});
            addedFixtures++;
            logger.verbose(`[Update Fixture Map] Added fixture: ${response[0].serial_num}`);
            commissionedNums.push(response[0].serial_num);
            //}
            //else{
            //  logger.error(`[Update Fixture Map] Could not commission fixture with SN: ${response[0].serial_num}`);
            //}
          }else{
            let taken = storedSlaves.find((addr) => {
              return addr == response[0].slave_addr;
            });
            if(taken){
              let slaveAddr = available.pop();
              logger.debug(`[Update Fixture Map] There is already a fixture with this slave address. Setting new address: ${slaveAddr}`);
              //if not for the firmware bug, wait flag of FC4 should be = 1
              let ack = await this.setSlaveAddress(response[0].serial_num, slaveAddr, 0);
              //if(ack == 1){
              this.fixtureMap.push({serial_num: response[0].serial_num, slave_addr: slaveAddr, moxa_mac: this.commissioningMoxaMAC, port: this.commissioningPORT});
              addedFixtures++;
              logger.verbose(`[Update Fixture Map] Added fixture: ${response[0].serial_num}`);
              commissionedNums.push(response[0].serial_num);
              //}
              //else{
              //  logger.error(`[Update Fixture Map] Could not commission fixture with SN: ${response[0].serial_num}`);
              //}
            }else{
              this.fixtureMap.push({serial_num: response[0].serial_num, slave_addr: response[0].slave_addr, moxa_mac: this.commissioningMoxaMAC, port: this.commissioningPORT});
              storedSlaves.push(response[0].slave_addr);
              addedFixtures++;
              logger.verbose(`[Update Fixture Map] Added fixture: ${response[0].serial_num}`);
              commissionedNums.push(response[0].serial_num);
            }
          }
        }
      }
    }
    logger.debug(`responseList after commissioning: ${this.responseList}`);
    //empty taken slaves for next port
    this.takenSlaves = [];
    return addedFixtures;
  }
  /*
    Fixture Commands for commissioning
    ...
    ....
  */
  getSerialNumbers(period, timeout){
    /*
      only implemented in timed broadcast mode,
      for commissioning
    */
    //FC3
    return new Promise( (resolve, reject) => {
      let limit = timeout/period, counter = 0; //5 times
      let serBroadcastInterval = setInterval( () => {
        this.basicSend(0, 3, 1);
        counter ++;
        if (counter >= limit) {
          clearInterval(serBroadcastInterval);
          resolve(counter);
        }
      }, period);
    });
  }
  async setSlaveAddress(serialNum, slaveAddr, wait=1){
    /*
      Doest not use findFixture by serial number because it is
      used during commissioning and no slave address has (obviously)
      been set a priori.
    */
    //FC4
    let payload =  compute.dec2byte(serialNum, 4).reverse();
    payload = payload.concat(slaveAddr);
    let seqNum = this.basicSend(0, 4, payload);
    let response = undefined, idx = 0;
    if(wait){
      [response, idx] = await this.waitForMsg(seqNum); //500 milliseconds timeout
      if( response!= undefined){
        //remove response from list
        this.responseList.splice(idx,1);
        if(compute.compareCRC(response.msg)){
          return response.msg[10];
        }else{
          logger.error(`[setSlaveAddress] Response CRC msg is incorrect!`);
          return -1;
        }
      }else{
        logger.warn('[setSlaveAddress] Reached timeout and no msg arrived!');
        return -1;
      }
    }else{
        return seqNum;
    }
  }
  /*
    Single Shot Fixture Commands
    ...
    ....
  */
  async getFixtureInfo(serialNum=0){
    //FC 9
    let ret = await this.singleShotCommand(serialNum, 9, null, 'getFixtureInfo');
    try {
      ret.splice(0,10);
      let fw = compute.byte2uint(ret.splice(0,4).reverse());
      let hw = compute.byte2uint(ret.splice(0,4).reverse());
      let uvMax = compute.byte2uint(ret.splice(0,4).reverse());
      let bluMax = compute.byte2uint(ret.splice(0,4).reverse());
      let greMax = compute.byte2uint(ret.splice(0,4).reverse());
      let hrMax = compute.byte2uint(ret.splice(0,4).reverse());
      let frMax = compute.byte2uint(ret.splice(0,4).reverse());
      let wMax = compute.byte2uint(ret.splice(0,4).reverse());
      return {"FW": fw, "HW": hw, "uvMax": uvMax, "blueMax": bluMax, "greenMax": greMax, "hyperRedMax": hrMax, "farRedMax": frMax, "whiteMax": wMax};
    }catch(TypeError){
      return {"FW": -1, "HW": -1, "uvMax": -1, "blueMax": -1, "greenMax": -1, "hyperRedMax": -1, "farRedMax": -1, "whiteMax": -1};
    }
  }
  async getLEDstate(serialNum=0, conf='percentage'){
    //FC 13
    let payload =[0];
    if(conf == 'percentage') payload[0] = 3;
    else{
      payload[0] = 7;
    }
    let ret = await this.singleShotCommand(serialNum, 13, payload, 'getLEDstate');
    try {
      ret.splice(0,11); //splice conifg byte too for the time being
      let uv = compute.byte2uint(ret.splice(0,4).reverse());
      let blue = compute.byte2uint(ret.splice(0,4).reverse());
      let green = compute.byte2uint(ret.splice(0,4).reverse());
      let hr = compute.byte2uint(ret.splice(0,4).reverse());
      let fr = compute.byte2uint(ret.splice(0,4).reverse());
      let white = compute.byte2uint(ret.splice(0,4).reverse());
      return {"uv": uv, "blue": blue, "green": green, "hyperRed": hr, "farRed": fr, "white": white};
    }catch(TypeError){
      return {"uv": -1, "blue": -1, "green": -1, "hyperRed": -1, "farRed": -1, "white": -1};
    }
    return ret;
  }
  async getTimeReference(serialNum=0){
    //FC 11
    let ret = await this.singleShotCommand(serialNum, 11, null, 'getTimeReference');
    return compute.byte2uint([ret[13], ret[12], ret[11], ret[10]]);
  }
  async setTimeReference(serialNum=0, epoch){
    //FC 10
    let payload = compute.uint2byte(epoch, 'little');//.reverse();
    let ret = await this.singleShotCommand(serialNum, 10, payload, 'setTimeReference');
    try {
      ret.splice(0,10);
      return ret;
    }catch(TypeError){
      return ret;
    }
  }
  async getIlluminanceConfiguration(serialNum=0){
    //FC25
    let ret = await this.singleShotCommand(serialNum, 25, null, 'getIlluminanceConfiguration');
    try {
      ret.splice(0,10); //splice conifg byte too for the time being
      let coefUV = compute.byte2float(ret.splice(0,4).reverse());
      let coefBlue = compute.byte2float(ret.splice(0,4).reverse());
      let coefGreen = compute.byte2float(ret.splice(0,4).reverse());
      let coefHR = compute.byte2float(ret.splice(0,4).reverse());
      let coefFR = compute.byte2float(ret.splice(0,4).reverse());
      let coefWhite = compute.byte2float(ret.splice(0,4).reverse());
      return {"uv": coefUV, "blue": coefBlue, "green": coefGreen, "hyperRed": coefHR, "farRed": coefFR, "white": coefWhite};
    }catch(TypeError){
      return {"uv": -1, "blue": -1, "green": -1, "hyperRed": -1, "farRed": -1, "white": -1};
    }
    return ret;
  }
  async setIlluminanceConfiguration(serialNum=0, coefs){
    //FC24
    let payload = [];
    for(let i = 0; i < 6; i++){
      payload = payload.concat(compute.float2byte(coefs[i]));
    }
    let ret = await this.singleShotCommand(serialNum, 24, payload, 'setIlluminanceConfiguration');
    try {
      ret.splice(0,10);
      return ret;
    }catch(TypeError){
      return ret;
    }
  }
  async getModuleTemperature(serialNum=0){
    //FC26
    let ret = await this.singleShotCommand(serialNum, 26, null, 'getModuleTemperature');
    try {
      ret.splice(0,10);

      let b = 0, ch = 0;
      let bArr = [], temperatures = [];
      while( ch < 12) {
        bArr.push(ret.pop());
        b++;
        if( b==4 ){
          temperatures.push(compute.byte2float(bArr));
          ch++;
          b = 0;
          bArr = [];
        }
      }
      return temperatures;
    }catch(TypeError){
      return ret;
    }
  }
  /*
    Schedule commands
  */
  async setSchedule(serialNum, schedID, startDtime, endDtime, dimming=[0,0,0,0,0,0]){
    //FC14 - channels default = [0,0,0,0,0,0] for testing only
    let channels = [];
    let config = 0x03;
    let payload = compute.uint2byte(schedID, 'little');
    payload = payload.concat(compute.uint2byte(startDtime, 'little'));
    payload = payload.concat(compute.uint2byte(endDtime, 'little'));
    payload = payload.concat(config);
    for(let i = 0; i < 6; i++){
      channels = channels.concat(compute.dec2byte(dimming[i]).reverse());
    }
    payload = payload.concat(channels);
    let ret = await this.singleShotCommand(serialNum, 14, payload, 'setSchedule');
    try {
      ret.splice(0,10);
      return ret;
    }catch(TypeError){
      return ret;
    }
  }
  async getSchedule(serialNum, keyword, search='id'){
    //FC 15

    let payload = [0];
    if(search=='id'){
      payload[0] = 0;
    }else if(search == 'index'){
      payload[0] = 1;
    }
    let keywd = compute.dec2byte(keyword, 4);
    payload.push(keywd[3],keywd[2],keywd[1],keywd[0]);

    let ret = await this.singleShotCommand(serialNum, 15, payload, 'getSchedule');
    return ret;
  }
  async getScheduleCount(serialNum){
    //FC16
    let ret = await this.singleShotCommand(serialNum, 16, null, 'getScheduleCount');
    try {
      ret.splice(0,10);
      return compute.byte2uint(ret.reverse());
    }catch(TypeError){
      return ret;
    }
  }
  async getScheduleState(serialNum){
    //FC17
    let ret = await this.singleShotCommand(serialNum, 17, null, 'getScheduleState');
    try {
      ret.splice(0,10);
      let run = ret.splice(0,1);
      return {state: run[0], id: compute.byte2uint(ret)};
    }catch(TypeError){
      return {state: -1, id: 'error'};
    }
  }
  async deleteSchedule(serialNum, schedID){
    //FC18
    let payload = compute.uint2byte(schedID, 'little');
    let ret = await this.singleShotCommand(serialNum, 18, payload, 'deleteSchedule');
    try {
      ret.splice(0,10);
      return ret;
    }catch(TypeError){
      return ret;
    }
  }
  async deleteAllSchedules(serialNum){
    //FC19
    let ret = await this.singleShotCommand(serialNum, 19, null, 'deleteAllSchedules');
    try {
      ret.splice(0,10);
      return ret;
    }catch(TypeError){
      return ret;
    }
  }
  async stopScheduling(serialNum){
    //FC20
    let ret = await this.singleShotCommand(serialNum, 20, null, 'stopScheduling');
    try {
      ret.splice(0,10);
      return ret;
    }catch(TypeError){
      return ret;
    }
  }
  async resumeScheduling(serialNum){
    //FC21
    let ret = await this.singleShotCommand(serialNum, 21, null, 'resumeScheduling');
    try {
      ret.splice(0,10);
      return ret;
    }catch(TypeError){
      return ret;
    }
  }
  /*
    Real-time
  */
  async setLEDrt(serialNum=0, dimming, conf='percentage'){
    //FC12
    let payload = [0];
    if(conf == 'percentage') payload[0] = 3;
    else{
      payload[0] = 7;
    }
    let channels = [];
    for(let i = 0; i < 6; i++){
      channels = channels.concat(compute.dec2byte(dimming[i]).reverse());
    }
    payload = payload.concat(channels);
    console.log(payload);
    //payload = payload.concat([0,0,0,0,0x0A,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    let slaveAddr = -1, moxaPort = -1, moxaAddr = -1;
    let remotePort = 0, remoteAddress = 0;
    try {
      remotePort = this.conn.remotePort;
      remoteAddress = this.conn.remoteAddress;
    }
    catch(TypeError){
      logger.error(`[setLED real-time] Open a connection before sending a command!`);
      return -4;
    }
    if( serialNum == 0) slaveAddr = 0;
    else {
      [slaveAddr, moxaAddr, moxaPort] = this.findFixture(serialNum);
      if(slaveAddr == null){
        logger.error(`[setLED real-time] Fixture ${serialNum} was not found.`);
        return -2
      }
      if(moxaPort != remotePort || moxaAddr != remoteAddress){
        logger.error(`[setLED real-time] Wrong connection. Fixture ${serialNum} is at ${moxaPort}:${moxaAddr}`);
        return -3
      }
    }
    let seqNum = this.basicSend(slaveAddr, 12, payload);
    return seqNum;
  }
  /*
    Not normally used commands
  */
  async getSlaveAddress(serialNum){
    //FC5
    let payload = compute.uint2byte(serialNum, 'little');
    let ret = await this.singleShotCommand(0, 5, payload, 'getSlaveAddress');
    try {
      ret.splice(0,10);
      return ret[0];
    }catch(TypeError){
      return ret;
    }
  }
  async setGroupId(serialNum, groupID){
    //FC6
    let payload = compute.uint2byte(groupID, 'little');
    let ret = await this.singleShotCommand(serialNum, 6, payload, 'setGroupId');
    try {
      ret.splice(0,10);
      return ret
    }catch(TypeError){
      return ret;
    }
  }
  async getGroupId(serialNum){
    //FC7
    let ret = await this.singleShotCommand(serialNum, 7, null, 'getGroupId');
    try {
      ret.splice(0,10);
      return compute.byte2uint(ret.reverse());
    }catch(TypeError){
      return ret;
    }
  }
};

module.exports = {
  RS485protocol
};
