import styled, { css } from 'styled-components';

export default styled.td `
  vertical-align: middle;
  padding: 0 calc(var(--spacing, 30px) / 3);

  &:first-child {
    padding-left: var(--spacing, 30px);
  }

  ${props => props.selectable && css`
    width: var(--selectable, 80px);
    text-align: center;
  `}

  ${props => props.config && css`
    min-width: var(--config, 100px);
    text-align: center;
  `}

  ${props => props.measure && css`
    min-width: var(--measure, 100px);
    text-align: center;
  `}

  ${props => props.status && css`
    min-width: var(--status, 100px);
    text-align: center;

    svg {
      fill: var(--green, #009D3D);
    }
  `}
`
