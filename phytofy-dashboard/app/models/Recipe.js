const electron = require('electron');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

import FormValidator from '../utils/FormValidator';

let Recipe;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Recipe = new Datastore({ filename: `${userDataPath}/recipes.db`, autoload: true });

  return Recipe;
}

const rules = [
    {
      field: 'name',
      method: validator.isEmpty,
      validWhen: false,
      message: 'Name is required'
    },
    {
        field: 'name',
        method: async (value) => {
          let count = await Recipe.count({name: value});
          return count == 0;
        },
        validWhen: true,
        ignoreEqualsPrev: true,
        message: 'A recipe with this name already exists'
      },
      {
        field: 'name',
        method: validator.isLength,
        args: [{min: 0, max: 30}],
        validWhen: true,
        message: 'Name exceeds the maximum length of 30'
      },
      {
        field: 'name',
        method: async (value) => {
          return /^[A-Za-z\d\s]+$/.test(value)
        },
        validWhen: true,
        message: 'Name contains invalid characters'
      },
]  

export default Recipe ? Recipe : createDatastore();

export const RecipeFormValidator = new FormValidator(rules);
